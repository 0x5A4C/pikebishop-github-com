.. pikeBishop output documentation master file, created by
   sphinx-quickstart on Mon Jul 22 11:35:49 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pikeBishop output's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   testGpx00
   testGpx01
   trip_logs


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
