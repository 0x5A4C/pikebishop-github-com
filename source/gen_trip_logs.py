#!/usr/bin/env python3

"""
generate trip log
"""

import os
import sys
import traceback
import inspect
import logging
import argparse
import re
import shutil
from string import Template
from rstcloth.rstcloth import RstCloth

def handle_exception(exc_type, exc_value, exc_traceback):
    print("Unhandled Exception:")
    print()
    print("".join(traceback.format_exception(exc_type, exc_value, exc_traceback)))
    sys.exit(1)


def caller():
    return inspect.getouterframes(inspect.currentframe())[1][1:4][2]


def init_logger(verbosity):
    # set up logging to console
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG if verbosity > 1 else logging.ERROR)
    formatter = logging.Formatter('%(name)-30s:%(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)

    logging.getLogger('').setLevel(logging.DEBUG)

    logger = logging.getLogger(caller())
    logger.debug("done")


def get_files_witx_ext(dir_path, ext):
    files_matches = []
    for file_name in os.listdir(dir_path):
        if file_name.endswith(ext):
            files_matches.append(file_name)
    return files_matches


def get_trip_log_title(gpx):
    logger = logging.getLogger(caller())

    title = 'Trip log: {0}\n'.format(os.path.splitext(os.path.basename(gpx))[0])
    title += '====================================================================================\n\n'
    logger.debug('trip log title: %s', title)
    return title

def get_trip_log_leaflet_map(gpx):
    logger = logging.getLogger(caller())
    map_html = ''

    template = open('leaflet_maptm.tmpl', 'r').read()
    map_html = Template(template).substitute({'gpx' : os.path.join('../', '_static', 'gpx', os.path.basename(gpx))})

    return map_html

def gen_trip_logs():
    logger = logging.getLogger(caller())
    logger.debug('generate trip logs')

    repo_root_path = os.path.abspath(os.path.join(
        os.path.dirname(os.path.realpath(__file__)), '_static', 'gpx'))
    logger.debug("path: %s", repo_root_path)

    gpxs = get_files_witx_ext(repo_root_path, '.gpx')
    logger.debug(gpxs)

    reports = []
    gpx = []
    for gpx in gpxs:
        logger.debug('generate trip log for: %s', gpx)
        # create dir
        trip_log_dir = os.path.abspath(os.path.join(
            os.path.dirname(os.path.realpath(__file__)), os.path.basename(gpx)))
        trip_log_dir = os.path.splitext(trip_log_dir)[0]
        try:
            logger.debug("create dir: %s", trip_log_dir)
            os.mkdir(trip_log_dir)
        except FileExistsError:
            logger.debug("directory exists")
        # copy gpx
        local_gpx = os.path.join(repo_root_path, gpx)
        logger.debug('create local copy of gpx: %s', local_gpx)
        shutil.copy(local_gpx, trip_log_dir)
        # create report file
        trip_log_file_name = os.path.splitext(
            os.path.basename(gpx))[0] + '.rst'
        logger.debug('create trip log file: %s', trip_log_file_name)
        trip_log_file_full_name = os.path.join(trip_log_dir, trip_log_file_name)
        reports.append(trip_log_file_full_name)
        logger.debug('trip_log_file_full_name: %s', trip_log_file_full_name)
        gpx_trip_log = open(trip_log_file_full_name, 'w+')
        # write log title
        gpx_trip_log.write(get_trip_log_title(local_gpx))
        gpx_trip_log.write(get_trip_log_leaflet_map((local_gpx)))

    return reports, gpx

def gen_trip_logs_sum(reports):
    logger = logging.getLogger(caller())
    logger.debug('generate trip logs summary')
    # trip_logs = open('trip_logs.rst', 'w+')
    trip_logs_sum = RstCloth()

    trip_logs_sum.title('Trip logs')
    # trip_logs_sum.h1('Trip logs')

    trip_logs_sum.newline()

    content_list = []
    for report in reports:
        # trip_logs_sum._add('{0}'.format(report))
        # trip_logs_sum.newline()
        logger.debug(report)
        logger.debug(os.path.dirname(report))

        rel_path = os.path.join(os.path.split(os.path.dirname(report))[-1], os.path.basename(report))
        content_list.append(rel_path)

    trip_logs_sum.directive(name="toctree", fields=[('maxdepth', 2), ('caption', 'Trip logs:')], content = content_list)

    trip_logs_sum.write('trip_logs.rst')

def main():
    """
    Main function
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", action='count',
                        help="verbose debug output", default=0)
    args = parser.parse_args()

    init_logger(args.verbose)
    logger = logging.getLogger(caller())
    logger.debug('Input args values: %s', args)

    reports, gpx = gen_trip_logs()
    gen_trip_logs_sum(reports)


if __name__ == "__main__":
    main()
