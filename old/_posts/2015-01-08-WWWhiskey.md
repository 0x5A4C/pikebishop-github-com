---
layout: post
category : drink
tagline: "Supporting tagline"
tags : [drink]
---
{% include JB/setup %}
---

##Dimple Pinch:##

Walter White's Final Drink


<u>Haig Dimple 15 YO Bottling Note</u>

Haig Dimple, or 'Dimple Pinch' as it's called in the US of A, recently popped up in everybody's favourite programme Breaking Bad! A blend with a high malt proportion, including whiskies from Glenkinchie and Linkwood.
