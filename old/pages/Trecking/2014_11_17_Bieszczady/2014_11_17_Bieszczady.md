---
layout: page
title: "2014-11-17 2014-11-17 - Bieszczady"
description: ""
---
{% include JB/setup %}

#Bieszczady

###Schronisko PTTK Chatka Puchatka - Ustrzyki Górne
###2014-11-17 05:26:01 - 2014-11-17 17:29:05


Schronisko PTTK Chatka Puchatka, Połonina Wetlińska, Roh, Srebrzysta Przełęcz, Osadzki, Hnatowe Berdo, Szare Berdo, Przełęcz M.Orłowicza, Smerek, Wetlina, Smerek, Górny Koniec, Rabia Skała - 1199 m npm Bieszczady, Czoło - 1159 m npm Bieszczady, Mogiłki, Czerteż, Hrubky, Kamienna, Rawka, Wielka Semanowa - 1071 m npm Bieszczady, Ustrzyki Górne, 


<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style> html, body, #map {height:100%; width:100%; padding:0px; margin:0px;}</style>
<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.6.2/leaflet.css" />
<!--[if lte IE 8]><link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.6.2/leaflet.ie.css" /><![endif]-->

<link rel="stylesheet" href="Res/js/LfElevation/dist/Leaflet.Elevation-0.0.1.css" />

<script type="text/javascript" src="http://cdn.leafletjs.com/leaflet-0.6.2/leaflet.js"></script>
<script type="text/javascript" src="Res/js/LfElevation/dist/Leaflet.Elevation-0.0.1.min.js"></script>
<script type="text/javascript" src="Res/js/LfElevation/lib/leaflet-gpx/gpx.js"></script>

<div id="map" style="width: 100%; height: 600px"></div>

<script type="text/javascript">
var map = new L.Map('map').setView([50.242656, 16.736217], 13);

var url = 'http://otile{s}.mqcdn.com/tiles/1.0.0/map/{z}/{x}/{y}.jpeg', attr ='Tiles Courtesy of <a href="http://www.mapquest.com/">MapQuest</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>', service = new L.TileLayer(url, {subdomains:"1234",attribution: attr});

var el = L.control.elevation();
el.addTo(map);
var g=new L.GPX("Res/bieszczady.gpx", {
    async: true,
        marker_options: {
        startIconUrl: 'Res/js/LfElevation/lib/leaflet-gpx/pin-icon-start.png',
        endIconUrl: 'Res/js/LfElevation/lib/leaflet-gpx/pin-icon-end.png',
        shadowUrl: 'Res/js/LfElevation/lib/leaflet-gpx/pin-shadow.png'
        }
});
g.on('loaded', function(e) {map.fitBounds(e.target.getBounds());});
g.on("addline",function(e) {el.addData(e.line);});
g.addTo(map);
map.addLayer(service);
</script>

<br>

		Length 2D:          36.71 km
		Length 3D:          37.38 km
		Moving time:        10:02:03
		Stopped time:       02:01:01
		Max speed:          1.58 m/s = 5.69 km/h
		Total uphill:       1803 m
		Total downhill:     2366 m
		Started:            2014-11-17 05:26:01
		Ended:              2014-11-17 17:29:05


								Length 2D:          36.71 km
								Length 3D:          37.38 km
								Moving time:        10:02:03
								Stopped time:       02:01:01
								Max speed:          1.58 m/s = 5.69 km/h
								Total uphill:       1803 m
								Total downhill:     2366 m
								Started:            2014-11-17 05:26:01
								Ended:              2014-11-17 17:29:05


[<img src="Res/geoSectionGpxRasterObject_Map.png" width="" height="100"/>](Res/geoSectionGpxRasterObject_Map.png "Raster Map")

**[Raster Map](Res/geoSectionGpxRasterObject_Map.png) (large file).**

[<img src="Res/geoSectionGpxRasterObject_Profile.png" width="" height="100"/>](Res/geoSectionGpxRasterObject_Profile.png "Raster Profile")

**[Raster Profile](Res/geoSectionGpxRasterObject_Profile.png) (large file).**

<script type="text/javascript" src="highslide/highslide-with-gallery.js"></script>
<script type="text/javascript" src="highslide/highslide.config.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="highslide/highslide-ie6.css" />
<![endif]-->

<h3>Gallery</h3>
<div class="highslide-gallery">

<ul>
<li><a href="highslide/images/IMG_2561.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2561.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2551.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2551.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2589.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2589.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2564.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2564.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2552.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2552.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2570.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2570.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2581.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2581.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2558.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2558.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2559.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2559.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2583.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2583.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2577.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2577.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2569.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2569.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2549.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2549.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2557.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2557.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2575.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2575.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2574.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2574.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2560.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2560.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2585.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2585.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2554.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2554.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2555.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2555.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2573.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2573.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2571.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2571.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2563.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2563.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2579.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2579.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2587.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2587.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2572.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2572.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2553.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2553.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2562.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2562.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2576.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2576.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2567.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2567.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2566.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2566.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2584.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2584.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2580.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2580.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2582.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2582.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2550.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2550.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2556.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2556.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2565.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2565.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2586.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2586.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2568.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2568.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2578.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2578.thumbnail"  alt=""/></a></li>
<li><a href="highslide/images/IMG_2588.JPG" class="highslide" title="" onclick="return hs.expand(this, config1 )"><img src="highslide/images/IMG_2588.thumbnail"  alt=""/></a></li>

</ul>
<div style="clear:both"></div></div>
