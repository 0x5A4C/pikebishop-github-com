---
layout: page
title: "Wieeelka Klodzka jstools4gps"
description: ""
---
{% include JB/setup %}

**jstools4gps**
<script src="http://maps.google.com/maps?file=api&v=1&key=ABQIAAAA_mZ97IarGA8n3DLPdxV-yBRgRic70yPyiUfuAsB0dLZo7kePUxTDpleBGW-rsl3L2vFVIOD6VPwRwA" type="text/javascript"></script>
<script src="http://openlayers.org/api/OpenLayers.js"> </script> 
<script type="text/javascript" src="Res/js/jstools4gps_fixed/js/mapstraction.js"> </script>
<script type="text/javascript" src="Res/js/jstools4gps_fixed/js/prototype.js"> </script>
<script type="text/javascript" src="Res/js/jstools4gps_fixed/js/flotr-0.2.0-alpha.js"> </script>
<script type="text/javascript" src="Res/js/jstools4gps_fixed/js/canvastext.js"> </script>
<script type="text/javascript" src="Res/js/jstools4gps_fixed/js/ObjTree.js"> </script>
<script type="text/javascript" src="Res/js/jstools4gps_fixed/js/tools4gps.js"> </script>
<link rel="stylesheet" href="Res/js/jstools4gps_fixed/cs/style.css" type="text/css" />
<!--[if IE]>
<script type="text/javascript" src="Res/js/jstools4gps_fixed/excanvas.js"> </script>
<script type="text/javascript" src="Res/js/jstools4gps_fixed/base64.js"> </script>
<![endif]-->

<div id="map" style="width: 580px; height: 350px"> </div>
<div id="buttons">
<img class="zoom" src="Res/js/jstools4gps_fixed/img/zoom_off.png" width="22" height="22">
<img class="xaxis" src="Res/js/jstools4gps_fixed/img/dist_off.png" width="22" height="22">
<img class="yaxis" src="Res/js/jstools4gps_fixed/img/elev_off.png" width="22" height="22">
<img class="test" src="Res/js/jstools4gps_fixed/earth_off.png" width="22" height="22">
</div>
<div id="profile" style="width:580px; height:200px;"> </div>

<script type="text/javascript">
document.observe('dom:loaded', function() {
var map = new Mapstraction('map','openlayers');

var parser = new GPS.Parser("Res/gpxData/3.gpx");

parser.run(function (gpsData) 
{var chart = new GPS.Profile("profile", gpsData);
var gpsmap = new GPS.Map(map, gpsData, {'chart': chart, 'marker': {iconAnchor: [-16,-16]}});
var buttonbar = new GPS.ButtonBar("buttons", {'chart': chart,yaxis: {values: ['elev', 'vel', 'heartrate'],tagvel: 'Show heart rate in \'Y\' axis',tagheartrate: 'Show elevation in \'Y\' axis'}, test: {tag: 'Reset map zoom',onClick: function(button) {map.autoCenterAndZoom();}}});
});
});
</script>

