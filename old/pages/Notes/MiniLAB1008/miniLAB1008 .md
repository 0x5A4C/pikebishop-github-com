---
layout: page
title: "miniLAB 1008"
description: ""
---
{% include JB/setup %}

##MCC DAQ miniLAB 1008 in Linux envirioment##

**libhid - building and installing**

**[Inspiration](http://kozelljozsef.blogspot.com/2013/10/mcc-daq-minilab-1008-usb-under-linux.html) and main tutorial.**

Steps:
  
 - sudo apt-get install libusb-1.0-0 libusb-1.0-0-dev
 - fetch libhid from:
    - ftp://lx10.tx.ncsu.edu/pub/Linux/drivers/USB/
    - http://libhid.alioth.debian.org/
    - http://www.wiri.be/libhid-0.2.16.zip 
    - or from [there](Res/libhid-0.2.16.zip).
 - tar -xzf libhid*.tar.gz
 - cd libhid*
 - ./configure --prefix=/usr --sysconfdir=/etc --disable-werror
 - make
 
            In file included from linux.c:6:0:
            ../include/hid.h:5:17: fatal error: usb.h: No such file or directory
             #include <usb.h>
                             ^
            compilation terminated.
        
 - sudo apt-get install libusb-dev
 - make
 - much better
 - sudo make install
 
 - wget ftp://lx10.tx.ncsu.edu/pub/Linux/drivers/USB/60-mcc.rules
 - sudo cp 60-mcc.rules /dev/.udev/rules.d/
 - sudo udevadm control --reload-rules
 - tar xzf MCCLIBHID*.tgz
    tar (child): MCCLIBHID*.tgz: Cannot open: No such file or directory
 - wget ftp://lx10.tx.ncsu.edu/pub/Linux/drivers/USB/MCCLIBHID.1.63.tgz
 - cd mcc-libhid
 - make
 - sudo make install
 - sudo ./test-minilab1008 

        miniLAB 1008 Device is found! interface = 0
        
        miniLAB 1008 Testing
        ----------------
        Hit 'b' to blink LED
        Hit 's' to set user id
        Hit 'g' to get user id
        Hit 'c' to test counter 
        Hit 'd' to test digital I/O 
        Hit 't' to test digital bit I/O
        Hit 'o' to test analog output
        Hit 'i' to test analog input (differential)
        Hit 'I' to test analog input (single ended)
        Hin 'n' to test analog input scan
        Hit 'e' to exit

 - yessss!
    
**Local resources**

 - [libhid](Res/libhid-0.2.16.zip)
 - [60-mcc.rules](Res/60-mcc.rules)
 - [MCCLIBHID.1.63.tgz](Res/MCCLIBHID.1.63.tgz)
        
##Python interface##

[Project](http://sourceforge.net/projects/pythonlabtools/) which somehow supports miniLab 1008 in Python - see [MeasurementComputingUSB.py](Res/MeasurementComputingUSB.py) module.

After compiling mcc-libhid simple test (test-minilab1008) is also prepared. This app depends on libmcchid.so. Interface of any shared lib can be inspected with following command:
     
    nm -D ./libmcchid.so   
     
Resault:
   
    00164f2c B __bss_start
    0002a520 R CJCGradients
    00027631 T cleanup_USB1208FS_Plus
    0000f4ec T cleanup_USB1208HS
    00022e9c T cleanup_USB1608FS_Plus
    0001516e T cleanup_USB1608G
    00013a20 T cleanup_USB1608HS
    0002876e T cleanup_USB2020
    00021fe6 T cleanup_USB20X
    00020e59 T cleanup_USB2416
    000246e0 T cleanup_USB2600
    000264a8 T cleanup_USB_CTR
             w __cxa_finalize
    00164f2c D _edata
    00165094 B _end
             U exp
             U fflush
    00028884 T _fini
             U __fprintf_chk
             U free
             U fwrite
             w __gmon_start__
             U hid_force_open
             U hid_new_HIDInterface
    0000aa98 T _init
    0001ff98 T intTOsint24
             w _ITM_deregisterTMCloneTable
             w _ITM_registerTMCloneTable
             w _Jv_RegisterClasses
             U malloc
    0000b3b0 T match_product
    0000b3df T match_serial_number
             U memcpy
             U __memcpy_chk
    000210c7 T NISTCalcTemp
    00021028 T NISTCalcVoltage
             U perror
    0000b451 T PMD_Find_Interface
    0000b713 T PMD_GetFeatureReport
    0000b66d T PMD_GetInputReport
    0000b524 T PMD_GetSerialNumber
    0000b5b9 T PMD_SendOutputReport
             U __printf_chk
             U putchar
             U puts
             U rint
    0001ff79 T sint24TOint
             U sleep
             U __stack_chk_fail
             U stderr
             U stdout
             U strncmp
             U strncpy
    00021180 T tc_temperature_USB2416
    00038c20 D ThermocoupleData
    0002a5a0 R TypeBReverse
    00038c80 D TypeBReverseTable
    0002a640 R TypeBTable0
    0002a5e0 R TypeBTable1
    00038ca0 D TypeBTables
    0002a6a0 R TypeEReverse
    00038cc0 D TypeEReverseTable
    0002a760 R TypeETable0
    0002a700 R TypeETable1
    00038ce0 D TypeETables
    0002aea0 R TypeJReverse
    00038e90 D TypeJReverseTable
    0002af80 R TypeJTable0
    0002af40 R TypeJTable1
    0002af00 R TypeJTable2
    00038ea0 D TypeJTables
    0002ad40 R TypeKReverse
    0002ad18 R TypeKReverseExtra
    00038e40 D TypeKReverseTable
    0002ae40 R TypeKTable0
    0002ade0 R TypeKTable1
    0002ada0 R TypeKTable2
    00038e60 D TypeKTables
    0002a7c0 R TypeNReverse
    00038d00 D TypeNReverseTable
    0002a8a0 R TypeNTable0
    0002a860 R TypeNTable1
    0002a820 R TypeNTable2
    00038d20 D TypeNTables
    0002ab80 R TypeRReverse
    00038de0 D TypeRReverseTable
    0002acc0 R TypeRTable0
    0002ac60 R TypeRTable1
    0002ac20 R TypeRTable2
    0002abe0 R TypeRTable3
    00038e00 D TypeRTables
    0002a9e0 R TypeSReverse
    00038d80 D TypeSReverseTable
    0002ab20 R TypeSTable0
    0002aac0 R TypeSTable1
    0002aa80 R TypeSTable2
    0002aa40 R TypeSTable3
    00038da0 D TypeSTables
    0002a900 R TypeTReverse
    00038d50 D TypeTReverseTable
    0002a9a0 R TypeTTable0
    0002a960 R TypeTTable1
    00038d60 D TypeTTables
    00020da1 T usbADCal_USB2416
    0002170f T usbAInBulkFlush_USB20X
    00012dc1 T usbAInConfigRead_USB1608HS
    0000eff8 T usbAInConfigR_USB1208HS
    00013e3a T usbAInConfigR_USB1608G
    000282e7 T usbAInConfigR_USB2020
    00024258 T usbAInConfigR_USB2600
    0000ef46 T usbAInConfig_USB1208HS
    00014bd5 T usbAInConfig_USB1608G
    00012d53 T usbAInConfig_USB1608HS
    000281b7 T usbAInConfig_USB2020
    00024152 T usbAInConfig_USB2600
    0001bfed T usbAInLoadQueue_miniLAB1008
    0000bd9d T usbAInLoadQueue_USB1208LS
    0001171b T usbAInLoadQueue_USB1608FS
    0001addf T usbAInLoadQueue_USB1616FS
    0001be45 T usbAIn_miniLAB1008
    000201c8 T usbAInMinPacerPeriod_USB2416
    000268ba T usbAInScanClearFIFO_USB1208FS_Plus
    00022425 T usbAInScanClearFIFO_USB1608FS_Plus
    00013f01 T usbAInScanClearFIFO_USB1608G
    00027c19 T usbAInScanClearFIFO_USB2020
    0002158e T usbAInScanClearFIFO_USB20X
    0002330c T usbAInScanClearFIFO_USB2600
    00026850 T usbAInScanConfigR_USB1208FS_Plus
    000223bb T usbAInScanConfigR_USB1608FS_Plus
    000267e6 T usbAInScanConfig_USB1208FS_Plus
    00022351 T usbAInScanConfig_USB1608FS_Plus
    00012e45 T usbAInScanConfig_USB1608HS
    0001c173 T usbAInScan_miniLAB1008
    00020170 T usbAInScanQueueRead_USB2416
    0002010a T usbAInScanQueueWrite_USB2416
    000273ce T usbAInScanRead_USB1208FS_Plus
    0000ee4f T usbAInScanRead_USB1208HS
    00022c39 T usbAInScanRead_USB1608FS_Plus
    00014ace T usbAInScanRead_USB1608G
    00028079 T usbAInScanRead_USB2020
    00021e31 T usbAInScanRead_USB20X
    0002405b T usbAInScanRead_USB2600
    00026912 T usbAInScanStart_USB1208FS_Plus
    0000e1bf T usbAInScanStart_USB1208HS
    0002247d T usbAInScanStart_USB1608FS_Plus
    00013d21 T usbAInScanStart_USB1608G
    00012ce5 T usbAInScanStart_USB1608HS
    00027f84 T usbAInScanStart_USB2020
    000215e6 T usbAInScanStart_USB20X
    00020856 T usbAInScanStart_USB2416
    00023f52 T usbAInScanStart_USB2600
    000200a1 T usbAInScanStatus_USB2416
    0002678e T usbAInScanStop_USB1208FS_Plus
    0000e287 T usbAInScanStop_USB1208HS
    000222f9 T usbAInScanStop_USB1608FS_Plus
    00013ea9 T usbAInScanStop_USB1608G
    000126b8 T usbAInScanStop_USB1608HS
    00027bc1 T usbAInScanStop_USB2020
    00021536 T usbAInScanStop_USB20X
    00020049 T usbAInScanStop_USB2416
    000232b4 T usbAInScanStop_USB2600
    0000d0c6 T usbAInScan_USB1208FS
    0000d537 T usbAInScan_USB1208FS_SE
    0000bf23 T usbAInScan_USB1208LS
    0000fe21 T usbAInScan_USB1408FS
    00010290 T usbAInScan_USB1408FS_SE
    000111a9 T usbAInScan_USB1608FS
    00012710 T usbAInScan_USB1608HS
    0001a8d1 T usbAInScan_USB1616FS
    0001697f T usbAinScan_USBTC_AI
    0001c110 T usbAInStop_miniLAB1008
    0000d081 T usbAInStop_USB1208FS
    0000bec0 T usbAInStop_USB1208LS
    0000fddc T usbAInStop_USB1408FS
    00011164 T usbAInStop_USB1608FS
    0001a88c T usbAInStop_USB1616FS
    0000cf52 T usbAIn_USB1208FS
    00026722 T usbAIn_USB1208FS_Plus
    0000e161 T usbAIn_USB1208HS
    0000bbf5 T usbAIn_USB1208LS
    0000fcae T usbAIn_USB1408FS
    00010f62 T usbAIn_USB1608FS
    0002229a T usbAIn_USB1608FS_Plus
    00013cb1 T usbAIn_USB1608G
    00012f4e T usbAIn_USB1608HS
    0001a686 T usbAIn_USB1616FS
    00027b51 T usbAIn_USB2020
    000214d8 T usbAIn_USB20X
    0001ffb5 T usbAIn_USB2416
    00023256 T usbAIn_USB2600
    000168d6 T usbAin_USBTC_AI
    0000d46b T usbALoadQueue_USB1208FS
    000101c4 T usbALoadQueue_USB1408FS
    000130a5 T usbAOutConfigRead_USB1608HS
    0001302f T usbAOutConfig_USB1608HS
    0001d83d T usbAOutConfig_USB31XX
    0001bf71 T usbAOut_miniLAB1008
    000128a1 T usbAOutRead_USB1608HS
    00026a7d T usbAOutR_USB1208FS_Plus
    0000e3cb T usbAOutR_USB1208HS
    00014045 T usbAOutR_USB1608GX_2AO
    00023462 T usbAOutR_USB26X7
    00026b42 T usbAOutScanClearFIFO_USB1208FS_Plus
    0000e4b7 T usbAOutScanClearFIFO_USB1208HS
    00014131 T usbAOutScanClearFIFO_USB1608GX_2AO
    0002354e T usbAOutScanClearFIFO_USB26X7
    00012a79 T usbAOutScanConfig_USB1608HS
    00026b9a T usbAOutScanStart_USB1208FS_Plus
    0000e50f T usbAOutScanStart_USB1208HS
    00014189 T usbAOutScanStart_USB1608GX_2AO
    00012fc1 T usbAOutScanStart_USB1608HS
    00020431 T usbAOutScanStart_USB2416_4AO
    000235a6 T usbAOutScanStart_USB26X7
    000203c8 T usbAOutScanStatus_USB2416_4AO
    00026aea T usbAOutScanStop_USB1208FS_Plus
    0000e45f T usbAOutScanStop_USB1208HS
    000140d9 T usbAOutScanStop_USB1608GX_2AO
    000128f9 T usbAOutScanStop_USB1608HS
    00020370 T usbAOutScanStop_USB2416_4AO
    000234f6 T usbAOutScanStop_USB26X7
    0000cae8 T usbAOutScan_USB1208FS
    0000f848 T usbAOutScan_USB1408FS
    00012951 T usbAOutScan_USB1608HS
    0000cf0d T usbAOutStop_USB1208FS
    0000fc69 T usbAOutStop_USB1408FS
    0001d49d T usbAOutSync_USB31XX
    0000ca82 T usbAOut_USB1208FS
    00026a15 T usbAOut_USB1208FS_Plus
    0000e2df T usbAOut_USB1208HS
    0000bd21 T usbAOut_USB1208LS
    0000f7e2 T usbAOut_USB1408FS
    00013f59 T usbAOut_USB1608GX_2AO
    00012849 T usbAOut_USB1608HS
    0002176f T usbAOut_USB20X
    0002050e T usbAOut_USB2416_4AO
    00023364 T usbAOut_USB26X7
    0001d3c2 T usbAOut_USB31XX
    0001eadc T usbArm_USB4303
    0001c7b7 T usbBlink_miniLAB1008
    000157b3 T usbBlink_USB1024LS
    0000da9a T usbBlink_USB1208FS
    000272b2 T usbBlink_USB1208FS_Plus
    0000f0a1 T usbBlink_USB1208HS
    0000c551 T usbBlink_USB1208LS
    000107f1 T usbBlink_USB1408FS
    00011886 T usbBlink_USB1608FS
    00022b1d T usbBlink_USB1608FS_Plus
    00014a0a T usbBlink_USB1608G
    00013113 T usbBlink_USB1608HS
    0001af57 T usbBlink_USB1616FS
    00027ec0 T usbBlink_USB2020
    00021f86 T usbBlink_USB20X
    0002076f T usbBlink_USB2416
    00023e8e T usbBlink_USB2600
    0001d5d7 T usbBlink_USB31XX
    0001f1e6 T usbBlink_USB4303
    00018758 T usbBlink_USB52XX
    00025f52 T usbBlink_USB_CTR
    0001fc82 T usbBlink_USBDIO24
    0001a006 T usbBlink_USBDIO96H
    0001e430 T usbBlink_USBERB
    0001e05a T usbBlink_USBPDISO8
    0001ccfa T usbBlink_USBSSR
    00015d48 T usbBlink_USBTC
    00016a5c T usbBlink_USBTC_AI
    00017aa8 T usbBlink_USBTEMP
    00011c0e T usbBuildCalTable_USB1608FS
    0001b244 T usbBuildCalTable_USB1616FS
    00026dda T usbBuildGainTable_DE_USB1208FS_Plus
    00026e97 T usbBuildGainTable_SE_USB1208FS_Plus
    0000ec40 T usbBuildGainTable_USB1208HS
    0000eccc T usbBuildGainTable_USB1208HS_4AO
    000226bf T usbBuildGainTable_USB1608FS_Plus
    000148ba T usbBuildGainTable_USB1608G
    00014924 T usbBuildGainTable_USB1608GX_2AO
    00013482 T usbBuildGainTable_USB1608HS
    00027df9 T usbBuildGainTable_USB2020
    0002194f T usbBuildGainTable_USB20X
    00020b6d T usbBuildGainTable_USB2416
    00020be7 T usbBuildGainTable_USB2416_4AO
    00023d5d T usbBuildGainTable_USB2600
    00023dc7 T usbBuildGainTable_USB26X7
             U usb_bulk_read
             U usb_bulk_write
    00020d41 T usbCalConfig_USB2416
    0001737c T usbCalConfig_USBTC_AI
    00018d82 T usbCalibrate_USB52XX
    0001622d T usbCalibrate_USBTC
    0001745b T usbCalibrate_USBTC_AI
    0001802f T usbCalibrate_USBTEMP
    000206f3 T usbCJC_USB2416
             U usb_claim_interface
             U usb_clear_halt
             U usb_close
    00017573 T usbConfigAlarm_USBTC_AI
    00019a31 T usbConfigureAlarm_USB52XX
    00019111 T usbConfigureLogging_USB52XX
             U usb_control_msg
    00024d33 T usbCounterDebounceR_USB_CTR
    00024db2 T usbCounterDebounceW_USB_CTR
    00024e34 T usbCounterGateConfigR_USB_CTR
    00024eb3 T usbCounterGateConfigW_USB_CTR
    00026c93 T usbCounterInit_USB1208FS_Plus
    0000e5b3 T usbCounterInit_USB1208HS
    00022578 T usbCounterInit_USB1608FS_Plus
    0001422d T usbCounterInit_USB1608G
    00012bcc T usbCounterInit_USB1608HS
    000217f8 T usbCounterInit_USB20X
    00020618 T usbCounterInit_USB2416
    0002365c T usbCounterInit_USB2600
    00025198 T usbCounterLimitValuesR_USB_CTR
    0002522f T usbCounterLimitValuesW_USB_CTR
    00024b31 T usbCounterModeR_USB_CTR
    00024bb0 T usbCounterModeW_USB_CTR
    00024c32 T usbCounterOptionsR_USB_CTR
    00024cb1 T usbCounterOptionsW_USB_CTR
    00024f35 T usbCounterOutConfigR_USB_CTR
    00024fb4 T usbCounterOutConfigW_USB_CTR
    00025036 T usbCounterOutValuesR_USB_CTR
    000250df T usbCounterOutValuesW_USB_CTR
    000252d6 T usbCounterParamsR_USB_CTR
    000253ad T usbCounterParamsW_USB_CTR
    00024a11 T usbCounterSet_USB_CTR
    00026ceb T usbCounter_USB1208FS_Plus
    0000e60c T usbCounter_USB1208HS
    000225d0 T usbCounter_USB1608FS_Plus
    00014286 T usbCounter_USB1608G
    00012c24 T usbCounter_USB1608HS
    00021850 T usbCounter_USB20X
    00020678 T usbCounter_USB2416
    000236c7 T usbCounter_USB2600
    00024aa0 T usbCounter_USB_CTR
    0001bd28 T usbDBitIn_miniLAB1008
    000154f7 T usbDBitIn_USB1024LS
    0000bad8 T usbDBitIn_USB1208LS
    0001d29e T usbDBitIn_USB31XX
    0001e961 T usbDBitIn_USB4303
    0001f9c4 T usbDBitIn_USBDIO24
    00019cd6 T usbDBitIn_USBDIO96H
    0001e1f1 T usbDBitIn_USBERB
    0001de21 T usbDBitIn_USBPDISO8
    0001cac1 T usbDBitIn_USBSSR
    0001bdca T usbDBitOut_miniLAB1008
    00015597 T usbDBitOut_USB1024LS
    0000bb7a T usbDBitOut_USB1208LS
    0001d36d T usbDBitOut_USB31XX
    0001e9dd T usbDBitOut_USB4303
    0001fa66 T usbDBitOut_USBDIO24
    00019d5c T usbDBitOut_USBDIO96H
    0001e275 T usbDBitOut_USBERB
    0001dea5 T usbDBitOut_USBPDISO8
    0001cb45 T usbDBitOut_USBSSR
    00010d6d T usbDConfigBit_USB1608FS
    0001a49d T usbDConfigBit_USB1616FS
    0001d1cd T usbDConfigBit_USB31XX
    000183dd T usbDConfigBit_USB52XX
    000159cd T usbDConfigBit_USBTC
    0001660d T usbDConfigBit_USBTC_AI
    0001772d T usbDConfigBit_USBTEMP
    0001bba0 T usbDConfigPort_miniLAB1008
    00015310 T usbDConfigPort_USB1024LS
    0000c930 T usbDConfigPort_USB1208FS
    0000b950 T usbDConfigPort_USB1208LS
    0000f690 T usbDConfigPort_USB1408FS
    00010d20 T usbDConfigPort_USB1608FS
    0001a450 T usbDConfigPort_USB1616FS
    0001d180 T usbDConfigPort_USB31XX
    00018390 T usbDConfigPort_USB52XX
    0001f7f0 T usbDConfigPort_USBDIO24
    00019bb0 T usbDConfigPort_USBDIO96H
    00015980 T usbDConfigPort_USBTC
    000165c0 T usbDConfigPort_USBTC_AI
    000176e0 T usbDConfigPort_USBTEMP
    000197b9 T usbDeleteFile_USB52XX
             U usb_device
    0000b7ab T usb_device_find_USB_MCC
    00027517 T usbDFU_USB1208FS_Plus
    00022d82 T usbDFU_USB1608FS_Plus
    00010e3e T usbDInBit_USB1608FS
    00012592 T usbDInBit_USB1608HS
    0001a568 T usbDInBit_USB1616FS
    000184ae T usbDInBit_USB52XX
    00015a9e T usbDInBit_USBTC
    000166de T usbDInBit_USBTC_AI
    000177fe T usbDInBit_USBTEMP
    0001bc13 T usbDIn_miniLAB1008
    00015383 T usbDIn_USB1024LS
    0000c985 T usbDIn_USB1208FS
    0000b9c3 T usbDIn_USB1208LS
    0000f6e5 T usbDIn_USB1408FS
    00010dc2 T usbDIn_USB1608FS
    00012530 T usbDIn_USB1608HS
    0001a4f2 T usbDIn_USB1616FS
    0001fe50 T usbDIn_USB2416
    0001d222 T usbDIn_USB31XX
    0001e8a0 T usbDIn_USB4303
    00018432 T usbDIn_USB52XX
    0001f863 T usbDIn_USBDIO24
    00019c05 T usbDIn_USBDIO96H
    0001e120 T usbDIn_USBERB
    0001dd50 T usbDIn_USBPDISO8
    0001c9f0 T usbDIn_USBSSR
    00015a22 T usbDIn_USBTC
    00016662 T usbDIn_USBTC_AI
    00017782 T usbDIn_USBTEMP
    0001eb31 T usbDisarm_USB4303
    0002663f T usbDLatchR_USB1208FS_Plus
    0000e080 T usbDLatchR_USB1208HS
    000221b9 T usbDLatchR_USB1608FS_Plus
    00013bd0 T usbDLatchR_USB1608G
    00027a70 T usbDLatchR_USB2020
    000213f7 T usbDLatchR_USB20X
    00023173 T usbDLatchR_USB2600
    00024930 T usbDLatchR_USB_CTR
    000266b6 T usbDLatchW_USB1208FS_Plus
    0000e0f6 T usbDLatchW_USB1208HS
    0002222f T usbDLatchW_USB1608FS_Plus
    00013c46 T usbDLatchW_USB1608G
    00027ae6 T usbDLatchW_USB2020
    0002146d T usbDLatchW_USB20X
    000231ea T usbDLatchW_USB2600
    000249a6 T usbDLatchW_USB_CTR
    00010f0d T usbDOutBit_USB1608FS
    00012650 T usbDOutBit_USB1608HS
    0001a631 T usbDOutBit_USB1616FS
    0001857d T usbDOutBit_USB52XX
    00015b6d T usbDOutBit_USBTC
    000167ad T usbDOutBit_USBTC_AI
    000178cd T usbDOutBit_USBTEMP
    0001bcb5 T usbDOut_miniLAB1008
    0001ff1b T usbDOutR_USB2416
    00015445 T usbDOut_USB1024LS
    0000ca2d T usbDOut_USB1208FS
    0000ba65 T usbDOut_USB1208LS
    0000f78d T usbDOut_USB1408FS
    00010ec0 T usbDOut_USB1608FS
    000125f0 T usbDOut_USB1608HS
    0001a5e4 T usbDOut_USB1616FS
    0001feb3 T usbDOut_USB2416
    0001d320 T usbDOut_USB31XX
    0001e914 T usbDOut_USB4303
    00018530 T usbDOut_USB52XX
    0001f912 T usbDOut_USBDIO24
    00019c81 T usbDOut_USBDIO96H
    0001e19c T usbDOut_USBERB
    0001ddcc T usbDOut_USBPDISO8
    0001ca6c T usbDOut_USBSSR
    00015b20 T usbDOut_USBTC
    00016760 T usbDOut_USBTC_AI
    00017880 T usbDOut_USBTEMP
    000265cf T usbDPort_USB1208FS_Plus
    0000e011 T usbDPort_USB1208HS
    0002214a T usbDPort_USB1608FS_Plus
    00013b61 T usbDPort_USB1608G
    00027a01 T usbDPort_USB2020
    00021388 T usbDPort_USB20X
    00023103 T usbDPort_USB2600
    000248c1 T usbDPort_USB_CTR
    000264ee T usbDTristateR_USB1208FS_Plus
    0000df30 T usbDTristateR_USB1208HS
    0002206b T usbDTristateR_USB1608FS_Plus
    00013a80 T usbDTristateR_USB1608G
    00027920 T usbDTristateR_USB2020
    000212a9 T usbDTristateR_USB20X
    00023020 T usbDTristateR_USB2600
    000247e0 T usbDTristateR_USB_CTR
    00026563 T usbDTristateW_USB1208FS_Plus
    0000dfa6 T usbDTristateW_USB1208HS
    000220df T usbDTristateW_USB1608FS_Plus
    00013af6 T usbDTristateW_USB1608G
    00027996 T usbDTristateW_USB2020
    0002131d T usbDTristateW_USB20X
    00023097 T usbDTristateW_USB2600
    00024856 T usbDTristateW_USB_CTR
             U usb_find_busses
             U usb_find_devices
    00019405 T usbFormatCard_USB52XX
    0000f2c4 T usbFPGAConfig_USB1208HS
    00014f46 T usbFPGAConfig_USB1608G
    00028546 T usbFPGAConfig_USB2020
    000244b8 T usbFPGAConfig_USB2600
    00026280 T usbFPGAConfig_USB_CTR
    0000f321 T usbFPGAData_USB1208HS
    00014fa3 T usbFPGAData_USB1608G
    000285a3 T usbFPGAData_USB2020
    00024515 T usbFPGAData_USB2600
    000262dd T usbFPGAData_USB_CTR
    0000f494 T usbFPGAVersion_USB1208HS
    00015116 T usbFPGAVersion_USB1608G
    00028716 T usbFPGAVersion_USB2020
    00024688 T usbFPGAVersion_USB2600
    00026450 T usbFPGAVersion_USB_CTR
    0001edb3 T usbGet9513Config_USB4303
    00019ac4 T usbGetAlarmConfig_USB52XX
    00017606 T usbGetAlarmConfig_USBTC_AI
    0000dd78 T usbGetAll_USB1208FS
    00010acf T usbGetAll_USB1408FS
    0001234f T usbGetAll_USB1608FS
    0001a119 T usbGetAll_USBDIO96H
    0001ce5f T usbGetAll_USBSSR
    00018e0f T usbGetBurnoutStatus_USB52XX
    000162ba T usbGetBurnoutStatus_USBTC
    0001707c T usbGetBurnoutStatus_USBTC_AI
    000180bc T usbGetBurnoutStatus_USBTEMP
             U usb_get_busses
    000173d9 T usbGetCalSteps_USBTC_AI
    000192e3 T usbGetDeviceTime_USB52XX
    00019712 T usbGetDiskInfo_USB52XX
             U usb_get_driver_np
    00019866 T usbGetFileHeader_USB52XX
    00019638 T usbGetFileInfo_USB52XX
    00019468 T usbGetFirstFile_USB52XX
    0001c8f1 T usbGetID_miniLAB1008
    00015879 T usbGetID_USB1024LS
    0000c68a T usbGetID_USB1208LS
    0001fd48 T usbGetID_USBDIO24
    00018c2b T usbGetItem_USB52XX
    000160d6 T usbGetItem_USBTC
    00016f25 T usbGetItem_USBTC_AI
    00017ed8 T usbGetItem_USBTEMP
    000191cd T usbGetLoggingConfig_USB52XX
    0000b76d T usb_get_max_packet_size
    00013549 T usbGetMemAddress_USB1608HS
    00019550 T usbGetNextFile_USB52XX
    0001eee9 T usbGetRegister_USB4303
    000274b9 T usbGetSerialNumber_USB1208FS_Plus
    0000f266 T usbGetSerialNumber_USB1208HS
    00022d24 T usbGetSerialNumber_USB1608FS_Plus
    00014ee8 T usbGetSerialNumber_USB1608G
    000132b9 T usbGetSerialNumber_USB1608HS
    000284e8 T usbGetSerialNumber_USB2020
    00021f28 T usbGetSerialNumber_USB20X
    00020a05 T usbGetSerialNumber_USB2416
    0002445a T usbGetSerialNumber_USB2600
    00026222 T usbGetSerialNumber_USB_CTR
    0000dbbe T usbGetStatus_USB1208FS
    00010915 T usbGetStatus_USB1408FS
    000119aa T usbGetStatus_USB1608FS
    0001b07b T usbGetStatus_USB1616FS
    0001d6b6 T usbGetStatus_USB31XX
    0001f278 T usbGetStatus_USB4303
    000187e2 T usbGetStatus_USB52XX
    0001a090 T usbGetStatus_USBDIO96H
    0001e4ba T usbGetStatus_USBERB
    0001cd84 T usbGetStatus_USBSSR
    00015dd2 T usbGetStatus_USBTC
    00016ae6 T usbGetStatus_USBTC_AI
    00017b32 T usbGetStatus_USBTEMP
             U usb_get_string_simple
    0001e594 T usbGetTemp_USBERB
    00020abb T usbGetVersion_USB2416
             U usb_init
    0000f390 T usbInit_1208HS
    00015012 T usbInit_1608G
    00024584 T usbInit_2600
    0001c5eb T usbInitCounter_miniLAB1008
    00015612 T usbInitCounter_USB1024LS
    0000d9a5 T usbInitCounter_USB1208FS
    0000c385 T usbInitCounter_USB1208LS
    000106fc T usbInitCounter_USB1408FS
    00011791 T usbInitCounter_USB1608FS
    0001ae62 T usbInitCounter_USB1616FS
    0001d4e2 T usbInitCounter_USB31XX
    0001fae1 T usbInitCounter_USBDIO24
    00019db9 T usbInitCounter_USBDIO96H
    00016802 T usbInitCounter_USBTC_AI
    0002634c T usbInit_CTR
    00028612 T usbInit_USB2020
    0001f31a T usbInterruptConfig_USB4303
             U usb_interrupt_read
             U usb_interrupt_write
    0001ea32 T usbLoad_USB4303
    0002756f T usbMBDCommand_USB1208FS_Plus
    00022dda T usbMBDCommand_USB1608FS_Plus
    000275d8 T usbMBDRaw_USB1208FS_Plus
    00022e43 T usbMBDRaw_USB1608FS_Plus
    0000eb7e T usbMemAddressR_USB1208HS
    000147f8 T usbMemAddressR_USB1608G
    00027d37 T usbMemAddressR_USB2020
    00023c9b T usbMemAddressR_USB2600
    00025e33 T usbMemAddressR_USB_CTR
    0000ebdf T usbMemAddressW_USB1208HS
    00014859 T usbMemAddressW_USB1608G
    00027d98 T usbMemAddressW_USB2020
    00023cfc T usbMemAddressW_USB2600
    00025e94 T usbMemAddressW_USB_CTR
    0000eab8 T usbMemoryR_USB1208HS
    00014732 T usbMemoryR_USB1608G
    00027c71 T usbMemoryR_USB2020
    00023bd5 T usbMemoryR_USB2600
    00025d6d T usbMemoryR_USB_CTR
    0000eb25 T usbMemoryW_USB1208HS
    0001479f T usbMemoryW_USB1608G
    00027cde T usbMemoryW_USB2020
    00023c42 T usbMemoryW_USB2600
    00025dda T usbMemoryW_USB_CTR
    0000ed36 T usbMemWriteEnable_USB1208HS
    000149ad T usbMemWriteEnable_USB1608G
    00027e63 T usbMemWriteEnable_USB2020
    00023e31 T usbMemWriteEnable_USB2600
    00025ef5 T usbMemWriteEnable_USB_CTR
             U usb_open
    0001b75c T usbPrepareDownload_USB1616FS
    0001da46 T usbPrepareDownload_USB31XX
    0001f3d5 T usbPrepareDownload_USB4303
    00018eba T usbPrepareDownload_USB52XX
    0001a1da T usbPrepareDownload_USBDIO96H
    0001e617 T usbPrepareDownload_USBERB
    0001cefa T usbPrepareDownload_USBSSR
    00016365 T usbPrepareDownload_USBTC
    00017127 T usbPrepareDownload_USBTC_AI
    00018135 T usbPrepareDownload_USBTEMP
    0001f6a3 T usbRead9513Command_USB4303
    0001f774 T usbRead9513Data_USB4303
    00026d4f T usbReadCalMemory_USB1208FS_Plus
    00022634 T usbReadCalMemory_USB1608FS_Plus
    000218b4 T usbReadCalMemory_USB20X
    0001b860 T usbReadChecksum_USB1616FS
    0001b997 T usbReadCode_USB1616FS
    0001f544 T usbReadCode_USB4303
    00018fb3 T usbReadCode_USB52XX
    0001a2d5 T usbReadCode_USBDIO96H
    0001e712 T usbReadCode_USBERB
    0001cff5 T usbReadCode_USBSSR
    000164d2 T usbReadCode_USBTC
    00017294 T usbReadCode_USBTC_AI
    000182a2 T usbReadCode_USBTEMP
    0001c64e T usbReadCounter_miniLAB1008
    00015675 T usbReadCounter_USB1024LS
    0000d9ea T usbReadCounter_USB1208FS
    0000c3e8 T usbReadCounter_USB1208LS
    00010741 T usbReadCounter_USB1408FS
    000117d6 T usbReadCounter_USB1608FS
    0001aea7 T usbReadCounter_USB1616FS
    0001d527 T usbReadCounter_USB31XX
    0001fb44 T usbReadCounter_USBDIO24
    00019dfe T usbReadCounter_USBDIO96H
    00016847 T usbReadCounter_USBTC_AI
    000199ce T usbReadFileAbort_USB52XX
    0001996b T usbReadFileAck_USB52XX
    0001f007 T usbReadFreq_USB4303
    0002717c T usbReadMBDMemory_USB1208FS_Plus
    000229e7 T usbReadMBDMemory_USB1608FS_Plus
    00021c3f T usbReadMBDMemory_USB20X
    0001c6df T usbReadMemory_miniLAB1008
    00015706 T usbReadMemory_USB1024LS
    0000dc4c T usbReadMemory_USB1208FS
    0000c479 T usbReadMemory_USB1208LS
    000109a3 T usbReadMemory_USB1408FS
    00011a38 T usbReadMemory_USB1608FS
    0001336f T usbReadMemory_USB1608HS
    0001b0f9 T usbReadMemory_USB1616FS
    00020b13 T usbReadMemory_USB2416
    0001d744 T usbReadMemory_USB31XX
    0001f096 T usbReadMemory_USB4303
    00018854 T usbReadMemory_USB52XX
    0001fbd5 T usbReadMemory_USBDIO24
    00019eae T usbReadMemory_USBDIO96H
    0001e2d2 T usbReadMemory_USBERB
    0001df02 T usbReadMemory_USBPDISO8
    0001cba2 T usbReadMemory_USBSSR
    00015e44 T usbReadMemory_USBTC
    00016b58 T usbReadMemory_USBTC_AI
    00017ba4 T usbReadMemory_USBTEMP
    0001eb86 T usbRead_USB4303
    00027046 T usbReadUserMemory_USB1208FS_Plus
    000228b1 T usbReadUserMemory_USB1608FS_Plus
    00021b09 T usbReadUserMemory_USB20X
             U usb_release_interface
    0001ef6d T usbReset9513_USB4303
    0001c81a T usbReset_miniLAB1008
    00015816 T usbReset_USB1024LS
    0000dadf T usbReset_USB1208FS
    00027312 T usbReset_USB1208FS_Plus
    0000ed93 T usbReset_USB1208HS
    0000c5b4 T usbReset_USB1208LS
    00010836 T usbReset_USB1408FS
    000118cb T usbReset_USB1608FS
    00022b7d T usbReset_USB1608FS_Plus
    00014d5e T usbReset_USB1608G
    00013173 T usbReset_USB1608HS
    0001af9c T usbReset_USB1616FS
    00028353 T usbReset_USB2020
    00021d75 T usbReset_USB20X
    00020ce9 T usbReset_USB2416
    000242c4 T usbReset_USB2600
    0001d624 T usbReset_USB31XX
    0001f233 T usbReset_USB4303
    0001879d T usbReset_USB52XX
    00026112 T usbReset_USB_CTR
    0001fce5 T usbReset_USBDIO24
    0001a04b T usbReset_USBDIO96H
    0001e475 T usbReset_USBERB
    0001cd3f T usbReset_USBSSR
    00015d8d T usbReset_USBTC
    00016aa1 T usbReset_USBTC_AI
    00017aed T usbReset_USBTEMP
    0001ea87 T usbSave_USB4303
    0002568d T usbScanBulkFlush_USB_CTR
    00025635 T usbScanClearFIFO_USB_CTR
    00025478 T usbScanConfigR_USB_CTR
    000254e0 T usbScanConfigW_USB_CTR
    00026016 T usbScanRead_USB_CTR
    00025539 T usbScanStart_USB_CTR
    000255dd T usbScanStop_USB_CTR
    0001efba T usbSelect9513Clock_USB4303
    0001ed56 T usbSet9513Config_USB4303
             U usb_set_configuration
    000193a7 T usbSetDeviceTime_USB52XX
    0001c983 T usbSetID_miniLAB1008
    00015909 T usbSetID_USB1024LS
    0000c71c T usbSetID_USB1208LS
    0001fdda T usbSetID_USBDIO24
    00018a43 T usbSetItem_USB5201
    00018ae6 T usbSetItem_USB5203
    00016033 T usbSetItem_USBTC
    00016d47 T usbSetItem_USBTC_AI
    00017d93 T usbSetItem_USBTEMP
    00016e36 T usbSetItem_USBTEMP_AI
    00013421 T usbSetMemAddress_USB1608HS
    0001ee2f T usbSetOscOut_USB4303
    0001ee84 T usbSetRegister_USB4303
    00013317 T usbSetSerialNumber_USB1608HS
    00020a63 T usbSetSerialNumber_USB2416
    0000db71 T usbSetSync_USB1208FS
    000108c8 T usbSetSync_USB1408FS
    0001195d T usbSetSync_USB1608FS
    0001b02e T usbSetSync_USB1616FS
    0001d669 T usbSetSync_USB31XX
    0001ec46 T usbSetToggle_USB4303
    0001c87d T usbSetTrigger_miniLAB1008
    0000db24 T usbSetTrigger_USB1208FS
    0000c617 T usbSetTrigger_USB1208LS
    0001087b T usbSetTrigger_USB1408FS
    00011910 T usbSetTrigger_USB1608FS
    0001afe1 T usbSetTrigger_USB1616FS
    0002736a T usbStatus_USB1208FS_Plus
    0000edeb T usbStatus_USB1208HS
    00022bd5 T usbStatus_USB1608FS_Plus
    00014a6a T usbStatus_USB1608G
    00012c88 T usbStatus_USB1608HS
    00027f20 T usbStatus_USB2020
    00021dcd T usbStatus_USB20X
    000207f4 T usbStatus_USB2416
    00023eee T usbStatus_USB2600
    00025fb2 T usbStatus_USB_CTR
    0001ecd2 T usbStep_USB4303
    00020df9 T usbTCCalMeasure
    00013234 T usbTemperature_
    0000f1b9 T usbTemperature_USB1208HS
    00014e6e T usbTemperature_USB1608G
    0002845b T usbTemperature_USB2020
    000243d4 T usbTemperature_USB2600
    0000e687 T usbTimerControlR_USB1208HS
    00014301 T usbTimerControlR_USB1608G
    00023760 T usbTimerControlR_USB2600
    000256e6 T usbTimerControlR_USB_CTR
    0000e6df T usbTimerControlW_USB1208HS
    00014359 T usbTimerControlW_USB1608G
    000237b9 T usbTimerControlW_USB2600
    00025765 T usbTimerControlW_USB_CTR
    0000e8a0 T usbTimerCountR_USB1208HS
    0001451a T usbTimerCountR_USB1608G
    000239ad T usbTimerCountR_USB2600
    000259e8 T usbTimerCountR_USB_CTR
    0000e8f8 T usbTimerCountW_USB1208HS
    00014572 T usbTimerCountW_USB1608G
    00023a18 T usbTimerCountW_USB2600
    00025a67 T usbTimerCountW_USB_CTR
    0000e954 T usbTimerDelayR_USB1208HS
    000145ce T usbTimerDelayR_USB1608G
    00023a71 T usbTimerDelayR_USB2600
    00025ae6 T usbTimerDelayR_USB_CTR
    0000e9ac T usbTimerDelayW_USB1208HS
    00014626 T usbTimerDelayW_USB1608G
    00023aca T usbTimerDelayW_USB2600
    00025b65 T usbTimerDelayW_USB_CTR
    0000ea08 T usbTimerParamsR_USB1208HS
    00014682 T usbTimerParamsR_USB1608G
    00023b23 T usbTimerParamsR_USB2600
    00025be4 T usbTimerParamsR_USB_CTR
    0000ea60 T usbTimerParamsW_USB1208HS
    000146da T usbTimerParamsW_USB1608G
    00023b7c T usbTimerParamsW_USB2600
    00025caf T usbTimerParamsW_USB_CTR
    0000e738 T usbTimerPeriodR_USB1208HS
    000143b2 T usbTimerPeriodR_USB1608G
    00023813 T usbTimerPeriodR_USB2600
    000257ec T usbTimerPeriodR_USB_CTR
    0000e790 T usbTimerPeriodW_USB1208HS
    0001440a T usbTimerPeriodW_USB1608G
    0002387e T usbTimerPeriodW_USB2600
    0002586b T usbTimerPeriodW_USB_CTR
    0000e7ec T usbTimerPulseWidthR_USB1208HS
    00014466 T usbTimerPulseWidthR_USB1608G
    000238e9 T usbTimerPulseWidthR_USB2600
    000258ea T usbTimerPulseWidthR_USB_CTR
    0000e844 T usbTimerPulseWidthW_USB1208HS
    000144be T usbTimerPulseWidthW_USB1608G
    00023954 T usbTimerPulseWidthW_USB2600
    00025969 T usbTimerPulseWidthW_USB_CTR
    0001867b T usbTinScan_USB52XX
    00015c6b T usbTinScan_USBTC
    000179cb T usbTinScan_USBTEMP
    000185d2 T usbTin_USB52XX
    00015bc2 T usbTin_USBTC
    00017922 T usbTin_USBTEMP
    0000f161 T usbTriggerConfigR_USB1208HS
    00014e16 T usbTriggerConfigR_USB1608G
    00028403 T usbTriggerConfigR_USB2020
    0002437c T usbTriggerConfigR_USB2600
    000261ca T usbTriggerConfigR_USB_CTR
    0000f101 T usbTriggerConfig_USB1208HS
    00014db6 T usbTriggerConfig_USB1608G
    000283ab T usbTriggerConfig_USB2020
    0002431c T usbTriggerConfig_USB2600
    0002616a T usbTriggerConfig_USB_CTR
    000131cb T usbTrigger_USB1608HS
    00013606 T usbUpdateAddress_USB1608HS
    000137df T usbUpdateChecksumReset_USB1608HS
    00013782 T usbUpdateChecksum_USB1608HS
    0001b952 T usbUpdateCode_USB1616FS
    0001365e T usbUpdateData_USB1608HS
    000135a1 T usbUpdateMode_USB1608HS
    00013713 T usbUpdateVersion_USB1608HS
    0001f64e T usbWrite9513Command_USB4303
    0001f71f T usbWrite9513Data_USB4303
    00026f11 T usbWriteCalMemory_USB1208FS_Plus
    0002277c T usbWriteCalMemory_USB1608FS_Plus
    000219d4 T usbWriteCalMemory_USB20X
    0001b7a6 T usbWriteCode_USB1616FS
    0001da90 T usbWriteCode_USB31XX
    0001f41f T usbWriteCode_USB4303
    00018f0c T usbWriteCode_USB52XX
    0001a224 T usbWriteCode_USBDIO96H
    0001e661 T usbWriteCode_USBERB
    0001cf44 T usbWriteCode_USBSSR
    000163b7 T usbWriteCode_USBTC
    00017179 T usbWriteCode_USBTC_AI
    00018187 T usbWriteCode_USBTEMP
    00027217 T usbWriteMBDMemory_USB1208FS_Plus
    00022a82 T usbWriteMBDMemory_USB1608FS_Plus
    00021cda T usbWriteMBDMemory_USB20X
    0000dcb3 T usbWriteMemory_USB1208FS
    00010a0a T usbWriteMemory_USB1408FS
    0001228a T usbWriteMemory_USB1608FS
    000133c8 T usbWriteMemory_USB1608HS
    0001b697 T usbWriteMemory_USB1616FS
    00020c52 T usbWriteMemory_USB2416
    0001d92a T usbWriteMemory_USB31XX
    0001f12e T usbWriteMemory_USB4303
    0001895c T usbWriteMemory_USB52XX
    00019f41 T usbWriteMemory_USBDIO96H
    0001e36b T usbWriteMemory_USBERB
    0001df95 T usbWriteMemory_USBPDISO8
    0001cc35 T usbWriteMemory_USBSSR
    00015f4c T usbWriteMemory_USBTC
    00016c60 T usbWriteMemory_USBTC_AI
    00017cac T usbWriteMemory_USBTEMP
    0001b8de T usbWriteSerial_USB1616FS
    0001db7c T usbWriteSerial_USB31XX
    0001f4d0 T usbWriteSerial_USB4303
    0001909b T usbWriteSerial_USB52XX
    0001a3df T usbWriteSerial_USBDIO96H
    0001e81c T usbWriteSerial_USBERB
    0001e09f T usbWriteSerial_USBPDISO8
    0001d0ff T usbWriteSerial_USBSSR
    0001645e T usbWriteSerial_USBTC
    00017220 T usbWriteSerial_USBTC_AI
    0001822e T usbWriteSerial_USBTEMP
    000270e1 T usbWriteUserMemory_USB1208FS_Plus
    0002294c T usbWriteUserMemory_USB1608FS_Plus
    00021ba4 T usbWriteUserMemory_USB20X
    00010c01 T volts_1408FS
    00010bd3 T volts_1408FS_SE
    0000de0f T volts_FS
    0000c787 T volts_LS
    0000dde7 T volts_SE
    00020eaf T voltsTos16_USB2416_4AO
    0000f542 T voltsTou12_USB1208HS_AO
    000151c4 T voltsTou16_USB1608GX_AO
    00027687 T volts_USB1208FS_Plus
    0000f5e4 T volts_USB1208HS
    000277dc T volts_USB1408FS_Plus
    00012410 T volts_USB1608FS
    00022ee2 T volts_USB1608FS_Plus
    00015268 T volts_USB1608G
    00013837 T volts_USB1608HS
    0001ba81 T volts_USB1616FS
    000287c4 T volts_USB2020
    0002203c T volts_USB20X
    00020f54 T volts_USB2416
    00024736 T volts_USB2600
    0001dbf1 T volts_USB31XX


        