
During the last two to three months, work has been done on writing a
Linux API for the USB-1208LS, USB-1208FS, USB-1024LS, USB-1024HLS, and
the USB-1608FS.  The API is not a driver in the sense of loading a
kernel module, but rather is a set of C routines which run in user
space that calls the kernel to interface with the HID/USB core.

Both the LS and FS version are HID devices.  Although programming HID
devices in Windows is straight forward, this is not the case in Linux.
The USBs are not HID devices in the sense of a mouse, keyboard, or joy
stick, but they do use a lot of the structure of a HID device.
However, the FS is speced to run at 50k Hz sustained, a speed that
exceeds human motor control which is well under 50 Hz.

Also, under Windows, there is only one way to interface with a HID
device, and that is through the Microsoft HID interface driver.  This
is well documented and extensively tested.  In addition, representatives
from Microsoft contributed to the original HID specification and
adopted it early on into their products.

Linux takes a very different approach to device drivers.  Under Linux,
it is not uncommon to patch the kernel itself and recompile, as one
would do any other program in Linux.  When I began to write the API, I
was faced with three choices:

1. Write a true Linux kernel module that interfaced with the usb core driver.

2. Write a user space API which used the Linux hiddev driver.

3. Write a user space API which used libhid/libusb support.

There are pros and cons to each of these approaches, but very little
in the way of discussion or documentation as to which approach is
better.  As the "devil is in the details", I went ahead, with help from
Mike Erickson and Marco Pantaleoni, and implemented options 2 and 3.


Option 2: Linux hiddev

The Linux HID character device was written by Vojtech Pavlik
<vojtech@suse.cz>. Paul Stewart <stewart@wetlogic.net> is also listed
as an author for hid.c.

In order to use the hiddev driver, one must include hiddev support in
the Linux kernel.  This is generally done with most of the major
distributions.  Each HID interface is associated with a device node,
usually /dev/usb/hiddev[0-15], although the location of these files
could change.  

When a usb device is attached to the controller, the usb core driver
interrogates the device and if it is a HID device, assigns the hid
driver to it by default.  The command:

cat /proc/bus/usb/devices 

produces the following:

P:  Vendor=09db ProdID=0076 Rev= 1.01
S:  Manufacturer=MCC
S:  Product=USB-1024LS
S:  SerialNumber=00016540
C:* #Ifs= 1 Cfg#= 1 Atr=a0 MxPwr=100mA
I:  If#= 0 Alt= 0 #EPs= 2 Cls=03(HID  ) Sub=00 Prot=00 Driver=hid
E:  Ad=81(I) Atr=03(Int.) MxPS=   8 Ivl=10ms
E:  Ad=01(O) Atr=03(Int.) MxPS=   8 Ivl=10ms

The last four lines are the most important.  

C:* #Ifs= 1 Cfg#= 1 Atr=a0 MxPwr=100mA

This line describes the configuration of the device, which has 1 interface.

The next line describes all the interfaces (which there is only 1):

I:  If#= 0 Alt= 0 #EPs= 2 Cls=03(HID  ) Sub=00 Prot=00 Driver=hid

The interface number of the first interface is 0, and it is associated
with the hid driver.  This is going to be important later, for if one
is going to use another driver, one has to dissociate the hid driver
from the interface.

The endpoints associated with each interface are then
described:

E:  Ad=81(I) Atr=03(Int.) MxPS=   8 Ivl=10ms
E:  Ad=01(O) Atr=03(Int.) MxPS=   8 Ivl=10ms

Interface 0 has two endpoints: an input interrupt endpoint 0x81, and
and output interrupt endpoint 0x01.  Not mentioned are the control
endpoints, which interface 0 has by default. The HID spec only
supports control and interrupt endpoints, and not isynchronous or bulk
endpoints.

When programming HID devices under Windows, one is not usually
concerned with this level of detail, and the same is true using
hiddev.  However, what is of interest is sending and receiving data
from the device.  Exactly how to do this with hiddev is poorly
documented.

Data is sent to/from endpoints using reports.  The FS device only
uses one report, report 0.  For a slow device, each report consists
of 8 bytes called usages.  It took us a while to reliably send usages
to the LS device in Linux.  It is also slow, because the way the
driver is written, one must issue an ioctl() call for each usage byte
sent to the device.  This is one of the reasons hiddev is so slow, as
an ioctl call involves a kernel context switch.

The following is a short list of the pros and cons of the hiddev driver:

Pros:
=====
1. Comes by default on most Linux distributions.
2. Fairly easy to use if one knows how.
3. It works from user space without being root.

Cons:
====
1. hiddev is broken under Linux 2.4.  We have only gotten it to
   work under the 2.6 kernel.
2. hiddev enforces the usages max/min limits, which caused problems in
   the LS series.
3. hiddev is quite slow.  I could not reliable get more than 200-1000 Hz
   through the driver.
4. Lots of delays have to put into the code for data to be transferred.
   (see code in ftp://lx10.tx.ncsu.edu/pub/Linux/drivers/USB-1.4.tgz)
5. Documentation and examples are almost non-existent.

The biggest concerns are items 3, 4 and 1 in that order.  The
performance under hiddev is quite poor and would take some effort to
improve the speed.  Also, one really needs to read and understand the
HID 1.11 specification before trying to use the driver.

If you want to interface to a HID device without using hiddev, the first thing
you have to do is dis-associate the hid driver from the device interface.
There are 3 ways to do this:

1. Remove the hid driver.  This is rather drastic, and will cause all other
   hid devices (mouse, keyboard, etc.) not to work.  

2. In the kernel source /usr/src/linux-XX/drivers/usb/hid-core.c

   there is a blacklist structure:

struct hid_blacklist {
        __u16 idVendor;
        __u16 idProduct;
        unsigned quirks;
} hid_blacklist[] = {

For example:

#define USB_VENDOR_ID_MEASUREMENT_COMPUTING 0x09db
#define USB_DEVICE_ID_USB-1024LS      0x0076

Then at the bottom of the hid_blacklist[]

        { USB_VENDOR_ID_ESSENTIAL_REALITY, USB_DEVICE_ID_ESSENTIAL_REALITY_P5, HID_QUIRK_IGNORE },
        { USB_VENDOR_ID_MEASUREMENT_COMPUTING, USB_DEVICE_ID_USB-1024LS, HID_QUIRK_IGNORE },
        { 0, 0 

Then recompile the kernel.  Having to recompile the kernel to use a
HID device is also a rather drastic measure, esp. if one is managing
a large number of systems.

3.  The third option is the easiest and most flexible.  Along with the libhid package (to be discussed), 
    there is a program called libhid-detach-device.  
    
    Example:

    libhid-detach-device 09db:0076



Option 3: libhid/libusb

The third option discussed here is to use libhid/libusb.  In order to use
this approach, the following packages need to be installed:


  - libtool     (1.5.6-1)
  - automake1.8 (1.8.5-1)
  - autoconf    (2.59-7)
  - pkg-config  (0.15.0-4)
  - gcc         (3.3.4-2)

  - libusb-0.1-4 (1:0.1.8-17)
  - libusb-dev (1:0.1.8-17)
  - libswig1.3.22 (1.3.22-3)
  - swig (1.3.22-3)
  - xsltproc (1.1.12-5)
  - docbook-xsl (1.66.1-1) 
  - doxygen (1.2.15-2)

  - hotplug  (obsolete in later versions of 2.6.X kernel, replaed by udev).

Not all these packages are really needed, but the make process may
fail, esp. when compiling libhid.  The most important ones are gcc 3.3
or later, libusb and libusb-dev 0.1-4-0.1.10a-9 or later, and the
latest version of libhid.  I do not think there is currently a rpm
package for libhid.  I compiled the library from the source
code. Please note that the libhid is included with the Debian
distribution, but it may be old and broken, so it is best to install
the latest package.

Getting all the correct packages installed is not that hard for
current systems, but could be a pain for older systems.

The following is a short list of the pros and cons of the libusb/libhid driver:

Pros:
=====
1. Works under 2.4 and 2.6 kernel
2. Does not enforce usage max/min
3. data transfer can be done through libusb calls, thus is much faster.
   On the LS series, I got a factor of 3, and was able to get 50kHz on
   the FS devices.
4. libusb/libhid works under Linux, Windows, and Mac OS, so only one library needs
   to be supported for all three operating systems.


Cons:
====
1. Need to be root or have the program run suid unless one configures
   with /proc/bus/usb/XXX/XXX file with hotplug.
2. Interfacing with the device with low level usb calls requires 
   a deeper understanding (better documentation) of the HID interface on the 
   device side.  There is no functionality for sending reports from libhid.
3. There are still some timing issues, but more stable then hiddev.


