---
layout: page
title: "WA-L-03"
description: ""
---
{% include JB/setup %}

##WA-L-03 display##

WA-L-01 documentation

    Characteristics:


        	format:	                         2 lines of 20 characters
        
        	supply voltage:	                 5V  0,25 V
        
        	input current:	                 depends on display tube:
                                                        JM55E - 200mA
                                                        JM55D - 600mA
                                                        VFD - 350mA
        
        	dimensions:                      360 x 193 x 30 (42) mm 
                                             19x45x180
                                             d xh xw
        	screen:                          162 x 37 mm
        
        	code page:                       PC852 (Latin2)
        
        	adjustment of revolution angle:  209
        
        	adjustment of bending angle:     20
        
        	interface:                       serial RS232C
        
        	transmission:                    data transfer rate: - 9600 bps;
                                             word length:        - 8 bits
                                             stop bits:          - 1
                                             parity:             - none
        
        	interface connector type:        WF6
        



    List of Control Commands
            
            
            Command code                              Description
            
            0x01                                      Enquiry about number of displayed characters. One byte of the value 40 (0x28) is returned in response.
            
            0x02                                      Displays control firmware version and checksum
            
            0x03                                      The cursor current position address. An address within the  range 0x00..0x14 is returned if the cursor is at the first line or within  the range 0x20..0x34 if the cursor is at the second line
            
            0x05                                      Not used
            
            0x04                                      Returns character indicated by cursor
            
            0x06                                      Cursor blinking ON
            
            0x07                                      Cursor blinking OFF
            
            0x08                                      Moves cursor one character back. Cursor position remains unchanged if it is at the beginning of the first (upper) line
            
            0x09                                      Moves cursor one character forwards. If cursor is at the end of second (bottom) line it moves to the beginning of the first line.
            
            0x0A                                      Cursor moves vertically to the second line. Offset address remains unchanged
            
            0x0B                                      Not used
            
            0x0C                                      Clear display
            
            0x0D                                      Move cursor to the beginning of the line
            
            0x0E                                      Disable cursor
            
            0x0F                                      Enable cursor
            
            0x10                                      Data input starts from the first character of the second line. Cursor moves and performs New Line function automatically 
            
            0x11                                      Data input starts from the beginning of the first line. Cursor moves and performs New Line function automatically
            
            0x12                                      Disable cursor return. The cursor stops at the last character of the line which is overwritten.
            
            0x13                                      Move text horizontally after filling screen. In case if more than 40 characters is sent to display the last character at the second line is not overwritten but the whole displayed text is moved to the left – the beginning characters disappear.
            
            0x14                                      Restore initial settings
            
            0x15                                      Clears display and set cursor. If currently selected mode is 0x10 then cursor is set at the beginning of the second line otherwise it is set at the beginning of the first line.
            
            0x16                                      Wrap up text automatically. Cursor moves from the last position at the first line to the first position at the second line and from the last position at the second line to the first position at the first line.
            
            0x1B 0x05 0x49                            In response to this sequence of bytes display returns 18 bytes long message: “D,2,IEEE,3678,-01’<CR> 
            
            0x1B 0x74 <n>                             <n> - code page number            
                                                      Set code page number <n>. The command is ignored when Ukrainian code page is installed.
                                                      
            0x1F 0x24 <C> <L>                         <C> -  column number (1..20)
                                                      <L> - line number (1..2)
                                                      Set cursor at the address <C> <L>
                                                      
            0x1F 0x58 <n>                             Turn backlight ON/OFF.
                                                      <n> = 0x01 or 0x02 – backlight OFF
                                                      <n> = 0x03 or 0x04 – backlight ON

#Test from command line#

Setting up serial interface
    *sudo stty -F /dev/ttyUSB0 speed 9600 cs8 -cstopb -parenb*
    
Sending command (clear screen)
    *sudo echo -en '\x0c' > /dev/ttyUSB0*

Sending text
    *sudo echo -en 'Ala ma kota' > /dev/ttyUSB0*


#Test from python#

import serial
ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=1)
print ser.portstr       #check which port was really used
ser.write("hello")      #write a string
ser.close()      



[**WS-9040-IT**](http://www.technoline.eu/details.php?id=1582&kat=5)

[<img src="Res/WS 9040 IT.jpg" width="" height="300"/>](Res/WS 9040 IT.jpg "WS 9040 IT")

[**WS-9130-IT**](http://www.technoline.eu/details.php?id=1521&kat=5)

[<img src="Res/WS 9130-IT.jpg" width="" height="300"/>](Res/WS 9130-IT.jpg "WS 9130 IT")

[**WM-5012**](http://www.technoline.eu/details.php?id=1470&kat=79)

[<img src="Res/WM 5012.jpg" width="" height="300"/>](Res/WM 5012.jpg "WM 5012")

##Current consumption##

**WS-9040-IT**     - 10,8 mA

**WS-9130-IT** (1) - 11.24 mA

**WS-9130-IT** (2) - 12.20 mA

**TX37-IT**    (2) - 0.5 - 0.1 mA (peak)

**TX37-IT**    (1) - 0.3 - 0.1 mA (peak)

**TX38-IT**        - 0.5 mA (peak)

**WM-5012**        - 0.5 mA (?)

##Manuals##

[WS-9040-IT](Res/Manuals/WS-9040-IT.pdf)

[WS-9130-IT](Res/Manuals/WS-9130-IT.pdf)

[WM-5012](Res/Manuals/WM-5012.pdf)


**WM 512 weather data regions** ([source](http://www.meteotronic.info/index.php?pays=schweiz&id=32&lang=de))

[<img src="Res/WM5012Regions.jpg" width="" height="100"/>](Res/WM5012Regions.jpg "WM 512 weather data regions")

        Reg Länder 	       Städte
        0	Frankreich	   Bordeaux
        1	Frankreich	   La Rochelle
        2	Frankreich	   Paris
        3	Frankreich	   Brest
        4	Frankreich	   Clermont-Ferrand
        5	Frankreich	   Béziers
        6	Belgien	       Brüssel
        7	Frankreich	   Dijon
        8	Frankreich	   Marseilles
        9	Frankreich	   Lyon
        10	Frankreich	   Grenoble
        11	Schweiz	       La Chaux de Fonds
        12	Deutschland	   Frankfurt am Main
        13	Belgien, Luxemburg, Deutschland	Wallonne
        14	Deutschland	   Duisburg
        15	Großbritannien Swansea
        16	Großbritannien Manchester
        17	Frankreich	   Le Havre
        18	Großbritannien London
        19	Deutschland	   Bremerhaven
        20	Dänemark	   Herning
        21	Dänemark	   Arhus
        22	Deutschland	   Hannover
        23	Dänemark	   Kopenhagen
        24	Deutschland	   Rostock
        25	Deutschland	   Ingolstadt
        26	Deutschland	   München
        27	Italien	       Bozen
        28	Deutschland	   Nürnberg
        29	Deutschland	   Leipzig
        30	Deutschland	   Erfurt
        31	Schweiz        Lausanne
        32	Schweiz        Zürich
        33	Schweiz	       Adelboden
        34	Schweiz	       Sion
        35	Schweiz	       Glarus
        36	Schweiz	       Davos
        37	Deutschland	   Kassel
        38	Schweiz	       Locarno
        39	Italien	       Sestriere
        40	Italien	       Mailand
        41	Italien	       Rom
        42	Niederlande	   Amsterdam
        43	Italien	       Genua
        44	Italien	       Venedig
        45	Frankreich	   Straßburg
        46	Österreich	   Klagenfurt
        47	Österreich	   Innsbruck
        48	Österreich	   Salzburg
        49	Slowakei	   Bratislava
        50	Tschechien	   Prag
        51	Tschechien	   Decin
        52	Deutschland	   Berlin
        53	Schweden	   Göteborg
        54	Schweden	   Stockholm
        55	Schweden	   Kalmar
        56	Schweden	   Jönköping
        57	Deutschland	   Donaueschingen
        58	Norwegen	   Oslo
        59	Deutschland	   Stuttgart
        60	Italien	       Neapel
        61	Italien	       Ancona
        62	Italien	       Bari
        63	Ungarn	       Budapest
        64	Spanien	       Madrid
        65	Spanien	       Bilbao
        66	Italien	       Palermo
        67	Spanien	       Palma de Mallorca
        68	Spanien	       Valencia
        69	Spanien	       Barcelona
        70	Andorra	       Andorra
        71	Spanien	       Sevilla
        72	Portugal	   Lissabon
        73	Italien	       Sassari
        74	Spanien	       Gijon
        75	Irland	       Galway
        76	Irland	       Dublin
        77	Großbritannien Glasgow
        78	Norwegen	   Stavanger
        79	Norwegen	   Trondheim
        80	Schweden	   Sundswall
        81	Polen	       Danzig
        82	Polen	       Warschau
        83	Polen	       Krakau
        84	Schweden       Umeå
        85	Schweden	   Östersund
        86	Schweiz	       Samedan
        87	Kroatien       Zagreb
        88	Schweiz	       Zermatt
        89	Kroatien	   Split


