---
layout: page
title: "Movie to animated gif"
description: ""
---
{% include JB/setup %}

**Convertion of movie to animatated gif with mplayer**

        mplayer guvcview_video-1.mkv -ao null -ss 0:00:05 -endpos 15 -vo gif89a:fps=13:output=animated.gif -vf scale=240:180

**[Source](http://www.linuxandlife.com/2012/07/4-cool-tricks-to-do-with-mplayer.html) of this great trick.**

**Oryginal note with explanation of all parameters.**

        There are many tools to create animated GIF images and MPlayer is one of them. To create a GIF image from a video, the command you can use is:
        
        mplayer video-file -ao null -ss 0:00:15 -endpos 15 -vo gif89a:fps=24:output=animated.gif -vf scale=400:280
        
        
        Here is what the variables in this command mean:
        
        "video-file": the video from which you want to create the GIF
        "-ss 0:00:15" : when the GIF starts in the video, replace 0:00:15 with what you want
        "-endpos 15": the length of the GIF, replace 15 with what you want
        "gif89a:fps=24" : 24 here means 24 frames/seconds
        "output=animated.gif" : change the animated.gif into the file name you want
        "scale=400:280" : dimensions of the GIF - See more at: http://www.linuxandlife.com/2012/07/4-cool-tricks-to-do-with-mplayer.html#sthash.suNY1pEw.dpuf
        
**An example of the result.**
        
[<img src="Res/64pPrototype.gif" width="" height="100"/>](Res/64pPrototype.gif "64pixels")
        
[Source](http://blog.hugochinchilla.net/2011/09/time-lapse-videos-mencoder/)


Step 1:

    ls -1tr > frames.txt

Step2:
        
    mencoder -nosound -ovc lavc -lavcopts \
    vcodec=mpeg4:mbd=2:trell:autoaspect:vqscale=3 \
    -vf scale=1920:1080 -mf type=jpeg:fps=20 \
    mf://@frames.txt -o time-lapse.avi


mencoder -nosound -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell:autoaspect:vqscale=3 -vf scale=640:480 -mf type=jpeg:fps=60 mf://@frames.txt -o time-lapse.avi