import re
import math
import argparse

parser = argparse.ArgumentParser(\
	description='Filter a GPX file to remove small movements')
parser.add_argument('input_file', metavar='input_file', \
	help='Input GPX file')
parser.add_argument('output_file', metavar='output_file', \
	help='Output GPX file')
parser.add_argument("-v","--verbose", action="store_true",\
	help="increase output verbosity")
parser.add_argument("-d","--distance",\
	default=0.1, type=float, \
	help="Minimum distance between points in km (default 0.1)")
args = parser.parse_args()
min_distance_in_km = float(args.distance)

def get_distance_from_lat_lon(lat1,lon1,lat2,lon2):
	""" Uses the Haversine formula for calculations """
	radius = 6371
	d_lat = math.radians(lat2 - lat1)
	d_lon = math.radians(lon2 - lon1)
	a = math.sin(d_lat / 2) * math.sin(d_lat / 2) + \
		math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * \
		math.sin(d_lon / 2) * math.sin(d_lon / 2)
	c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
	d = radius * c
	return d

total_distance = 0
total_tracks = 0
removed_tracks = 0

input_file = open(args.input_file)
output_file = open(args.output_file,'w')

lat_re = re.compile('lat="([^"]*)')
lon_re = re.compile('lon="([^"]*)')

trkpt_re = re.compile('trkpt')

in_trackpoint = False
last_lat = None
last_lon = None
segment_distance = 0

output_line = True
for line in input_file:
	if not in_trackpoint:
		trkpt_result = trkpt_re.search(line)
		if trkpt_result:
			in_trackpoint = True
			lat_result = lat_re.search(line)
			lon_result = lon_re.search(line)
			lat = float(lat_result.group(1))
			lon = float(lon_result.group(1))
			total_tracks += 1
			if not last_lat is None:
				distance = get_distance_from_lat_lon(lat,lon,last_lat,last_lon)
				total_distance += distance
				segment_distance += distance
				if segment_distance < min_distance_in_km:
					output_line = False
					removed_tracks += 1
				else:
					output_line = True
					segment_distance = 0
			last_lat = lat
			last_lon = lon
		else:
			output_line = True
	else:
		trkpt_result = trkpt_re.search(line)
		if trkpt_result:
			in_trackpoint = False
	if output_line:
		output_file.write(line)

if args.verbose:
	print "Total Distance : " + str(total_distance)
	print "Total Tracks   : " + str(total_tracks)
	print "Removed Tracks : " + str(removed_tracks)
