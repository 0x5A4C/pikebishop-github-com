---
layout: page
title: "Gpx Visualization"
description: "Gpx visualization on web pages with markdown - static and dynamic."
---
{% include JB/setup %}

---

#Resources
 - [gpxpy](https://github.com/tkrajina/gpxpy) from web page: *This is a simple python library for parsing and manipulating GPX files. GPX is an XML based format for GPS tracks.* *The repository contain a little command line utility (GPXInfo) to extract basic statistics from a file.*

 - [jsFiddle](http://jsfiddle.net/) - Great online service for editing and testing js. 

 - [Plot my ride](http://www.plotmyride.com/route/show/1239/1/L/0/TRACK) - online service for plotting gpx (not only?) data. Nice looking.

 - [How-To Use wp-gpx-maps on a non-wordpress website](http://www.devfarm.it/forums/topic/sample-wrapper/) - interesting but requires php. Check working [example](./Res/wpgpxmaps.zip).
 
 - [Data-Driven Documents](http://d3js.org/) - directly from web page: *D3.js is a JavaScript library for manipulating documents based on data.*. There source and is many examples on [GitHub](https://github.com/mbostock/d3/wiki/Gallery); It looks astonishing. 
 
 - [D3 example](http://blog.thematicmapping.org/2013/11/showing-gps-tracks-in-3d-with-threejs.html) - example plotting gpx on 3d map with WebGl.

 - [Leaflet](http://leafletjs.com/) - from web page: Leaflet is a modern open-source JavaScript library for mobile-friendly interactive maps.
 - [Leaflet.Elevation](http://mrmufflon.github.io/Leaflet.Elevation/) - from web page: A Leaflet plugin to view interactive height profiles of polylines lines using d3.

 - [gpx.tomaskafka.com](http://gpx.tomaskafka.com) - beautiful service for plotting gpx. No registration, "uploading it to big brother's cloud". **Favourite one!**. GitHub offers [sources](https://github.com/tkafka/Javascript-GPX-track-viewer).
 
 - [Tracklog](https://github.com/nano/tracklog) - from app. readme: *Tracklog is a Ruby on Rails application for managing GPS track logs. It’s based on Rails Edge and thus requires Ruby 1.9*. Nice but it's full web server app.
 
 - [Shift edit](https://shiftedit.net/) - Online editor useful wile experiments with JS.

 - [Visualizations: Geographic/GIS](http://cs.smith.edu/dftwiki/index.php/Visualizations:_Geographic/GIS) - from web page: This page is maintained by Dominique Thiebaut and contains various interesting visualization examples or related material gathered on the Web, and in various publications.
 
 - [Journey2web](http://dev.filyb.info/journey2web/) - from web page: *Journey2web is a script to build a map of journey, from a set of GPS tracks, text descriptions and pictures.*

 - [gpxviewer](https://code.google.com/p/gpxviewer/) - from web page: *The GPX viewer is a 100% client-side JavaScript GPX file viewer that uses Google Maps to map waypoints and tracklogs.*

- [gpx.js](https://github.com/Leonidas-from-XIV/gpx.js) - from web page: *The idea of GPX.js is to provide a friendly interface for working with GPX files.*

- [GoPro video maps ](https://www.mapbox.com/blog/gopro-video-maps/) - from web page: *GoPro video maps*

- [a](http://kartograph.org/)

- [aa](http://sensitivecities.com/so-youd-like-to-make-a-map-using-python-EN.html)

- [aaa](Khtmlib)

#Problem
---

How to visualize gps track on static generated web page. In such case input format for source documents is markdown/textile/rst/asciidoc etc. Seems that there are two obvious solutions:

* generating pictures presenting required data - **Static** method,
* trying to use some well known solutions based on Google or OpenStreetMap technology - **Dynamic** method,
* some (all?) frameworks used to generate static web services allows to implement plugins/adsons - this technology is platform dependent and this is unacceptable disadvantage.

#Generating basic statistics of track
---

 * [gpxpy]

**Output**

		File: 00_WielkaKłodzka.gpx
			Length 2D: 348.316737737
			Length 3D: 351.448229034
			Moving time: 07:21:13
			Stopped time: -8:38:54
			Max speed: 25.8976012633m/s = 93.2313645478km/h
			Total uphill: 12548.3m
			Total downhill: 12102.3m
			Started: 2013-08-03 07:00:01
			Ended: 2013-08-03 07:00:08
			Points: 26839
			Avg distance between points: 15.3427201477m

			Track #0, Segment #0
				Length 2D: 56.1587721993
				Length 3D: 56.6790665228
				Moving time: 01:13:05
				Stopped time: -2:46:55
				Max speed: 19.7464955847m/s = 71.0873841049km/h
				Total uphill: 1784.4m
				Total downhill: 1732.4m
				Started: 2013-08-03 07:00:01
				Ended: 2013-08-03 07:00:01
				Points: 4592
				Avg distance between points: 2.09243161814m

			Track #1, Segment #0
				Length 2D: 75.0454029191
				Length 3D: 75.8177144027
				Moving time: 01:38:13
				Stopped time: -2:21:51
				Max speed: 22.1175049196m/s = 79.6230177105km/h
				Total uphill: 3092.1m
				Total downhill: 3203.1m
				Started: 2013-08-03 07:00:01
				Ended: 2013-08-03 07:00:05
				Points: 5952
				Avg distance between points: 2.79613260252m

			Track #2, Segment #0
				Length 2D: 80.3044091049
				Length 3D: 81.015943159
				Moving time: 01:39:56
				Stopped time: -2:20:06
				Max speed: 25.8976012633m/s = 93.2313645478km/h
				Total uphill: 3180.0m
				Total downhill: 3135.0m
				Started: 2013-08-03 07:00:05
				Ended: 2013-08-03 07:00:07
				Points: 6168
				Avg distance between points: 2.9920790307m

			Track #3, Segment #0
				Length 2D: 54.6467379645
				Length 3D: 54.9035684266
				Moving time: 01:03:45
				Stopped time: -2:56:16
				Max speed: 22.6392800499m/s = 81.5014081797km/h
				Total uphill: 1396.1m
				Total downhill: 1163.1m
				Started: 2013-08-03 07:00:07
				Ended: 2013-08-03 07:00:08
				Points: 3755
				Avg distance between points: 2.03609441352m

			Track #4, Segment #0
				Length 2D: 82.1614155488
				Length 3D: 83.0319365229
				Moving time: 01:46:14
				Stopped time: -2:13:46
				Max speed: 21.6376567548m/s = 77.8955643174km/h
				Total uphill: 3095.7m
				Total downhill: 2868.7m
				Started: 2013-08-03 07:00:08
				Ended: 2013-08-03 07:00:08
				Points: 6372
				Avg distance between points: 3.06126962811m	 

 * [wherewasi]

**Output**

			WhereWasI Output
			=================

		Filename=00_WielkaKłodzka.gpx
		Number of Track Segments	= 5
		---------------------------------------------
		Start Segment / Point 	= 0/0
		End Segment / Point 	= 4/6371
		Start Time            	= 03/08/2013 07:00:01
		End Time              	= 03/08/2013 07:02:13
		Number of Track Points	= 26839
		---------------------------------------------
		Total Time            	= 00:00
		Total Distance        	= 412.02 km
		Total Climb           	= 14262 m
		---------------------------------------------
		Average Speed         	= 211896.27 km/hr
		Maximum Speed         	= 856.36 km/hr
		---------------------------------------------

[gpxpy]:https://github.com/tkrajina/gpxpy
[wherewasi]:http://code.google.com/p/wherewasi/

#Preapring GPX data for map visualization
---
 
 * [Travelling salesman - gpx reduce]

	Removes points from gpx-files to reduce filesize and tries to keep introduced distortions to the track at a minimum.
	Do not work with multi-track files!
	
		python gpx_reduce.py 3.gpx

* [GpsBabel]

		gpsbabel -i gpx -f 3.gpx -x simplify,count=400 -o gpx -F 3_simplified.gpx
	
	Small file, but bad results.

		gpsbabel -i gpx -f 3.gpx -x simplify,error=0.001k -o gpx -F 3_simplified_01.gpx
		
	File reduced (> 50%). Parameter error specifies distance error (if k after digits than in km).

* [Gpx processing with python]

	Filter a GPX file to remove small movements (default criteria == 0.1 produce very (and too small files)).

		python gpx_distance_filter.py 3.gpx 3_filtered.gpx

[Travelling salesman - gpx reduce]:http://wiki.openstreetmap.org/wiki/User:Travelling_salesman/gpx_reduce
[GpsBabel]:http://www.gpsbabel.org/htmldoc-1.4.2/filter_simplify.html
[Gpx processing with python]:http://www.gregreynolds.co.uk/gpx-processing-with-python/

#Markdown html and javascript
---

- Indentation of part of text is interpreted by markdown processor as ordinary text, which should be presented as following:

		Markdown text starting from 1st (!) column
		<foo>
			<bar>
			</bar>	
		</foo>
		Markdown text starting from 1st (!) column

If we want to avoid interpretation of html/javaScript as text, we have to remove unwanted indentation.

- If script tag is used in this way:

		<script src="{{ relative }}js/jstools4gps/js/mapstraction.js"></script>

  markdown parser will changed  it into self-closing tag. This will produced html which will parsed with errors by browser. To avoid space have to be added in this way:

		<script src="{{ relative }}js/jstools4gps/js/mapstraction.js"> </script> 

- Including scripts

  - relative path (from [How to deploy a jekyll site locally])
  
			{% capture lvl %}{{ page.url | append:'index.html' | split:'/' | size }}{% endcapture %}
			{% capture relative %}{% for i in (3..lvl) %}../{% endfor %}{% endcapture %}

			<link href="{{ relative }}css/main.css" rel="stylesheet" />
			<script src="{{ relative }}scripts/jquery.js"> </script>

			---
			title: My Page title
			root: "../"
			---
  
  - direct path
  
			<link href="/css/main.css" rel="stylesheet" />
			<script src="/scripts/jquery.js"> </script>  

  - in general path wit underscore in front of name should be avoided, they are ignored while parsing by jekyll(?)

#Static
---
##Profile
###Qgis
This sophisticated tool have some plugin, which can be used to visualize gpx profiles, but this technology is off topic.

###Generate static image with Python script

####GnuPlot

GnuPlot is fantastic command-line tool for generating different types of charts. 
I analyses data and with assistance of some commands generates designed chart. Example how it works can be found in [wikipedia](http://commons.wikimedia.org/wiki/File:Rp_Einwohner.svg). Below source code and from used in this example.

Script for gnu plot.

	reset
	data = "rp_data.txt"
	name = "rp_Einwohner"
	type = ".svg"
	set terminal svg size 800,600 font "Arial, 14"
	set grid
	unset key
	set style fill transparent solid 0.3
	set style data filledcurves below x1
	set mxtics 5
	set mytics 5
	set format y "%.1f"
	set xrange [1815:2010]
	set xlabel "Jahr"
	set ylabel "Einwohner (Mio.)"
	set output name.type
	plot data using 1:2 linewidth 2 lc rgb "dark-blue"
	unset output

Data for gnu plot.

	#rp_data.txt
	#Jahr Mio. Einwohner
	1815 1.202412
	1835 1.614684
	1871 1.832388
	1905 2.434505
	1939 2.959994
	1950 3.004784
	1961 3.417116
	1965 3.581993
	1970 3.645437
	1975 3.665777
	1980 3.642482
	1985 3.615049
	1990 3.763510
	1995 3.977919
	2000 4.034557
	2005 4.058843
	2008 4.028351
	2009 4.012675
	2010 4.003745

During tests gnuplot was controlled  in two different ways:

* with python,
* with internal commands stored in separate file.

**Preparing data for gnuplot.**

 Input data for gnuplot have to be aggregated in datasets. Easiest way to build dataset is generate text file with sets of numbers formatted into columns. Char used for separation can be defined as gnuplot parameter. If gnuplot is controlled directly form python, it's possible to pass data to gnuplot directly thru python object.
 This trick is implemented by "gplot" module, which (probably)  generates own temporary dataset file. For example see "whereWasI" source. This tool was designed to generate some statistics from gpx data. It can be found in [wherewasi](http://code.google.com/p/wherewasi/ "wherewasi") tool sources.

Example of generating dataset text file with script based on  "whereWasI" application (part of wherewasi.py file):

        print "\n"
        print "\t\tanalyseGPX Output"
        print "\t\t=================\n"
        print "\tFilename=%s" % args[0]
        print "\tNumber of Track Segments\t= %d" % pgpx.getNumTrkSeg()
        print "\t---------------------------------------------"
        print "\tStart Segment / Point \t= %d/%d" % \
            (options.s_seg,options.s_pt)
        print "\tEnd Segment / Point \t= %d/%d" % \
            (options.e_seg,options.e_pt)
        print "\t---------------------------------------------"
        print "Date         Time          time_t      sec  Distance (km) Elevation (m)"
        edistprofXY = []
        etimeprofXY = []
        dtimeprofXY = []

	     edistprofXY_file = open("edistprofXY.dat", "w")
	     etimeprofXY_file = open("etimeprofXY.dat", "w")
	     dtimeprofXY_file = open("dtimeprofXY.dat", "w")


        for rec in profdata:
            timeStruct = localtime(rec[0])
            timeStr = strftime("%d/%m/%Y %X",timeStruct)
            print "%s \t %d \t %d \t %6.2f \t %6.0f" % \
                (timeStr,rec[0],rec[1], rec[2], rec[3])
            edistprofXY.append([rec[2],rec[3]])
            etimeprofXY.append([rec[1]/3600.,rec[3]])
            dtimeprofXY.append([rec[1]/3600.,rec[2]])

	     edistprofXY_file.write("{0};{1}\n".format(rec[2], rec[3]))
	     etimeprofXY_file.write("{0};{1}\n".format(rec[1]/3600., rec[3]))
	     dtimeprofXY_file.write("{0};{1}\n".format(rec[1]/3600., rec[2]))

        if options.graph:
            try:
		          import Gnuplot
                g=Gnuplot.Gnuplot()
                if options.eprofile:
                    g.title("GPX Track Elevation Profile")
                    g.xlabel('Distance from Start (km)')
                    g.ylabel('Elevation (m)')
                    g.plot(edistprofXY)
                    raw_input('--Please press return to continue...\n')
                    g.reset()
                if options.dprofile:
                    g.title("GPX Track Elevation Profile")
                    g.xlabel('Time from Start (hours)')
                    g.ylabel('Distance(km)')
                    g.plot(dtimeprofXY)
                    raw_input('---Please press return to continue...\n')
                    g.reset()
            except: # python-gnuplot is not available or is broken
		          print 'ERROR: python gnuplot interface is not found'

	     edistprofXY_file.close()
	     etimeprofXY_file.close()
	     dtimeprofXY_file.close()

Example of using gnuplot with **python**:

	try:
		import gplot

		gplt = gplot.Gnuplot()

		gplt.xlabel(txt='xlabel', color=None, font=None)
		gplt.ylabel(txt='ylabel', color=None, font=None)
		gplt.title(txt='title', color=None, font=None)
	
		gplt.options = 'set style data lines'
		gplt.options = 'set grid'
		gplt.options = 'set key outside below'

		gplt.plot(edistprofXY)

	except Exception as inst:
		print type(inst)     # the exception instance
		print inst.args      # arguments stored in .args
		print inst           # __str__ allows args to printed directly

Example of using **internal commands stored in separate file**:

    #input
    set timefmt "%d.%m.%Y"
    set datafile separator ";"
     
    #output
    unset colorbox

    #set object 1 rect at screen 0.31, screen 0.78 size screen 0.4, screen 0.28 \
    #    fc rgb '#FDFFE4' fs transparent solid 0.8 border rgb '#6E5C49' lw 3
     
    #set object 2 rect at screen 0.14, screen 0.86 size char 2, char 1 \
    #    fc rgb '#578D99' fs transparent solid 0.6 border rgb '#578D99'
    #set label 2 "vor 1811: Schätzungen" at screen 0.16, screen 0.86 left 
     
    #set object 3 rect at screen 0.14, screen 0.78 size char 2, char 1 \
    #    fc rgb '#005D70' fs transparent solid 0.6 border rgb '#005D70' 
    #set label 3 "ab 1811: Volkszählungen, Fortschreibungen" at screen 0.16, screen 0.78 left 
     
    #set object 4 rect at screen 0.14, screen 0.70 size char 2, char 1 \
    #    fc rgb '#F25D00' fs transparent solid 0.6 border rgb '#F25D00' 
    #set label 4 "ab 2011: Prognose der Bertelsmann-Stiftung" at screen 0.16, screen 0.70 left 

    set style data lines
    set style fill transparent solid 0.4
    #set style fill pattern 2
    set grid
     
    set term svg size 800,400 font "Arial,10"

    set title 'GPX Track Elevation Profile'
    #[2]
    set xlabel 'Distance from Start (km)'
    #[3]
    set ylabel 'Elevation (m)'
    set output 'edistprofXY.svg'
    plot 'edistprofXY.dat' with filledcurves below x1 lt rgb 'dark-blue' lw 1
    #plot 'edistprofXY.dat' with impulses lt rgb 'dark-blue' lw 1

    set title 'GPX Track Distance Profile'
    #[1]
    set xlabel 'Time from Start (hours)'
    #[2]
    set ylabel 'Distance (km)'
    set output 'dtimeprofXY.svg'
    plot 'dtimeprofXY.dat' with filledcurves below x1 lt rgb 'dark-red' lw 1
    #plot 'dtimeprofXY.dat' with impulses lt rgb 'dark-red' lw 1

    set title 'GPX Track Time Profile'
    #[1]
    set xlabel 'Time from Start (hours)' 
    #[3]
    set ylabel 'Elevation (m)'
    set output 'etimeprofXY.svg'
    plot "etimeprofXY.dat" with filledcurves below x1 lt rgb 'dark-green' lw 1
    #plot "etimeprofXY.dat" with impulses lt rgb 'dark-green' lw 1

    set title 'GPX Track'
    set xlabel 'Time from Start (hours)'
    set ylabel 'Elevation (m)'
    set output 'gpxXY.svg'
    plot 'edistprofXY.dat' with filledcurves below x1 lt rgb 'dark-blue' lw 1, \
         'dtimeprofXY.dat' with filledcurves below x1 lt rgb 'dark-red' lw 1, \
         'etimeprofXY.dat' with filledcurves below x1 lt rgb 'dark-green' lw 1

    set title 'GPX Track'
    set xlabel 'Time from Start (hours)'
    set ylabel 'Elevation (m)'
    set output 'gpxXY.svg'
    plot 'edistprofXY.dat' with filledcurves below x1 lt rgb 'dark-blue' lw 1, \
         'dtimeprofXY.dat' with filledcurves below x1 lt rgb 'dark-red' lw 1, \
         'etimeprofXY.dat' with filledcurves below x1 lt rgb 'dark-green' lw 1

File is loaded by calling following command:

      user@pc:~/Tools/wherewasi$ gnuplot 

	      G N U P L O T
	      Version 4.6 patchlevel 4    last modified 2013-10-06 
	      Build System: Linux i686

	      Copyright (C) 1986-1993, 1998, 2004, 2007-2013
	      Thomas Williams, Colin Kelley and many others

	      gnuplot home:     http://www.gnuplot.info
	      faq, bugs, etc:   type "help FAQ"
	      immediate help:   type "help"  (plot window: hit 'h')

      Terminal type set to 'x11'
      gnuplot> load "./gss.gplot"
      gnuplot> 

Some results:

[<img src="Res/edistprofXY.svg" width="700" height=""/>](Res/edistprofXY.svg "")
[<img src="Res/etimeprofXY.svg" width="700" height=""/>](Res/etimeprofXY.svg "")
[<img src="Res/dtimeprofXY.svg" width="700" height=""/>](Res/dtimeprofXY.svg "")


**Installation**

Tool installed from distribution (ubuntu) was not working. Error (some) related to X11 was reported (something about absence of X11...). Helped rebuilding from sources with configuration reviewing.

####Google chart api

[Google chart api]:http://www.madpickles.org/rokjoo/2010/08/11/gpx-elevation-profile-plotting-with-the-google-chart-api/

##Map
###Generate static image with Python script
####Gnuplot
http://riotorto.users.sourceforge.net/gnuplot/geomap/index.html
####MatPlotLib

**Instlation** (from packeges definitly)

	sudo apt-get install python-matplotlib
	sudo apt-get install python-mpltoolkits.basemap

**Basic example**

Code

	from mpl_toolkits.basemap import Basemap
	import matplotlib.pyplot as plt
	import numpy as np
	from datetime import datetime

	import gpxpy
	import pylab
	import math

	def get_xy_from_points_mercator(points) :
		# http://fr.wikipedia.org/wiki/Projection_de_Mercator
		longitude_origine=sum([p.longitude for p in points])/len(points)
		coords=[] 
		for p in points :
		    x=p.longitude-longitude_origine
		    y=math.log(math.tan(math.radians(math.pi/4+p.latitude/2)))        
		    coords.append((x,y))
		return coords  
	 
	# make sure the value of resolution is a lowercase L,
	#  for 'low', not a numeral 1
	#map = Basemap(projection='merc', lat_0=50, lon_0=17,
	#              	resolution='h', area_thresh=1.0,
	#              	llcrnrlat=49.8699, llcrnrlon=17.5191,
	#				urcrnrlat=50.7069, urcrnrlon=15.8080)                

	map = Basemap(projection='lcc', lat_0=50, lon_0=17,
		          	resolution='h', area_thresh=0.1,
		          	urcrnrlat=50.7069, urcrnrlon=17.5191,
					llcrnrlat=49.8699, llcrnrlon=15.8080)                


	map.drawcoastlines()
	map.drawcountries()
	#map.fillcontinents(color='coral')
	#map.bluemarble() 

	map.drawmapboundary()
	 
	map.drawmeridians(np.arange(0, 360, 30))
	map.drawparallels(np.arange(-90, 90, 30)) 

	lon = -135.3318
	lat = 57.0799
	x,y = map(lon, lat)
	map.plot(x, y, 'bo', markersize=12) 
	 
	#date = datetime.utcnow()
	#CS=map.nightshade(date)
	#plt.title('Day/Night Map for %s (UTC)' % date.strftime("%d %b %Y %H:%M:%S")) 

	gpx_file = open('3.gpx', 'r')
	gpx = gpxpy.parse(gpx_file)
	points=gpx.tracks[0].segments[0].points
	for i,point in enumerate(points):
		print('Point at ({0},{1},{2}) : {3}'.format(point.latitude, point.longitude, point.elevation,point.time))
		x,y = map(point.longitude, point.latitude)
		map.plot(x, y, 'bo', markersize=12) 

	map.etopo()

	pylab.plot(x,y)
	pylab.show()


Result

[<img src="Res/matplotlib.jpg" width="700" height=""/>](Res/matplotlib.jpg)

Data provided with library is very poor and weak. Usless (almost) for gsp data presentation.

**With ShapeFiles**

Nice tutorial is in [Basemap with shapefiles -more detailed map](http://www.geophysique.be/2011/01/27/matplotlib-basemap-tutorial-07-shapefiles-unleached/)

Geo data can be found in [Global Administrative Areas service](http://www.gadm.org/)

Code

	#
	# BaseMap example by geophysique.be
	# tutorial 10
	 
	import numpy as np
	import matplotlib.pyplot as plt
	from mpl_toolkits.basemap import Basemap
	 
	### PARAMETERS FOR MATPLOTLIB :
	import matplotlib as mpl
	mpl.rcParams['font.size'] = 10.
	mpl.rcParams['font.family'] = 'Comic Sans MS'
	mpl.rcParams['axes.labelsize'] = 8.
	mpl.rcParams['xtick.labelsize'] = 6.
	mpl.rcParams['ytick.labelsize'] = 6.
	 
	fig = plt.figure(figsize=(11.7,8.3))
	#Custom adjust of the subplots
	plt.subplots_adjust(left=0.05,right=0.95,top=0.90,bottom=0.05,wspace=0.15,hspace=0.05)
	ax = plt.subplot(111)
	#Let's create a basemap of Europe
	x1 = -5.0
	x2 = 15.
	y1 = 45.
	y2 = 54.
	 
	m = Basemap(resolution='i',projection='merc', llcrnrlat=y1,urcrnrlat=y2,llcrnrlon=x1,urcrnrlon=x2,lat_ts=(x1+x2)/2)
	m.drawcountries(linewidth=0.5)
	m.drawcoastlines(linewidth=0.5)
	m.drawparallels(np.arange(y1,y2,2.),labels=[1,0,0,0],color='black',dashes=[1,0],labelstyle='+/-',linewidth=0.2) # draw parallels
	m.drawmeridians(np.arange(x1,x2,2.),labels=[0,0,0,1],color='black',dashes=[1,0],labelstyle='+/-',linewidth=0.2) # draw meridians
	 
	from matplotlib.collections import LineCollection
	from matplotlib import cm
	import shapefile
	 
	r = shapefile.Reader(r"borders/BEL_adm3")
	shapes = r.shapes()
	records = r.records()
	 
	for record, shape in zip(records,shapes):
		lons,lats = zip(*shape.points)
		data = np.array(m(lons, lats)).T
	 
		if len(shape.parts) == 1:
		    segs = [data,]
		else:
		    segs = []
		    for i in range(1,len(shape.parts)):
		        index = shape.parts[i-1]
		        index2 = shape.parts[i]
		        segs.append(data[index:index2])
		    segs.append(data[index2:])
	 
		lines = LineCollection(segs,antialiaseds=(1,))
		lines.set_facecolors(cm.jet(np.random.rand(1)))
		lines.set_edgecolors('k')
		lines.set_linewidth(0.1)
		ax.add_collection(lines)
	 
	plt.savefig('tutorial10.png',dpi=300)
	plt.show()


Result

[<img src="Res/matplotlib_shape.jpg" width="700" height=""/>](Res/matplotlib_shape.jpg)

Still not satisfying.

**Resources**

[Great tutorial showing how to start with matplotlib and basemap](http://introtopython.org/visualization_earthquakes.html)

[Set of start tutorials](http://www.geophysique.be/tutorials/)

[Matplotlib basemap users recipes](http://matplotlib.org/basemap/users/examples.html)

[Matplotlib and GPX -basic example](https://deptinfo-ensip.univ-poitiers.fr/ENS/doku/doku.php?id=stu:visualiseur_gpx)

[Examples of drawing shaded relief images](http://matplotlib.org/basemap/users/geography.html)

[Matplotlib users recipes](http://matplotlib.org/users/recipes.html)

[Using hillshade image as intensity -improved matplotlib shade](http://rnovitsky.blogspot.com/2010/04/using-hillshade-image-as-intensity.html)

[Basemap with shapefiles -more detailed map](http://www.geophysique.be/2011/01/27/matplotlib-basemap-tutorial-07-shapefiles-unleached/)

[Pyevolve](http://pyevolve.sourceforge.net/wordpress/?tag=python&paged=3)

####Mapnik

**Instlation** (from packeges definitly)

Mapnik v2.2.0

	sudo add-apt-repository ppa:mapnik/v2.2.0
	sudo apt-get update
	sudo apt-get install libmapnik libmapnik-dev mapnik-utils python-mapnik

**Basic example**

From [Mapnik - getting started in python](https://github.com/mapnik/mapnik/wiki/GettingStartedInPython)

Code

	import mapnik
	m = mapnik.Map(600,300)
	m.background = mapnik.Color('steelblue')
	s = mapnik.Style()
	r = mapnik.Rule()
	polygon_symbolizer = mapnik.PolygonSymbolizer(mapnik.Color('#f2eff9'))
	r.symbols.append(polygon_symbolizer)
	line_symbolizer = mapnik.LineSymbolizer(mapnik.Color('rgb(50%,50%,50%)'),0.1)
	r.symbols.append(line_symbolizer)
	s.rules.append(r)
	m.append_style('My Style',s)
	ds = mapnik.Shapefile(file='ne_110m_admin_0_countries.shp')
	layer = mapnik.Layer('world')
	layer.datasource = ds
	layer.styles.append('My Style')
	m.layers.append(layer)
	m.zoom_all()
	mapnik.render_to_file(m,'world.png', 'png')
	print "rendered image to 'world.png'"

Result 

[<img src="Res/mapnik_world.png" width="700" height=""/>](Res/mapnik_world.png)

**Very basic gpx example**

Code

	import mapnik
	m = mapnik.Map(1200,1200)
	m.background = mapnik.Color('steelblue')
	s = mapnik.Style()
	r = mapnik.Rule()
	polygon_symbolizer = mapnik.PolygonSymbolizer(mapnik.Color('#f2eff9'))
	r.symbols.append(polygon_symbolizer)
	line_symbolizer = mapnik.LineSymbolizer(mapnik.Color('rgb(50%,50%,50%)'),0.1)
	r.symbols.append(line_symbolizer)
	point_symbolizer = mapnik.MarkersSymbolizer()
	point_symbolizer.allow_overlap = True
	point_symbolizer.opacity = 0.5 # semi-transparent
	r.symbols.append(point_symbolizer)
	s.rules.append(r)
	m.append_style('My Style',s)
	#ds = mapnik.Shapefile(file='borders/ne_110m_admin_0_countries.shp')
	layer = mapnik.Layer('world')
	ds = mapnik.Ogr(file="3.gpx", layer_by_index=2)
	layer.datasource = ds
	layer.styles.append('My Style')
	m.layers.append(layer)
	m.zoom_all()
	mapnik.render_to_file(m,'world.png', 'png')
	print "rendered image to 'world.png'"

Result 

[<img src="Res/mapnik_gpx.png" width="700" height=""/>](Res/mapnik_gpx.png "Mapnik basic example.")

Generating real map requires access to full geospatial data source. In most examples this is sql db server (postgree) with imported data (from OSM for example). It also possible to use data in xml format directly (from OSM again), but it not recomended and there is only one example showing this technology. Anyway it is always sophisticated (a little) technology.

**Resources**

[Instalation](https://github.com/mapnik/mapnik/wiki/UbuntuInstallation "Instalation on Ubuntu")

[Python mapnik example on how to render a map with a gps track on it](http://stackoverflow.com/questions/15691525/python-mapnik-example-on-how-to-render-a-map-with-a-gps-track-on-it)

[Mapnik - getting started in python](https://github.com/mapnik/mapnik/wiki/GettingStartedInPython)

[Some example](http://www.google.de/imgres?client=firefox-a&sa=X&rls=org.mozilla%3Aen-US%3Aofficial&biw=1274&bih=1050&tbm=isch&tbnid=V2oUl0xJDwEBtM%3A&imgrefurl=http%3A%2F%2Fblog.safe.com%2F2011%2F12%2Ffmeevangelist92%2F&docid=y6aAtwC449qpcM&imgurl=http%3A%2F%2Fcdn.blog.safe.com%2Fwp-content%2Fuploads%2F2011%2F11%2Fmapnikvmap0italy.png&w=480&h=411&ei=zS7VUvCLJ8mGswad-4GgDw&zoom=1&iact=rc&dur=1687&page=1&start=0&ndsp=28&ved=0CFkQrQMwAA)

####Other ideas - kartograph

Library (python/js) that allows rendering maps from shape data. It's focused on generating nice charts in SVG. It's possible to render full maps, but it requires Postgree + PostGis and importing data from OSM (same like mapnik).

**Resources**

[Kartograph](http://kartograph.org/)

####Other ideas - OSM Viz

**From web page:** OSMViz is a small set of Python tools for retrieving and using OpenStreetMap (OSM) images (actually, Mapnik images served by Slippy Map). Its original purpose was to draw a bunch of things moving around on the map, which has been somewhat generalized and expanded. 

In general this lib takes tails directly from OSM web server. Interface to tail servers is well documented so there is no secret. Only problem is rendering gpx on tiles.

**Resources**

[Osm Viz](http://cbick.github.io/osmviz/html/index.html "Osm Viz main page")

[Heatmap](http://sethoscope.net/heatmap/ "Heatmap - app using OSm Viz")

[Slippy map tilenamesin Python] (http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Python)

[OSMtiledownloader](http://wiki.openstreetmap.org/wiki/OSMtiledownloader)

#Dynamic
---
##Profile

###Highcharts

[Highcharts](http://www.highcharts.com/) - Start here.

[Highcharts without jQuery](http://jsfiddle.net/highcharts/aEuGP/) - Nice and simple example showing how to use Highcharts without jQuery.

[Highcharts Book](http://www.packtpub.com/learning-highcharts-for-javascript-data-visualization/book)

[![Highcharts Book](http://www.highcharts.com/images/stories/articles/learning-highcharts.jpg)](http://www.packtpub.com/learning-highcharts-for-javascript-data-visualization/book "Highcharts Book")

Simple example based on Highcharts without jQuery. Works

	Start
	---
	<title>asdasd</title>
	<script src="https://rawgithub.com/highslide-software/highcharts.com/master/js/adapters/standalone-framework.src.js"></script>
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	
	<div id="container" style="min-width: 300px; height: 300px; margin: 1em"></div>
	
	<script>	
	var chart = new Highcharts.Chart({
	    chart: {
	        renderTo: 'container'
	    },
	    xAxis: {
	        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 
	            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	    },
	    series: [{
	        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
	    }]	
	});
	</script>
	
	End
	---

Result

[<img src="Res/HcNojQueryResult.jpg" width="700" height=""/>](Res/HcNojQueryResult.jpg "Highcharts without jQuery result")

###D3.js
Data Visualization with d3.js
https://app.packtpub.com/sites/default/files/0007OS.jpg

[D3.js examples](https://github.com/mbostock/d3/wiki/Gallery) - Bunch of incredible examples.

##Map
###OpenLayers

Map definition

	var map; //complex object of type OpenLayers.Map
	map = new OpenLayers.Map("demoMap");

Layers definition

	layerMapnik = new OpenLayers.Layer.OSM("Mapnik");
	layerMarkers = new OpenLayers.Layer.Markers("Markers");
	map.addLayer(layerMapnik);
	map.addLayer(layerMarkers);

Layer for gpx track

	var lgpx04 = new OpenLayers.Layer.Vector("Some Description", {
		strategies: [new OpenLayers.Strategy.Fixed()],
		protocol: new OpenLayers.Protocol.HTTP({
		url: "2013-08-24_10-27-37.gpx",
		format: new OpenLayers.Format.GPX()
		}),
		style: {strokeColor: "red", strokeWidth: 5, strokeOpacity: 0.5},
		projection: new OpenLayers.Projection("EPSG:4326")
		});
	map.addLayer(lgpx04);

Tricky:
 
 * finding start/end gpx point,
 * putting markers there,
 * zooming to loaded gpx.

	
		var size = new OpenLayers.Size(21, 25);
		var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
		var startPointIcon = new OpenLayers.Icon('http://www.openstreetmap.org/openlayers/img/marker.png', size, offset);
		var endPointIcon = new OpenLayers.Icon('http://www.openstreetmap.org/openlayers/img/marker.png', size, offset);
			
		lgpx04.events.register("loadend", lgpx04, function() 
		{
			this.map.zoomToExtent(this.getDataExtent());
		
			var startPoint = this.features[0].geometry.components[0];
			layerMarkers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(startPoint.x, startPoint.y),startPointIcon));
			
			var endPoint = this.features[0].geometry.components[this.features[0].geometry.components.length - 1];
			layerMarkers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(endPoint.x, endPoint.y),endPointIcon));
		});

Complete script ready to put into md document

	<title>OpenLayers Simplest Example</title>
	<script src="http://www.openlayers.org/api/OpenLayers.js"></script>
	<div id="demoMap" style="height:600px"></div>
	<script src="OpenLayers.js"></script>
	<script type="text/javascript">
	
	var map; //complex object of type OpenLayers.Map
	map = new OpenLayers.Map("demoMap");
	
	// Define the map layer
	// Here we use a predefined layer that will be kept up to date with URL changes
	layerMapnik = new OpenLayers.Layer.OSM("Mapnik");
	layerMarkers = new OpenLayers.Layer.Markers("Markers");
	map.addLayer(layerMapnik);
	map.addLayer(layerMarkers);
	
	// Add the Layer with the GPX Track
	var lgpx04 = new OpenLayers.Layer.Vector("Some Description", {
		strategies: [new OpenLayers.Strategy.Fixed()],
		protocol: new OpenLayers.Protocol.HTTP({
		url: "2013-08-24_10-27-37.gpx",
		format: new OpenLayers.Format.GPX()
		}),
		style: {strokeColor: "red", strokeWidth: 5, strokeOpacity: 0.5},
		projection: new OpenLayers.Projection("EPSG:4326")
		});
	map.addLayer(lgpx04);
	
	var size = new OpenLayers.Size(21, 25);
	var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
	var startPointIcon = new OpenLayers.Icon('http://www.openstreetmap.org/openlayers/img/marker.png', size, offset);
	var endPointIcon = new OpenLayers.Icon('http://www.openstreetmap.org/openlayers/img/marker.png', size, offset);
	
	lgpx04.events.register("loadend", lgpx04, function() 
		{
	  	this.map.zoomToExtent(this.getDataExtent());
	
	  	var startPoint = this.features[0].geometry.components[0];
	  	layerMarkers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(startPoint.x, startPoint.y),startPointIcon));
	
		var endPoint = this.features[0].geometry.components[this.features[0].geometry.components.length - 1];
	  	layerMarkers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(endPoint.x, endPoint.y),endPointIcon));
	});

	</script>
 
Result

[<img src="Res/JavaScriptResult.jpg " width="700" height=""/>](Res/JavaScriptResult.jpg "Java script result")

###Leaflet

[Start Here - basic examples](http://leafletjs.com/examples/quick-start.html)

[General examples](http://blog.thematicmapping.org/search/label/Leaflet)

[Gpx plotting example](http://blog.thematicmapping.org/2012/08/showing-gps-tracks-with-leaflet.html)

[Plugins](https://github.com/shramov/leaflet-plugins) - from web page: *Miscellaneous plugins for Leaflet library for services that need to display route information and need satellite imagery from different providers.*

[GPX plugin for Leaflet](https://github.com/mpetazzoni/leaflet-gpx) - from web page: *Leaflet is a Javascript library for displaying interactive maps. This plugin, based on the work of Pavel Shramov and his leaflet-plugins, it allows for the analysis and parsing of a GPX track in order to display it as a Leaflet map layer. As it parses the GPX data, it will record information about the recorded track, including total time, moving time, total distance, elevation stats and heart-rate.*

[Leaflet.Elevation](https://github.com/MrMufflon/Leaflet.Elevation) - from web page: *A Leaflet plugin to view an interactive height profile of polylines lines using d3. Currently Chrome and Firefox are supported and tested.*

**Simple map**

	<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.1/leaflet.css" />
	<script src="http://cdn.leafletjs.com/leaflet-0.7.1/leaflet.js"></script>
	<div id="map" style="width: 600px; height: 600px"></div>
	<script>
	var map = L.map('map').setView([51.505, -0.09], 13);

	L.tileLayer('http://tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/997/256/{z}/{x}/{y}.png', {attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>', maxZoom: 18}).addTo(map);

	var marker = L.marker([51.5, -0.09]).addTo(map);

	</script>

Result

[<img src="Res/Leaflet/LeafletScriptResult_Basic.jpg " width="700" height=""/>](Res/Leaflet/LeafletScriptResult_Basic.jpg "Leaflet script result")

**Map and gpx** - based on [Leaflet.Elevation]

	<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.1/leaflet.css" />
	<script src="http://cdn.leafletjs.com/leaflet-0.7.1/leaflet.js"></script>
	<script type="text/javascript" src="Res/js/LfElevation/lib/leaflet-gpx/gpx.js"></script>

	<div id="map" style="width: 600px; height: 600px"></div>

	<script>
	var map = L.map('map').setView([50.242656, 16.736217], 13);

	L.tileLayer('http://tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/997/256/{z}/{x}/{y}.png', {attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>', maxZoom: 18}).addTo(map);

	var marker = L.marker([50.242656, 16.736217]).addTo(map);

	var gpx = 'Res/gpxData/3.gpx'; // URL to your GPX file or the GPX itself
	new L.GPX(gpx, {async: true}).on('loaded', function(e) {map.fitBounds(e.target.getBounds());}).addTo(map);

	</script>

Result

[<img src="Res/Leaflet/LeafletScriptResult_Gpx.jpg " width="700" height=""/>](Res/Leaflet/LeafletScriptResult_Gpx.jpg "Leaflet script result")

**Map, gpx and progile** - based on [Leaflet.Elevation]

	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<style>	html, body, #map {height:100%; width:100%; padding:0px;	margin:0px;}</style>
	<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
	<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.6.2/leaflet.css" />
	<!--[if lte IE 8]><link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.6.2/leaflet.ie.css" /><![endif]-->

	<link rel="stylesheet" href="Res/js/LfElevation/dist/Leaflet.Elevation-0.0.1.css" />

	<script type="text/javascript" src="http://cdn.leafletjs.com/leaflet-0.6.2/leaflet.js"></script>
	<script type="text/javascript" src="Res/js/LfElevation/dist/Leaflet.Elevation-0.0.1.min.js"></script>
	<script type="text/javascript" src="Res/js/LfElevation/lib/leaflet-gpx/gpx.js"></script>

	<div id="map" style="width: 800px; height: 600px"></div>

	<script type="text/javascript">
	var map = new L.Map('map').setView([50.242656, 16.736217], 13);

	var url = 'http://otile{s}.mqcdn.com/tiles/1.0.0/map/{z}/{x}/{y}.jpeg',	attr ='Tiles Courtesy of <a href="http://www.mapquest.com/">MapQuest</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>', service = new L.TileLayer(url, {subdomains:"1234",attribution: attr});

	var el = L.control.elevation();
	el.addTo(map);
	var g=new L.GPX("Res/gpxData/3.gpx", {
		async: true,
			marker_options: {
			startIconUrl: 'Res/js/LfElevation/lib/leaflet-gpx/pin-icon-start.png',
			endIconUrl: 'Res/js/LfElevation/lib/leaflet-gpx/pin-icon-end.png',
			shadowUrl: 'Res/js/LfElevation/lib/leaflet-gpx/pin-shadow.png'
			}
	});
	g.on('loaded', function(e) {map.fitBounds(e.target.getBounds());});
	g.on("addline",function(e) {el.addData(e.line);});
	g.addTo(map);
	map.addLayer(service);

	</script>

Result

[<img src="Res/Leaflet/LeafletScriptResult_GpxProfile.jpg " width="700" height=""/>](Res/Leaflet/Leaflet/LeafletScriptResult_GpxProfile.jpg "Leaflet script result")

Line

	var map = new L.Map('map').setView([50.242656, 16.736217], 13);

was

	var map = new L.Map('map');

but this version was not working (why?)!

###Cesium

[Main page](http://cesiumjs.org/) - from web page: Cesium is a JavaScript library for creating 3D globes and 2D maps in a web browser without a plugin. It uses WebGL for hardware-accelerated graphics, and is cross-platform, cross-browser, and tuned for dynamic-data visualization. Cesium is open source under the Apache 2.0 license. It is free for commercial and non-commercial use.

Cesium example

[<img src="Res/OneApiThreeViews.png " width="700" height=""/>](Res/OneApiThreeViews.png "Cesium example") 

##Profile and map

###GPXViewer

[GPXViewer](http://www.j-berkemeier.de/GPXViewer/)

Declarations
	
	<title>Wielka dookólna</title>
	<script type="text/javascript">
	//<![CDATA[
	var key="ABQIAAAAs22JbJ19Avy7ky8nonMq3RS3NFTGfhGWCHSiir3tQFp3ertdYBSq_t3SickfeQU_qh240JuSk31R0Q";
	//]]>
	</script>
	<script type="text/javascript" src="GM_Utils/GPX2GM.js"></script>

Visualization

	<div style="margin:auto;width:1020px">
	<h1>Wielka dookólna</h1>
	<div id="map1" class="gpxview:2013-05-18_07-19-33.gpx,2013-05-30_08-18-46.gpx,2013-07-20_10-31-00.gpx,2013-08-03_09-00-01.gpx,2013-08-24_10-27-37.gpx:Karte" style="width:500px;height:500px;float:left"><noscript><p>Zum Anzeigen der Karte wird Javascript ben&ouml;tigt.</p></noscript></div>
	<div style="float:left;padding-left:20px">
	<div id="map1_hp" class="no_x" style="width:500px;height:200px;margin-top:10px"><noscript><p>Zum Anzeigen des Profils wird Javascript ben&ouml;tigt.</p></noscript></div>
	<div id="map1_sp" class="no_x" style="width:500px;height:200px;margin-top:-37px"><noscript><p>Zum Anzeigen des Profils wird Javascript ben&ouml;tigt.</p></noscript></div>
	<div id="map1_vp" style="width:500px;height:200px;margin-top:-37px"><noscript><p>Zum Anzeigen des Profils wird Javascript ben&ouml;tigt.</p></noscript></div>
	
	</div>
	     <p style="clear:both"></p>
	     <hr /><p>3. 11. 2009 <a href="http://www.j-berkemeier.de">J&uuml;rgen Berkemeier</a></p>
	</div>

Complete script ready to put into md document

	<title>Wielka dookólna</title>
	<script type="text/javascript">
	//<![CDATA[
	var key="ABQIAAAAs22JbJ19Avy7ky8nonMq3RS3NFTGfhGWCHSiir3tQFp3ertdYBSq_t3SickfeQU_qh240JuSk31R0Q";
	//]]>
	</script>
	<script type="text/javascript" src="GM_Utils/GPX2GM.js"></script>
	
	<div style="margin:auto;width:1020px">
	<h1>Wielka dookólna</h1>
	<div id="map1" class="gpxview:2013-05-18_07-19-33.gpx,2013-05-30_08-18-46.gpx,2013-07-20_10-31-00.gpx,2013-08-03_09-00-01.gpx,2013-08-24_10-27-37.gpx:Karte" style="width:500px;height:500px;float:left"><noscript><p>Zum Anzeigen der Karte wird Javascript ben&ouml;tigt.</p></noscript></div>
	<div style="float:left;padding-left:20px">
	<div id="map1_hp" class="no_x" style="width:500px;height:200px;margin-top:10px"><noscript><p>Zum Anzeigen des Profils wird Javascript ben&ouml;tigt.</p></noscript></div>
	<div id="map1_sp" class="no_x" style="width:500px;height:200px;margin-top:-37px"><noscript><p>Zum Anzeigen des Profils wird Javascript ben&ouml;tigt.</p></noscript></div>
	<div id="map1_vp" style="width:500px;height:200px;margin-top:-37px"><noscript><p>Zum Anzeigen des Profils wird Javascript ben&ouml;tigt.</p></noscript></div>
	
	</div>
	     <p style="clear:both"></p>
	     <hr /><p>3. 11. 2009 <a href="http://www.j-berkemeier.de">J&uuml;rgen Berkemeier</a></p>
	</div>

Result

[<img src="Res/GM_Utils.jpg" width="700" height=""/>](Res/GM_Utils.jpg "GM_Utils result") 

Resources
---
<!--
nice markdown trick
[<img src="http://www.highcharts.com/images/stories/articles/learning-highcharts.jpg">](http://www.packtpub.com/learning-highcharts-for-javascript-data-visualization/book)
-->

[![Highcharts Book](http://www.highcharts.com/images/stories/articles/learning-highcharts.jpg)](http://www.packtpub.com/learning-highcharts-for-javascript-data-visualization/book)

[![MatPlotLib Book](http://dgdsbygo8mp3h.cloudfront.net/sites/default/files/imagecache/productview_larger/bookimages/7900_MockupCover.jpg)](http://www.packtpub.com/matplotlib-python-development/book)

[![Learning Geospatial Analysis with Python Book](http://my.safaribooksonline.com/static/201401-7540-my/images/9781783281138/9781783281138_s.jpg)](http://my.safaribooksonline.com/book/programming/python/9781783281138)

[![Python Data Visualization Cookbook](http://dgdsbygo8mp3h.cloudfront.net/sites/default/files/imagecache/productview_larger/3367OS.jpg)](http://www.packtpub.com/article/plotting-charts-with-images-maps)

---
[How to deploy a jekyll site locally]: http://stackoverflow.com/questions/7985081/how-to-deploy-a-jekyll-site-locally-with-css-js-and-background-images-included

[Using cartopy with matplotlib]: http://www.scitools.org.uk/cartopy/docs/v0.4/matplotlib/intro.html

[http://introtopython.org/visualization_earthquakes.html]: http://introtopython.org/visualization_earthquakes.html

echo 'ojccmfAefwfmtfd/psh' | perl -pe 's/(.)/chr(ord($1)-1)/ge'
nibble@develsec.org

