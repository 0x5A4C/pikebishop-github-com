---
layout: page
title: "8051 Modules - pictures"
description: "8051 Modules - pictures"
---
{% include JB/setup %}

<script type="text/javascript" src="highslide/highslide-with-gallery.js"></script>
<script type="text/javascript" src="highslide/highslide.config.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="highslide/highslide-ie6.css" />
<![endif]-->

<h3>Gallery</h3>
<div class="highslide-gallery">
<ul>
<li>
<a href="highslide/images/large/IMG_2550.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2550.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2554.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2554.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2551.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2551.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2558.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2558.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2560.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2560.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2564.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2564.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2575.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2575.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2573.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2573.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2569.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2569.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2561.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2561.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2584.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2584.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2572.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2572.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2566.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2566.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2581.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2581.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2586.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2586.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2553.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2553.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2583.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2583.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2565.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2565.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2557.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2557.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2577.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2577.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2576.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2576.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2555.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2555.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2568.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2568.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2563.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2563.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2588.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2588.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2574.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2574.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2571.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2571.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2578.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2578.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2582.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2582.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2570.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2570.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2556.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2556.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2580.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2580.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2552.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2552.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2562.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2562.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2579.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2579.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2587.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2587.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2559.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2559.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2567.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2567.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2549.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2549.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/MatPlotLibBook.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/MatPlotLibBook.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2585.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2585.jpg"  alt=""/>
</a>
</li>
<li>
<a href="highslide/images/large/IMG_2589.jpg" class="highslide" 
title="" 
onclick="return hs.expand(this, config1 )">
<img src="highslide/images/thumbs/IMG_2589.jpg"  alt=""/>
</a>
</li>
</ul>
<div style="clear:both"></div></div>








