---
layout: page
title: "Power Supply"
description: "Power supply from PC module"
---
{% include JB/setup %}

##Power supply from PC module##


##NPS-700AB##
[**Source**](https://sites.google.com/site/tjinguytech/my-projects/server-ps)

[NPS-700AB](Res/NPS-700AB.jpg)
[Pins](Res/NPS700ABAA23300_Pins.jpg)

[<img src="Res/NPS-700AB.jpg" width="" height="300"/>](Res/NPS-700AB.jpg "NPS-700AB")
[<img src="Res/NPS700ABAA23300_Pins.jpg" width="" height="300"/>](Res/NPS700ABAA23300_Pins.jpg "NPS-700AB")

##AA23300##
[**Source**](https://sites.google.com/site/tjinguytech/my-projects/server-ps)

[AA23300](Res/AA23300.JPG)
[Pins](Res/NPS700ABAA23300_Pins.jpg)

[<img src="Res/AA23300.JPG" width="" height="300"/>](Res/AA23300.JPG "AA23300")
[<img src="Res/NPS700ABAA23300_Pins.jpg" width="" height="300"/>](Res/NPS700ABAA23300_Pins.jpg "NPS-700AB")


##DPS-600PB##
[**Source**](https://sites.google.com/site/tjinguytech/my-projects/HP47A)

[DPS-600PB](Res/DPS-600PB.jpg)
[Pins](Res/DPS-600PB_Pins.jpg)

[<img src="Res/DPS-600PB.jpg" width="" height="300"/>](Res/DPS-600PB.jpg "DPS-600PB")
[<img src="Res/DPS-600PB_Pins.jpg" width="" height="300"/>](Res/DPS-600PB_Pins.jpg "DPS-600PB")


##DS1300-3 - HP PROLIANT DL580/ML570G3 RPS REDUNDANT POWER SUPPLY. P/N: 348114R-B21##
[**Source**](http://www.rcgroups.com/forums/showthread.php?t=1005309)

[DTR](Res/DS1300-3 Series.pdf)
[DS1300-3](Res/DS1300-3.jpg)
[Pins](Res/DPS-600PB_Pins.jpg)

[<img src="Res/DS1300-3.jpg" width="" height="300"/>](Res/DS1300-3.jpg "DS1300-3")
[<img src="Res/DPS-600PB_Pins.jpg" width="" height="300"/>](Res/DPS-600PB_Pins.jpg "DS1300-3")


##DPS-500CB A##
[**Source**](http://rc-fpv.pl/viewtopic.php?p=118106)

[DPS-500CB A](Res/DPS-500CB A.jpg)

Pins:

    N N N N N X
    N N N N X N
    N N N X N N
    N N N N N N

[<img src="Res/DPS-500CB A.jpg" width="" height="300"/>](Res/DPS-500CB A.jpg "DPS-500CB A")

---

[**General and compleat thread on www.rcgroups.com**](http://www.rcgroups.com/forums/showthread.php?t=1292514)

*From web page*

*Thought it might be useful to gather this in the one place.
The practice of converting computer SERVER power supplies to make a very high quality high power supply at low cost is attractive. It doesn’t normally require any modification to the supply but the hard part is figuring out how to trick them into turning on. Far and away the best way is to find someone who has already done it. I have put down a few references I know about and welcome additions.*

##IBM Series 235 Supply##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=8908033&postcount=3)

[<img src="Res/IBM Series 235 Supply.jpg" width="" height="300"/>](Res/IBM Series 235 Supply.jpg "IBM Series 235 Supply")

##Compaq HP ESP114##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=14001273&postcount=1)

[<img src="Res/Compaq HP ESP114.jpg" width="" height="300"/>](Res/Compaq HP ESP114.jpg "Compaq HP ESP114")

##Dell A570P-00 570W##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=15308102&postcount=130)

[<img src="Res/Dell A570P-00 570W.jpg" width="" height="300"/>](Res/Dell A570P-00 570W.jpg "Dell A570P-00 570W")

##Compaq Proliant 169286-002 750W##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=15289601&postcount=128)
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=15391055&postcount=141)

[<img src="Res/Compaq Proliant 169286-002 750W.jpg" width="" height="300"/>](Res/Compaq Proliant 169286-002 750W.jpg "Compaq Proliant 169286-002 750W")

##HP Power Supply DPS-600PB/700CB##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=15794997&postcount=253)

[<img src="Res/HP Power Supply DPS-600PB.jpg" width="" height="300"/>](Res/HP Power Supply DPS-600PB.jpg "HP Power Supply DPS-600PB")

##PS-3381-1C1 PSU##
[**Source**](http://www.rcgroups.com/forums/showthread.php?t=358340&page=2)

[<img src="Res/PS-3381-1C1.jpg" width="" height="300"/>](Res/PS-3381-1C1.jpg "PS-3381-1C1")

##Compaq Model DPS-450BB ( Delta Model number DPS-450BP)##
Series ESP104
P/N: 401401-001
Spare/N: 101902-001
[**Source**](http://www.rcgroups.com/forums/showthread.php?t=1076321)

[<img src="Res/DPS-450BB.jpg" width="" height="300"/>](Res/DPS-450BB.jpg "DPS-450BB")

##Hewlett Packard HP-194989 (400W, 32A max. on the 12v line) / Hewlett Packard HP-280127 (325W, 26A max. on the 12v line)##
[**Source**](http://ohiopacket.org/files/docs/Powersup.pdf)

[<img src="Res/HP-194989.jpg" width="" height="300"/>](Res/HP-194989.jpg "HP-194989")

##Dell NPS-730AB 12V 60A supply##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=16734096&postcount=375)

[<img src="Res/NPS-730AB.jpg" width="" height="300"/>](Res/NPS-730AB.jpg "NPS-730AB")

##HP/Compaq Proliant DL380 G4 575w Power Supply, 406393-001##
Series ESP135 Model No PS-3601-1C GPN:367238-501 PartNo:366982-501 Spares No:406393-001
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=17042178&postcount=385)

[<img src="Res/Proliant DL380 G4 575w Power Supply.jpg" width="" height="300"/>](Res/Proliant DL380 G4 575w Power Supply.jpg "Proliant DL380 G4 575w Power Supply")

##HP model DPS-600PB series ESP135##
[**Source**](http://api.viglink.com/api/click?format=go&jsonp=vglnk_142059537445410&key=8a24c98a696b4e5723db293f62190b87&libId=910b1240-afb6-4276-9e54-a9e70d82fbec&loc=http%3A%2F%2Fwww.rcgroups.com%2Fforums%2Fshowthread.php%3Ft%3D1292514&v=1&out=https%3A%2F%2Fsites.google.com%2Fsite%2Ftjinguytech%2Fcharging-how-tos&title=How%20to%20convert%20Server%20Power%20Supplies%20-%20RC%20Groups&txt=https%3A%2F%2Fsites.google.com%2Fsite%2Ftjingu...arging-how-tos)

[<img src="Res/HP model DPS-600PB series ESP135.jpg" width="" height="300"/>](Res/HP model DPS-600PB series ESP135.jpg "HP model DPS-600PB series ESP135")

##Dell - Model NPS330BB A##
[**Source**](http://www.rcgroups.com/forums/showthread.php?t=1382457&highlight=nps330bb)

[<img src="Res/Dell - Model NPS330BB A.jpg" width="" height="300"/>](Res/Dell - Model NPS330BB A.jpg "Dell - Model NPS330BB A")

##Dell NPS-730AB A REV A01##
taken from Dell Power Edge 2600
max 13,5 Volt 60 Amps
[**Source**](http://www.rcgroups.com/forums/showthread.php?t=1005309&highlight=dell+nps+730ab#post17197180)

[<img src="Res/Dell NPS-730AB.jpg" width="" height="300"/>](Res/Dell NPS-730AB.jpg "Dell NPS-730AB")

##Dell PowerEdge 2850 power supply, 12V 57A (700W)##
Model #: Dell NPS-700AB
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=17023400&postcount=408)

[<img src="Res/Dell PowerEdge 2850.jpg" width="" height="300"/>](Res/Dell PowerEdge 2850.jpg "Dell PowerEdge 2850")

##HP ESP-115 (30 amps)##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=16791677&postcount=379)

[<img src="Res/HP ESP-115.jpg" width="" height="300"/>](Res/HP ESP-115.jpg "HP ESP-115")

##Fujitsu-Siemens Primepower 450 server ps##
[**Source**](http://www.rcgroups.com/forums/showthread.php?t=1263123)

[<img src="Res/Fujitsu-Siemens Primepower 450 server ps.jpg" width="" height="300"/>](Res/Fujitsu-Siemens Primepower 450 server ps.jpg "Fujitsu-Siemens Primepower 450 server ps")

##Dell 7000814-0000 700W PSU##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=15469162&postcount=154)

[<img src="Res/Dell 7000814-0000 700W PSU.jpg" width="" height="300"/>](Res/Dell 7000814-0000 700W PSU.jpg "Dell 7000814-0000 700W PSU")

##Hp PS3381-1C1 PSU##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=14205992&postcount=7)

[<img src="Res/Hp PS3381-1C1 PSU.jpg" width="" height="300"/>](Res/Hp PS3381-1C1 PSU.jpg "Hp PS3381-1C1 PSU")

##HP 511777-001 460W 12V PSU##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=15599551&postcount=167)

[<img src="Res/HP 511777-001.jpg" width="" height="300"/>](Res/HP 511777-001.jpg "HP 511777-001")

##Dell ASTN 7000245 Poweredge 6650 PS for 72 amps at 12v.##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=17653072&postcount=457)

[<img src="Res/Poweredge 6650 PS.jpg" width="" height="300"/>](Res/Poweredge 6650 PS.jpg "Poweredge 6650 PS ")

##Esp 108 (DPS-450cb) PSU full pinout##
[**Source**](http://www.rcgroups.com/forums/showthread.php?t=1005309)
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=13242673&postcount=13)

[<img src="Res/DPS-450cb.jpg" width="" height="300"/>](Res/DPS-450cb.jpg "DPS-450cb")

##Dell Poweredge 6650(7000245) PS - Voltage Adjust##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=18204933&postcount=654)

[<img src="Res/Dell Poweredge 6650.jpg" width="" height="300"/>](Res/Dell Poweredge 6650.jpg "Dell Poweredge 6650")

##Dell Power Edge 4600 Server PS##
See two posts 601 and 602 here
[**Source**](http://www.rcgroups.com/forums/showthread.php?t=1005309)

[<img src="Res/Dell Power Edge 4600 Server PS.jpg" width="" height="300"/>](Res/Dell Power Edge 4600 Server PS.jpg "Dell Power Edge 4600 Server PS")

##Dell DPS-500CB A##
[**Source**](http://www.rcgroups.com/forums/showpost.php?p=18749844&postcount=811)

[<img src="Res/Dell DPS-500CB A.jpg" width="" height="300"/>](Res/Dell DPS-500CB A.jpg "Dell DPS-500CB A")

##Delta DPS800GB-A##
[**Source**](http://www.rcgroups.com/forums/showthread.php?t=1005309)
Posts 880 and 881

[<img src="Res/Delta DPS800GB-A.jpg" width="" height="300"/>](Res/Delta DPS800GB-A.jpg "Delta DPS800GB-A")
