---
layout: page
title: "Panel Multimeter"
description: ""
---
{% include JB/setup %}

##Panel Multimeter##

AVT5415
AVTMOD16
AVT5399
AVT5333
AVT5300


http://www.elfly.pl/multimetr/
http://www.elfly.pl/multimetr/multimetr.htm
http://elfly.pl/multimetr/multimetr_DIP.htm
http://www.elektroda.pl/rtvforum/topic620045.html








[**WS-9040-IT**](http://www.technoline.eu/details.php?id=1582&kat=5)

[<img src="Res/WS 9040 IT.jpg" width="" height="300"/>](Res/WS 9040 IT.jpg "WS 9040 IT")

[**WS-9130-IT**](http://www.technoline.eu/details.php?id=1521&kat=5)

[<img src="Res/WS 9130-IT.jpg" width="" height="300"/>](Res/WS 9130-IT.jpg "WS 9130 IT")

[**WM-5012**](http://www.technoline.eu/details.php?id=1470&kat=79)

[<img src="Res/WM 5012.jpg" width="" height="300"/>](Res/WM 5012.jpg "WM 5012")

##Current consumption##

**WS-9040-IT**     - 10,8 mA

**WS-9130-IT** (1) - 11.24 mA

**WS-9130-IT** (2) - 12.20 mA

**TX37-IT**    (2) - 0.5 - 0.1 mA (peak)

**TX37-IT**    (1) - 0.3 - 0.1 mA (peak)

**TX38-IT**        - 0.5 mA (peak)

**WM-5012**        - 0.5 mA (?)

##Manuals##

[WS-9040-IT](Res/Manuals/WS-9040-IT.pdf)

[WS-9130-IT](Res/Manuals/WS-9130-IT.pdf)

[WM-5012](Res/Manuals/WM-5012.pdf)


**WM 512 weather data regions** ([source](http://www.meteotronic.info/index.php?pays=schweiz&id=32&lang=de))

[<img src="Res/WM5012Regions.jpg" width="" height="100"/>](Res/WM5012Regions.jpg "WM 512 weather data regions")

        Reg Länder 	       Städte
        0	Frankreich	   Bordeaux
        1	Frankreich	   La Rochelle
        2	Frankreich	   Paris
        3	Frankreich	   Brest
        4	Frankreich	   Clermont-Ferrand
        5	Frankreich	   Béziers
        6	Belgien	       Brüssel
        7	Frankreich	   Dijon
        8	Frankreich	   Marseilles
        9	Frankreich	   Lyon
        10	Frankreich	   Grenoble
        11	Schweiz	       La Chaux de Fonds
        12	Deutschland	   Frankfurt am Main
        13	Belgien, Luxemburg, Deutschland	Wallonne
        14	Deutschland	   Duisburg
        15	Großbritannien Swansea
        16	Großbritannien Manchester
        17	Frankreich	   Le Havre
        18	Großbritannien London
        19	Deutschland	   Bremerhaven
        20	Dänemark	   Herning
        21	Dänemark	   Arhus
        22	Deutschland	   Hannover
        23	Dänemark	   Kopenhagen
        24	Deutschland	   Rostock
        25	Deutschland	   Ingolstadt
        26	Deutschland	   München
        27	Italien	       Bozen
        28	Deutschland	   Nürnberg
        29	Deutschland	   Leipzig
        30	Deutschland	   Erfurt
        31	Schweiz        Lausanne
        32	Schweiz        Zürich
        33	Schweiz	       Adelboden
        34	Schweiz	       Sion
        35	Schweiz	       Glarus
        36	Schweiz	       Davos
        37	Deutschland	   Kassel
        38	Schweiz	       Locarno
        39	Italien	       Sestriere
        40	Italien	       Mailand
        41	Italien	       Rom
        42	Niederlande	   Amsterdam
        43	Italien	       Genua
        44	Italien	       Venedig
        45	Frankreich	   Straßburg
        46	Österreich	   Klagenfurt
        47	Österreich	   Innsbruck
        48	Österreich	   Salzburg
        49	Slowakei	   Bratislava
        50	Tschechien	   Prag
        51	Tschechien	   Decin
        52	Deutschland	   Berlin
        53	Schweden	   Göteborg
        54	Schweden	   Stockholm
        55	Schweden	   Kalmar
        56	Schweden	   Jönköping
        57	Deutschland	   Donaueschingen
        58	Norwegen	   Oslo
        59	Deutschland	   Stuttgart
        60	Italien	       Neapel
        61	Italien	       Ancona
        62	Italien	       Bari
        63	Ungarn	       Budapest
        64	Spanien	       Madrid
        65	Spanien	       Bilbao
        66	Italien	       Palermo
        67	Spanien	       Palma de Mallorca
        68	Spanien	       Valencia
        69	Spanien	       Barcelona
        70	Andorra	       Andorra
        71	Spanien	       Sevilla
        72	Portugal	   Lissabon
        73	Italien	       Sassari
        74	Spanien	       Gijon
        75	Irland	       Galway
        76	Irland	       Dublin
        77	Großbritannien Glasgow
        78	Norwegen	   Stavanger
        79	Norwegen	   Trondheim
        80	Schweden	   Sundswall
        81	Polen	       Danzig
        82	Polen	       Warschau
        83	Polen	       Krakau
        84	Schweden       Umeå
        85	Schweden	   Östersund
        86	Schweiz	       Samedan
        87	Kroatien       Zagreb
        88	Schweiz	       Zermatt
        89	Kroatien	   Split


