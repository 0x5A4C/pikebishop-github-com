---
layout: page
title: "Photo gallery"
description: "Java script photo gallery embeded into markdown."
---
{% include JB/setup %}


###Highslide
---

[Highslide] - main page.

	<script type="text/javascript" src="highslide/highslide-with-gallery.js"></script>
	<script type="text/javascript" src="highslide/highslide.config.js" charset="utf-8"></script>
	<link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
	<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="highslide/highslide-ie6.css" />
	<![endif]-->

	<h3>Gallery</h3>
	<div class="highslide-gallery">
	<ul>
	<li>
	<a href="highslide/images/large/IMG_2550.jpg" class="highslide" 
	title="" 
	onclick="return hs.expand(this, config1 )">
	<img src="highslide/images/thumbs/IMG_2550.jpg"  alt=""/>
	</a>
	</li>
	<li>
	<a href="highslide/images/large/IMG_2554.jpg" class="highslide" 
	title="" 
	onclick="return hs.expand(this, config1 )">
	<img src="highslide/images/thumbs/IMG_2554.jpg"  alt=""/>
	</a>
	</li>
	<li>
	<a href="highslide/images/large/IMG_2551.jpg" class="highslide" 
	title="" 
	onclick="return hs.expand(this, config1 )">
	<img src="highslide/images/thumbs/IMG_2551.jpg"  alt=""/>
	</a>
	</li>
	.
	.
	.
	</ul>
	<div style="clear:both"></div></div>

<!--
![image](Res/Highslide_00.jpg)
![image](Res/Highslide_01.jpg)
-->

![image](Res/Highslide_00.jpg)
![image](Res/Highslide_01.jpg)

###LightBox
---

[Lightbox] - main page.

	<script src="/js/jquery-1.10.2.min.js"> </script>
	<script src="/js/lightbox-2.6.min.js"> </script>
	<link href="/css/lightbox.css" rel="stylesheet" />

	<a href="Res/IMG_2549.JPG" data-lightbox="IMG_2549" title="my caption"><img class="example-image" src="Res/IMG_2549.JPG" alt="thumb-1" width="150" height="150"/></a>
	<a href="Res/IMG_2550.JPG" data-lightbox="IMG_2550" title="my caption"><img class="example-image" src="Res/IMG_2550.JPG" alt="thumb-1" width="150" height="150"/></a>

<!--
![image](Res/Lightbox.jpg)
-->

<!-- Resources and links -->
[Highslide]:http://highslide.com/
[Lightbox]:http://lokeshdhakar.com/projects/lightbox/





