---
layout: page
title: "Logitech Mx Master in Linux (aka Python is AWESOME)"
description: ""
---
{% include JB/setup %}

[<img src="Res/IMG_2252.JPG" width="" height="300"/>](Res/IMG_2252.JPG "Mx")

# Motivation and target

Use second scrool for volume adjustment.

# The plan

Grab mouse events and trig expected reaction - in background (demon).

# Classic attempt

xbindkeys xautomation
complicated configuration - failed on reading examples :-(

Btnx
sudo apt install btnx - failed - not present in ubuntu repo (since 11.xx) - have to be build from sources.

Eeeeeee let's skip it.

# Right way

There is only one right way - Python.

## Grab mouse events

[pynput](http://pythonhosted.org/pynput/mouse.html)

> sudo pip3 install pynput

    from pynput.mouse import Listener

    def on_move(x, y):
        print('Pointer moved to {0}'.format(
            (x, y)))

    def on_click(x, y, button, pressed):
        print('{0} at {1}'.format(
            'Pressed' if pressed else 'Released',
            (x, y)))
        if not pressed:
            # Stop listener
            return False

    def on_scroll(x, y, dx, dy):
        print('Scrolled {0}'.format(
            (x, y)))

    # Collect events until released
    with Listener(
            on_move=on_move,
            on_click=on_click,
            on_scroll=on_scroll) as listener:
        listener.join()

## Change volume

[pyalsaaudio](https://github.com/larsimmisch/pyalsaaudio)

> sudo apt install libasound2-dev

> sudo pip3 install pyalsaaudio

    import alsaaudio
    m = alsaaudio.Mixer()   # defined alsaaudio.Mixer to change volume
    m.setvolume(50) # set volume
    vol = m.getvolume() # get volume float value

## Demon him

[demonize](https://github.com/thesharp/daemonize)

> sudo pip install daemonize

    import os, sys
    from daemonize import Daemonize

    def main()
          # your code here

    if __name__ == '__main__':
            myname=os.path.basename(sys.argv[0])
            pidfile='/tmp/%s' % myname       # any name
            daemon = Daemonize(app=myname,pid=pidfile, action=main)
            daemon.start()

## That's all folks

    import os
    import sys
    import getopt
    import alsaaudio

    from pynput.mouse import Listener

    from daemonize import Daemonize

    def on_move(x, y):
        pass

    def on_click(x, y, button, pressed):
        pass

    def on_scroll(x, y, dx, dy):
        m = alsaaudio.Mixer()
        vol = m.getvolume()
        nVol = vol[0]+(dx*-1*5) # dx * scroll direction * speed factor
        if nVol > 100:
            nVol = 100
        if nVol < 0:
            nVol = 0
        m.setvolume(nVol)
        vol = m.getvolume()

    def main():
        with Listener(
                on_move=on_move,
                on_click=on_click,
                on_scroll=on_scroll) as listener:
            listener.join()

    if __name__ == '__main__':
            myname=os.path.basename(sys.argv[0])
            pidfile='/tmp/%s' % myname       # any name
            daemon = Daemonize(app=myname,pid=pidfile, action=main)
            daemon.start()


Works - thats it :-)

[<img src="Res/vol.jpg" width="" height="100"/>](Res/vol.jpg "volume")

---
