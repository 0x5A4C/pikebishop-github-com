---
layout: page
title: "OMP - NoiseTrap"
description: ""
---
{% include JB/setup %}

---

# Objectives #

Continuously analyse sound level and generate alarm when too high.

# 5 minute concept/design #

Software tasks:

- monitor sound level,
- trig alarm,
- display sound and alarm level,
- set alarm level.

Hardware components:

- input: simple microphone amplifier,
- output: simple beeper,
- central unit: Arduino for 1sth prototype,
- ui: some shield with display and manipulator.  


**Central unit** - Arduino clone
[<img src="Res/funduino.jpg" alt="Central unit" width="" height="200"/>](Res/funduino.jpg)

**UI (two variants)** - Arduino shield
[<img src="Res/avt1615.jpg" alt="Ui" width="" height="200"/>](Res/avt1615.jpg)
[<img src="Res/avt1722.jpg" alt="Input" width="" height="200"/>](Res/avt1722.jpg)

**Input** - microphne amplifier - simple kit 
[<img src="Res/avt1721.jpg" alt="Ui" width="" height="200"/>](Res/avt1721.jpg)

**Output** - buzzer 
[<img src="Res/buzzer.jpg" alt = "output" width="" height="200"/>](Res/buzzer.jpg)

# 3457 seconds implementation #

**Software**

Variant 1

Simple, sequenced application. Signal filtered in round buffer. Levels displayed on LCD.
Alarm can be changed by rotary encoder (for both ui). Biggest problem is slow encoder (checked in loop).

Variant 2

Based on ChibiOS port. Separated task for encoder, data acquisition and LCD display.
Problems: consumes lot of RAM, encoder is not faster.

**Hardware** 

Variant 1

One microphone

Variant 2 

Two microphones. This should help in controlling area with wide angle.

# MEMS microphone variant #

Mems microphone was considered as simple (from application point) solution for input block.
For tests AD... was choose.

Problems:

- very small package - really small (microscope necessary)
- signal is code in PDM format - decoding signal with FIR filter is quite complicated (this is topic for separate project). 

Will not be continued... 

**Mems microphone breakboard (by Dariusz "The Incredible")**

[<img src="Res/MEMS00.jpg" width="" height="200"/>](Res/MEMS00.jpg)

[<img src="Res/MEMS01.jpg" width="" height="200"/>](Res/MEMS01.jpg)

[<img src="Res/MEMS02.jpg" width="" height="200"/>](Res/MEMS02.jpg)

[<img src="Res/MEMS03.jpg" width="" height="200"/>](Res/MEMS03.jpg)

Resources

[pikebishop.github.io]: Pike Bishop in web.

---
![image](Res/Empty.jpg)
See my [About](Res/Empty.jpg) page for details. 

<!-- Resources and links -->
[pikebishop.github.io]:http://pikebishop.github.io/
