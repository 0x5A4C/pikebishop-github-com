---
layout: page
title: "OMP_LedLight"
description: ""
---
{% include JB/setup %}

---

# 6 minutes and 29 seconds design #

Features:

- monitor light intensity and movement,
- turn on/off back light.

Components:

- central unit
- movement sensor
- foto sensor
- load controller
- ui

**Central unit**

Arduino based (any clone is acceptable).

Mataboard

[<img src="Res/Metaboard.jpg"     width="" height="200"/>](Res/Metaboard.jpg "Metaboard") 
[<img src="Res/MetaboardComp.png" width="" height="200"/>](Res/MetaboardComp.png "Metaboard Components") 
[<img src="Res/MetaboardPcb.png"  width="" height="200"/>](Res/MetaboardPcb.png "Metaboard pcb") 
[<img src="Res/MetaboardSch.png"  width="" height="200"/>](Res/MetaboardSch.png "Metaboard sch")

Nanino 

Very nice, one sided board with Arduino compatible pin-out. Based on Arduino bootloader, but requires TTL-USB adapter. [Main project page.][NaninoMain] 


**Movement sensor**

Cheap HC-SR501 sensor.

[<img src="Res/HC-SR501_00.jpg" width="" height="200"/>](Res/HC-SR501_00.jpg "HC-SR501 front") 
[<img src="Res/HC-SR501_01.jpg" width="" height="200"/>](Res/HC-SR501_01.jpg "HC-SR501 back")
[<img src="Res/HC-SR501_02.jpg" width="" height="300"/>](Res/HC-SR501_02.jpg "HC-SR501 pins")


**Photo sensor**

- Photo resistor connected directly to uC port.
- Module with sensor and comparator. 

**Load controller**

- Buz11 or similar connected directly to uC port.
- Relay module.

**Ui**

Led and push button.

# Realization #

**1. Led and push button and movement sensor mounted in.**

[<img src="Res/UI00.JPG" width="" height="100"/>](Res/UI00.JPG "UI")
[<img src="Res/UI01.JPG" width="" height="100"/>](Res/UI01.JPG "UI")


**2. Making two from one**

[<img src="Res/Metaboard00.JPG" width="" height="100"/>](Res/Metaboard00.JPG "Metaboard")
[<img src="Res/Metaboard01.JPG" width="" height="100"/>](Res/Metaboard01.JPG "Metaboard")
[<img src="Res/Metaboard02.JPG" width="" height="100"/>](Res/Metaboard02.JPG "Metaboard")
[<img src="Res/Metaboard03.JPG" width="" height="100"/>](Res/Metaboard03.JPG "Metaboard")

**3. Flashing uC**

**Code** obtained directly from [Usbasploader bootloader][UsbasploaderMain] main page.

Fuse bits (from documentation for [Usbasp programmer][UsbaspMain]) do not work correctly with bootloader - after app programming it's impossible to invoke bootloader again, reset always calls app.

**Fuse bits** - Tested and working values:  

	HFUSE=0xc8  LFUSE=0xef <-----<< for Atmega8/16Mhz this works perfect.
	LOCK = 0xCF

**4. Setting up Arduino IDE**

File *boards.txt* have to be updated by adding following section

	##############################################################
	
	metaboard.name=Metaboard
	
	metaboard.upload.protocol=usbasp
	metaboard.upload.maximum_size=7168
	metaboard.upload.speed=19200
	
	metaboard.build.mcu=atmega8
	metaboard.build.f_cpu=16000000L
	metaboard.build.core=arduino
	metaboard.build.variant=standard
	
	metaboard.upload.disable_flushing=true
	
	##############################################################

Notice changes made in original data mentioned on [Metaboard][MataboardMain]:

- line *metaboard.upload.maximum_size=14336* was changed to *metaboard.upload.maximum_size=14336* (original memory size is for Atmega168, NOT for Atmega8 - first flashing will overwrite bootloader),
- line *metaboard.build.variant=standard* is added (without it compilation will fail because of unknown pin definition).

**5. Test program**

Pressing button will turn on LED. Five line long code on github. 

**6. Flashing**

In Arduino IDE:

- board = *Metaboard*
- programmer = *USBAsp*

In Console:

- result of command lsusb have to list USBAsp programmer as present in system,
- if no programmer is in system Metabord, have to be reseted (power off/on sometimes),
- remember that by default software have no access to USB (sudo Arduino will help but it's brute force technology).

Resolving problems with USB access rights:
	
		source:http://www.bitpim.org/help/
		
		1. Create new rule for udev
		
		    sudo gedit /etc/udev/rules.d/60-objdev.rules
		
		2. Edit new rule in gedit and add following:
		
		       SUBSYSTEM!="usb_device", ACTION!="add", GOTO="objdev_rules_end"
		       # USBasp
		       SYSFS{idVendor}=="16c0", SYSFS{idProduct}=="05dc", GROUP="users", MODE="666"
		       LABEL="objdev_rules_end"
				
		3. Restart udev.
		
		       sudo /etc/init.d/udev restart

**7. Load controller**

Load controller is based on MOSFET transistor:

[<img src="Res/LoadController.png" width="" height="100"/>](Res/LoadController.png "Load controller")

Diode is important when load is in inductive (motor).

**Software**

[<img src="Res/SoftStruct.jpg" width="" height="500"/>](Res/SoftStruct.jpg "Metaboard")

Resources
---

[MataboardMain]: https://metalab.at/wiki/Metaboard "Metaboard main page"

[UsbasploaderMain]: http://www.obdev.at/products/vusb/usbasploader.html "Usbasploader main page."

[UsbasploaderFork]: https://github.com/baerwolf/USBaspLoader "Usbasploader still(?) active fork"

[NaninoMain]: http://vonkonow.com/wordpress/2012/10/nanino-the-diy-friendly-arduino/ "Nanino main page"

[UsbaspMain]: http://www.fischl.de/usbasp/ "Usbasp main page"

---

![image](./Res/1009853h.jpg)

See my [About](./Res/1009853h.jpg) page for details. 

See my [About](./Res/LED21x60.docx) page for details.