---
layout: page
title: "OMP_WheelRobot"
description: ""
---
{% include JB/setup %}

---

#Target
---
##Version A

 * 3 wheels
 * can drive all directions
 * can steer clear of obstacles
 
##Version B

 * actively searches free way

##Version C

 * ir pilot
 * buzzer

##Version D - Optional

 * can speak (Homer Simpson voice)

#Parts
---
##Version A

**Frame**

![image](Res/frame.jpg)

**Motors**

![image](Res/motors.jpg)

**Wheels**

![image](Res/wheels.jpg)

**Motor Driver**

![image](Res/motorDriver.jpg)

**Sensor**

![image](Res/sensor.jpg)

**Control unit**

![image](Res/controlUnit.jpg)

#Realisation
---
##Version A

**Early prototype**

![image](Res/prototypeVA.jpg)

##Version B/C

**Software**
Based on multi tasking library.

Tasks:

 * Motor control
 * Sonar
 * Pilot
 * Buzzer

#Resources
---
![image](Res/1009853h.jpg)
[<img src="Res/edistprofXY.svg" width="700" height=""/>](Res/edistprofXY.svg)

See my [About](Res/1009853h.jpg) page for details. 

See my [About](Res/LED21x60.docx) page for details.