---
layout: page
title: "OMP_8x8LedToy"
description: "64 pixels toy..."
---
{% include JB/setup %}

Links
---
[64pixels]: http://tinkerlog.com/howto/64pixels/
[MultimodeClock]: http://www.simpleavr.com/avr/multimode-clock
[minimatrix]: https://github.com/arachnidlabs/minimatrix/         
[Arduino Game Of Life on 8×8 LED Matrix]: http://brownsofa.org/blog/archives/170
[Laser Command Game]: http://www.arduinopassion.com/laser-command-game/
[8x8 Pixel ROM Font Editor]: http://www.min.at/prinz/o/software/pixelfont/

1 - [64pixels][64pixels] *Starting point for this project. Includes implementation of display driver (without resistors), basic efects (scrolling),  definition of some sprites*

2 - [MultimodeClock][MultimodeClock] *Nice implementation of clock on 8x8 display - not used this time.*
         
3 - [minimatrix][minimatrix] *Beatiful animation on 8x8 display. Python tool for converting animated gif into HEX (Yess!). Storing animation in eeprom. Four ready to use animations (quite long).*
         
4 - [Arduino Game Of Life on 8×8 LED Matrix][Arduino Game Of Life on 8×8 LED Matrix] *Includes code which will be used for implementation of Life Game in this project.*
   
5 - [Laser Command Game][Laser Command Game] *Great(!) idea of interaction with led matrix and laser pointer (Essentially it is an 8×8 matrix LED as a 8×8 light sensor array). Not this time but...*
   
6 - [8x8 Pixel ROM Font Editor][Laser Command Game] *Maybe it will be useful.*
   
Parts
---
64pixels:

 - ATTINY2313V-10PU, microcontroller, 2 k flash RAM, Digikey
 - LEDMS88R, 8 * 8 LED matrix, Futurlec
 - Battery holder with switch for two AA batteries, Digikey
 - 2 AA batteries or rechargeables

MultimodeClock:

 - attiny2313v (v is low power version that works with 3V)
 - 8x8 LED matrix display (red works best on 3V power) 
 - tactile button

Resources
---
Matrix used in [64pixels][64pixels] project - [LEDMS88R](http://www.futurlec.com/LED/LEDMS88R.shtml):

[<img src="Res/led_00.jpg" width="" height="100"/>](Res/led_00.jpg "Led Matrix")
[<img src="Res/led_01.bmp" width="" height="100"/>](Res/led_01.bmp "Led Matrix")
[<img src="Res/led_02.bmp" width="" height="100"/>](Res/led_02.bmp "Led Matrix")

*LED-AM-12088AMR-B PBF* as perfect candidate for this project:

[<img src="Res/am-12088A_00.jpg" width="" height="100"/>](Res/am-12088A_00.jpg "Led Matrix")

From shop
---

[<img src="Res/Shop/IMG_1790.JPG" width="" height="100"/>](Res/Shop/IMG_1790.JPG "Led Matrix")
[<img src="Res/Shop/IMG_1791.JPG" width="" height="100"/>](Res/Shop/IMG_1791.JPG "Led Matrix")
[<img src="Res/Shop/IMG_1793.JPG" width="" height="100"/>](Res/Shop/IMG_1793.JPG "Led Matrix")

Code compilation
---

Starting version of project  is based on [64pixels][64pixels]. First build directly with provided make file.

Finally project moved to eclipse.

Programmer
---

USBAsp + avrdude + linux

command for Atmega16 testing environment:

	sudo avrdude -p m16 -c usbasp -U flash:w:8x8.hex -U eeprom:w:8x8.eep
	sudo is important - it grant access rights for programmer to usb port and can be skiped after following procedure:

		source:http://www.bitpim.org/help/
		
		1. Create new rule for udev
		
		    sudo gedit /etc/udev/rules.d/60-objdev.rules
		
		2. Edit new rule in gedit and add following:
		
		       SUBSYSTEM!="usb_device", ACTION!="add", GOTO="objdev_rules_end"
		       # USBasp
		       SYSFS{idVendor}=="16c0", SYSFS{idProduct}=="05dc", GROUP="users", MODE="666"
		       LABEL="objdev_rules_end"
				
		3. Restart udev.
		
		       sudo /etc/init.d/udev restart


Assembling
---


Testing
---
[<img src="Res/64pPrototype.jpeg" width="" height="100"/>](Res/64pPrototype.jpeg "64pixels")
[<img src="Res/64pPrototype.gif" width="" height="100"/>](Res/64pPrototype.gif "64pixels")

Experiments with code
---

 * Building test/development platform
 
   * gnUSB (Atmega16)
 
   * USBAsp programmer
  
   * raspBerryPi + AvrDude
   
[<img src="Res/GnUsbPrototype.jpeg" width="" height="100"/>](Res/GnUsbPrototype.jpeg "Prototype")
[<img src="Res/GnUsb.jpeg" width="" height="100"/>](Res/GnUsb.jpeg "GnUsb")

   
 * Implementation
 
   * Basic display refresh procedure (for Atmega16)
     * timer initialization
     * port -> display pin configuration (defines)
     * port initialization
     * refreshing display (based on defines
     * every thing should be a litle more flexible - going back to ATTINY have to possible
 
   * Implementation of simple visual effects
     * fill display memory
     * clear display memory
     * display sprite
     * animate sprite
     * roll screen out left/rigt
     * roll sprite in from left/right
     * roll sprite in/sprite out (one sprite is leaving second entering) from left/right - nice effect but consumes LOT of memory
     * animate sprite from eeprom (based on [minimatrix]) - spectacular efect
     * life game (based on [Arduino Game Of Life on 8×8 LED Matrix]) - revriten - used standard function rand - spectacular efect 
     
Finally all demos consumes over 4k of ROM :-(.

Possible scenario (but not implemented) for making code a little/much smaller

 * Re-factoring and small optimization
   * Most of effects have to be removed 
   * Code should be about 1k
   * Rest of memory can be used for storing big animations
   * Small sprites can be stored in EEPROM
     
   * Re-factoring of core of application
   * Selection of effects
     * Sprite aniamtion
     * EEPROM sprite animation 
   * Rebuilding gif conversion python tool, to generate ready to use code.
     * done - small enchacement added - hex values are displayed on console
   * Proof of concept
     * tested implementation
       * small sprite aniamtion (3 - 4 separate sprites stored in eeprom)
       * big sprites (converted gifs) animation (4 sprites stored in code mem.)
       * no roll in/out effects
     * works
       * ~2k of code mem ocupied
   * Testing version with game Life
   
 * Final implementation 
   * cleaning last test version
   * adding new small sprites (almost 30 is possible)
   * preparing ATTINY version
    
 * Assembling
 * Testing

Migrating to bigger platform 
---

It will be easier to move to bigger micro (ATMEGA328). Price is similar, all demos can be included, some can be enhanced.

**Comparing pin mapping for ATMEGA328 and ATTINY3213.**  


	                       +--------U--------+
                           |                 |  
	         (RESET)  PC6  o 1            28 o  PC5  (ADC5 / SCL)
                           |                 |  
	           (RXD)  PD0  o 2     		  27 o  PC4  (ADC4 / SDA)
                           |                 |  
	           (TXD)  PD1  o 3     		  26 o  PC3  (ADC3)  
                           |                 |  
	          (INT0)  PD2  o 4     		  25 o  PC2  (ADC2)  
                           |                 |  
	          (INT1)  PD3  o 5     		  24 o  PC1  (ADC1)  
                           |                 |  
	      (XCK / T0)  PD4  o 6     		  23 o  PC0  (ADC0)  
                           |                 |  
	                  VCC  o 7     		  22 o  GND  
                           |                 |  
	                  GND  o 8     		  21 o  AREF  
                           |                 |  
	 (XTAL1 / TOSC1)  PB6  o 9     		  20 o  AVCC  
                           |                 |  
	 (XTAL2 / TOSC2)  PB7  o 10   		  19 o  PB5  (SCK)  
                           |                 |  
	            (T1)  PD5  o 11    		  18 o  PB4  (MISO)  
                           |                 |    
	          (AIN0)  PD6  o 12    		  17 o  PB3  (MOSI / OC2)  
                           |                 |  
	          (AIN1)  PD7  o 13    		  16 o  PB2  (SS / OC1B)  
                           |                 |  
	          (ICP1)  PB0  o 14    		  15 o  PB1  (OC1A)  
                           |                 |    
	                       +-----------------+  
	
				Atmega8/Atmega328 pinout in ASCII Art.


	                       +--------U--------+
                           |                 |  
	         (RESET)  PA2  o 1            20 o  VCC
                           |                 |  
	           (RXD)  PD0  o 2     		  19 o  PB7
                           |                 |  
	           (TXD)  PD1  o 3     		  18 o  PB6
                           |                 |  
	         (XTAL1)  PA1  o 4     		  17 o  PB5
                           |                 |  
	         (XTAL2)  PA0  o 5     		  16 o  PB4
                           |                 |  
	                  PD2  o 6     		  15 o  PB3
                           |                 |  
	                  PD3  o 7     		  14 o  PB2
                           |                 |  
	                  PD4  o 8     		  13 o  PB1
                           |                 |  
	                  PD5  o 9     		  12 o  PB0
                           |                 |  
	                  GND  o 10   		  11 o  PD6
                           |                 |  
	                       +-----------------+
	
				      ATTiny2313 pinout in ASCII Art.


Pin mapping for migration from ATTINY2313 to ATMEGA328


	|  Chip/Pin |    |    |    |    |    |    |    |    |    |    |  
	|-----------|----|----|----|----|----|----|----|----|----|----|
	|ATTINY3213 |PA2 |PD0 |PD1 |PA1 |PA0 |PD2 |PD3 |PD4 |PD5 |GND |    
	|ATMEGA328  |NA  |PD1 |PD2 |PD3 |PD4 |PB7 |PD5 |PD6 |PD7 |NA  |  
	|ATTINY3213 |VCC |PB7 |PB6 |PB5 |PB4 |PB3 |PB2 |PB1 |PB0 |PD6 |  
	|ATMEGA328  |NA  |PC3 |PC2 |PC1 |PC0 |PB5 |PB4 |PB3 |PB2 |PD6 |  

**Changing matrix to bigger size**

Old matrix is too small to hide/mask bigger uC, so new one is selected. Old ones will be used in other project. 

[<img src="Res/big8x8_00.jpeg" width="" height="100"/>](Res/big8x8_00.jpeg "Bigger compared to smaller/older.")
[<img src="Res/big8x8_01.jpeg" width="" height="100"/>](Res/big8x8_01.jpeg "Matrix with uC.")


[<img src="Res/Demos/00.gif" width="" height="100"/>](Res/Demos/00.gif "Demo")
[<img src="Res/Demos/01.gif" width="" height="100"/>](Res/Demos/01.gif "Demo")
[<img src="Res/Demos/02.gif" width="" height="100"/>](Res/Demos/02.gif "Demo")
[<img src="Res/Demos/03.gif" width="" height="100"/>](Res/Demos/03.gif "Demo")
[<img src="Res/Demos/04.gif" width="" height="100"/>](Res/Demos/04.gif "Demo")
[<img src="Res/Demos/05.gif" width="" height="100"/>](Res/Demos/05.gif "Demo")
[<img src="Res/Demos/06.gif" width="" height="100"/>](Res/Demos/06.gif "Demo")
[<img src="Res/Demos/07.gif" width="" height="100"/>](Res/Demos/07.gif "Demo")

**AvrDude and Arduino**

    avrdude -carduino -pm328p -P /dev/ttyUSB0 -b57600 -Uflash:w:8x8.hex 
    
Real live example

    ----@ropuch:~/_Projects/_Electronic/OMP_8x8LedToy/8x8/Release$ avrdude -carduino -pm328p -P /dev/ttyUSB0 -b57600 -Uflash:w:8x8.hex 
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: Device signature = 0x1e950f
    avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "8x8.hex"
    avrdude: input file 8x8.hex auto detected as Intel Hex
    avrdude: writing flash (11414 bytes):
    
    Writing | ################################################## | 100% 3.14s
    
    avrdude: 11414 bytes of flash written
    avrdude: verifying flash memory against 8x8.hex:
    avrdude: load data flash data from input file 8x8.hex:
    avrdude: input file 8x8.hex auto detected as Intel Hex
    avrdude: input file 8x8.hex contains 11414 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 2.33s
    
    avrdude: verifying ...
    avrdude: 11414 bytes of flash verified
    
    avrdude: safemode: Fuses OK (H:00, E:00, L:00)
    
    avrdude done.  Thank you.
    












































    


