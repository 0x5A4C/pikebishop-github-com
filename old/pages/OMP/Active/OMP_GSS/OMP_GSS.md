---
layout: page
title: "OMP_GSS - Główny Szlak Sudecki im. Mieczysława Orłowicza"
description: "Główny Szlak Sudecki im. Mieczysława Orłowicza - samotne, ciągłe pokonanie najdłuższego sudeckiego szlaku."
---
{% include JB/setup %}

# Resources

[<img src="Res/zGarbaczewski_GSS.jpg" height="300px" />](Res/zGarbaczewski_GSS.jpg "Główny Szlak Sudecki im. Mieczysława Orłowicza")

**Zbigniew Garbaczewski** *Główny Szlak Sudecki im. Mieczysława Orłowicza*

Prawdopodobnie jedyna publikacja książkowa na temat GSS.
Zawartość w znacznej części nieaktulana, ale zawiera kilka bardzo użetycznych informacji, np. poniższą tabele.

[<img src="Res/Har00.jpg "  height="300"/>](Res/Har00.jpg "Główny Szlak Sudecki - Tabela przejścia")
[<img src="Res/Har01.jpg "  height="300"/>](Res/Har01.jpg "Główny Szlak Sudecki - Tabela przejścia")

# Etaps

**GSS Complete** 
	
	27.10.2013 - 05.11.2013
	Świeradów Zdrój - Paczków

		WhereWasI Output
		=================

	Filename=Gss_Total.gpx
	Number of Track Segments	= 1
	---------------------------------------------
	Start Segment / Point 	= 0/0
	End Segment / Point 	= 0/40511
	Start Time            	= 27/10/2013 09:03:03
	End Time              	= 05/11/2013 13:31:56
	Number of Track Points	= 40512
	---------------------------------------------
	Total Time            	= 220:28
	Total Distance        	= 393.39 km
	Total Climb           	= 19742 m
	---------------------------------------------
	Average Speed         	=  1.78 km/hr
	Maximum Speed         	= 176.77 km/hr
	---------------------------------------------


**Etap 0** 

	27.10.2013
	Świeradów Zdrój - Stóg Izerski
	Nocleg: Schronisko na stogu Izerskim

		WhereWasI Output
		=================

	Filename=Gss_D0.gpx
	Number of Track Segments	= 1
	---------------------------------------------
	Start Segment / Point 	= 0/0
	End Segment / Point 	= 0/833
	Start Time            	= 27/10/2013 09:03:03
	End Time              	= 27/10/2013 10:55:39
	Number of Track Points	= 834
	---------------------------------------------
	Total Time            	= 01:52
	Total Distance        	=  6.99 km
	Total Climb           	=   697 m
	---------------------------------------------
	Average Speed         	=  3.72 km/hr
	Maximum Speed         	=  9.75 km/hr
	---------------------------------------------


**Etap 1** 
	
	28.10.2013
	Stóg Izerski - Równia pod Śnieżką 
	Nocleg: Dom Ślaski

		WhereWasI Output
		=================

	Filename=Gss_D1.gpx
	Number of Track Segments	= 1
	---------------------------------------------
	Start Segment / Point 	= 0/0
	End Segment / Point 	= 0/5667
	Start Time            	= 28/10/2013 04:05:21
	End Time              	= 28/10/2013 16:00:28
	Number of Track Points	= 5668
	---------------------------------------------
	Total Time            	= 11:55
	Total Distance        	= 45.44 km
	Total Climb           	=  4693 m
	---------------------------------------------
	Average Speed         	=  3.81 km/hr
	Maximum Speed         	= 26.46 km/hr
	---------------------------------------------


**Etap 2** 

	29.10.2013
	Równia pod Śnieżką - Paprotki
	Nocleg: Stanica pod Zadzierną

		WhereWasI Output
		=================

	Filename=Gss_D2.gpx
	Number of Track Segments	= 1
	---------------------------------------------
	Start Segment / Point 	= 0/0
	End Segment / Point 	= 0/4638
	Start Time            	= 29/10/2013 04:00:32
	End Time              	= 29/10/2013 16:59:30
	Number of Track Points	= 4639
	---------------------------------------------
	Total Time            	= 12:58
	Total Distance        	= 47.52 km
	Total Climb           	=  1753 m
	---------------------------------------------
	Average Speed         	=  3.66 km/hr
	Maximum Speed         	= 18.58 km/hr
	---------------------------------------------


**Etap 3** 

	30.10.2013
	? - Schronisko Andrzejówka
	Nocleg: Schronisko Andrzejówka

		WhereWasI Output
		=================

	Filename=Gss_D3.gpx
	Number of Track Segments	= 1
	---------------------------------------------
	Start Segment / Point 	= 0/0
	End Segment / Point 	= 0/4157
	Start Time            	= 30/10/2013 03:46:21
	End Time              	= 30/10/2013 15:45:00
	Number of Track Points	= 4158
	---------------------------------------------
	Total Time            	= 11:58
	Total Distance        	= 41.84 km
	Total Climb           	=  1946 m
	---------------------------------------------
	Average Speed         	=  3.49 km/hr
	Maximum Speed         	= 70.52 km/hr
	---------------------------------------------


**Etap 4** 
	
	31.10.2013
	Schronisko Andrzejówka - Srebrna Góra
	Nocleg: Pod srebrnogórską chmurką

		WhereWasI Output
		=================

	Filename=Gss_D4.gpx
	Number of Track Segments	= 1
	---------------------------------------------
	Start Segment / Point 	= 0/0
	End Segment / Point 	= 0/5236
	Start Time            	= 31/10/2013 04:05:50
	End Time              	= 31/10/2013 18:30:11
	Number of Track Points	= 5237
	---------------------------------------------
	Total Time            	= 14:24
	Total Distance        	= 45.70 km
	Total Climb           	=  2260 m
	---------------------------------------------
	Average Speed         	=  3.17 km/hr
	Maximum Speed         	= 13.07 km/hr
	---------------------------------------------


**Etap 5** 
	
	01.11.2013
	Srebrna Góra - Szczeliniec Wielki
	Nocleg: Schronisko na Szczelińcu Wielkim

		WhereWasI Output
		=================

	Filename=Gss_D5.gpx
	Number of Track Segments	= 1
	---------------------------------------------
	Start Segment / Point 	= 0/0
	End Segment / Point 	= 0/4453
	Start Time            	= 01/11/2013 04:06:33
	End Time              	= 01/11/2013 17:24:18
	Number of Track Points	= 4454
	---------------------------------------------
	Total Time            	= 13:17
	Total Distance        	= 46.38 km
	Total Climb           	=  1785 m
	---------------------------------------------
	Average Speed         	=  3.49 km/hr
	Maximum Speed         	= 51.05 km/hr
	---------------------------------------------

**Etap 6** 

	02.11.2013
	Szczeliniec Wielki - Zieleniec
	Nocleg: Resort/Spa

		WhereWasI Output
		=================

	Filename=Gss_D6.gpx
	Number of Track Segments	= 1
	---------------------------------------------
	Start Segment / Point 	= 0/0
	End Segment / Point 	= 0/4552
	Start Time            	= 02/11/2013 04:52:03
	End Time              	= 02/11/2013 17:40:43
	Number of Track Points	= 4553
	---------------------------------------------
	Total Time            	= 12:48
	Total Distance        	= 47.26 km
	Total Climb           	=  1926 m
	---------------------------------------------
	Average Speed         	=  3.69 km/hr
	Maximum Speed         	= 176.77 km/hr
	---------------------------------------------


**Etap 7** 

	03.11.2013
	Zieleniec - Hala Pod Śnieżnikiem
	Nocleg: Schronisko na hali pod Śnieżnikiem

		WhereWasI Output
		=================

	Filename=Gss_D7.gpx
	Number of Track Segments	= 1
	---------------------------------------------
	Start Segment / Point 	= 0/0
	End Segment / Point 	= 0/4939
	Start Time            	= 03/11/2013 05:23:50
	End Time              	= 03/11/2013 20:01:43
	Number of Track Points	= 4940
	---------------------------------------------
	Total Time            	= 14:37
	Total Distance        	= 52.17 km
	Total Climb           	=  2002 m
	---------------------------------------------
	Average Speed         	=  3.57 km/hr
	Maximum Speed         	= 17.09 km/hr
	---------------------------------------------


**Etap 8** 
	
	04.11.2013
	Hala Pod Śnieżnikiem - Lądek Zdrój
	Nocleg: hotelik...

		WhereWasI Output
		=================

	Filename=Gss_D8.gpx
	Number of Track Segments	= 1
	---------------------------------------------
	Start Segment / Point 	= 0/0
	End Segment / Point 	= 0/2550
	Start Time            	= 04/11/2013 09:27:21
	End Time              	= 04/11/2013 14:23:11
	Number of Track Points	= 2551
	---------------------------------------------
	Total Time            	= 04:55
	Total Distance        	= 22.53 km
	Total Climb           	=  1323 m
	---------------------------------------------
	Average Speed         	=  4.57 km/hr
	Maximum Speed         	= 10.72 km/hr
	---------------------------------------------


**Etap 9** 

		05.11.2013
		Ladek Zdrój - Paczków Stacja PKP
		The End.

		WhereWasI Output
		=================

	Filename=Gss_D9.gpx
	Number of Track Segments	= 1
	---------------------------------------------
	Start Segment / Point 	= 0/0
	End Segment / Point 	= 0/3415
	Start Time            	= 05/11/2013 04:14:50
	End Time              	= 05/11/2013 13:31:56
	Number of Track Points	= 3416
	---------------------------------------------
	Total Time            	= 09:17
	Total Distance        	= 36.13 km
	Total Climb           	=  1061 m
	---------------------------------------------
	Average Speed         	=  3.89 km/hr
	Maximum Speed         	= 69.85 km/hr
	---------------------------------------------

# Epilog

	Czas ołowiu

	(...)
	Cienki lód
	Kruche szkło
	Janis J., Brian Jones
	Jimi H., Anna J.
	Kruchy lód
	Cienkie szkło
	Steve McQueen, Romy S.
	(Johnnie L./Ryszard R.), Elvis P.
	Martwy film
	Martwy blues
	Kruche szkło
	Cienki lód
	Czas ołowiu. 
	
	by Marek Dutkiewicz

---
asdasdasd



---
asdasdasd




