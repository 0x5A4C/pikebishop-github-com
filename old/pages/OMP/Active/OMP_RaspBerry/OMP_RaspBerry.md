---
layout: page
title: "OMP_RaspBerry"
description: ""
---
{% include JB/setup %}

---

#RaspBian

From <http://elinux.org/RPi_Easy_SD_Card_Setup>


1 - Download from <http://www.raspberrypi.org/downloads>

2 - Verify the the hash key 
   
	sha1sum ~/2012-12-16-wheezy-raspbian.zip

3 - Extract the image, with

	unzip ~/2012-12-16-wheezy-raspbian.zip
   
4 - Write the image to the card 

	dd bs=4M if=~/2012-12-16-wheezy-raspbian.img of=/dev/sdd - consider bs=4M

   Please note that block size set to 4M will work most of the time, if not, please try 1M, although 1M will take considerably longer.

#Asus USB N-10

###Resources

1 <http://rasspberrypi.wordpress.com/2012/09/02/wifi-on-rasspberry-pi-with-asus-usb-n10/>
	
*Simple variant - checked - works.*
	
2 <http://databoyz.wordpress.com/tag/how-to-setup-asus-usb-n10-wifi-adapter-on-raspberry-pi/>

*More complicated - not checked.*

###Simple

	cd /etc/network
	sudo cp interface interfaces.backup
	ls -l
	sudo nano interfaces
	
	allow-hotplug wlan0
	auto wlan0
	iface wlan0 inet dhcp
	wpa-ssid "your network ssid"
	wpa-psk "network passwd"

Some example...

![image](./Res/RpiASUSUsbN10.png)

#OwnCloud

###Resources

1 <http://www.owncloudbook.com/owncloud-on-raspberry-pi/http://www.owncloudbook.com/owncloud-on-raspberry-pi/>

2 <http://sourceforge.net/projects/cloudstorage12/files/Raspberry%20PI/>

3 <https://github.com/petrockblog/OwncloudPie>
  
  <http://blog.petrockblock.com/2012/08/15/your-own-cloud-server-with-owncloud-on-the-raspberry-pi/>
  
4 <http://www.techjawab.com/2013/09/how-to-setup-your-own-cloud-on.html>

###Instalation based on 1

1. Download <http://sourceforge.net/projects/owncloud-raspberrypi/files/owncloud-raspberrypi-0.2.img.gz/download>


2. Unzip *gunzip owncloud-raspberrypi-0.1.img.gz* 

3. sudo *dd bs=1M if=owncloud-raspberrypi-0.1.img of=/dev/sde*

   Consider:
   
     - bs=1M
     
     - /dev/sde not /dev/sde1 (!)

4. Boot RaspBerry

	user: pi password: owncloud

5. Run *raspi-config* to expand the filesystem - this is very important

6. Check at *http://ip*

7. Have fun

####After tests

Slow. Toooooo slow :-(

#Cozycloud

1 <https://www.cozycloud.cc/>

2 <http://n0where.net/cozy-v1-0-snowden-release/>

#Arkos

[1]: http://n0where.net/arkos/

[2]: https://arkos.io/

#HoneyPot

#WIFI HoneyPot

#Tor

#WebCammera
Best starting point is motion.

Motion expect configuration from external file. After instalation default config file is generated in **/etc/motion**.
Sources/locations of config file (in order from first to last):
- command line (-c option),
- current dir,
- **~/.motion** dir,
- **/etc/motion** dir.
Name of file stored in one of possible locations is **motion.conf**.

#Sdr

[Source]: http://www.satsignal.eu/raspberry-pi/dump1090.html

start:
	git clone git://git.osmocom.org/rtl-sdr.git
	cd rtl-sdr
	mkdir build
	cd build
	cmake ../ -DINSTALL_UDEV_RULES=ON
	make
	sudo make install
	sudo ldconfig



	sudo cp ./rtl-sdr/rtl-sdr.rules /etc/udev/rules.d/
	sudo reboot

	rtl_test -t 

	Found 1 device(s):
	  0:  Realtek, RTL2838UHIDIR, SN: 00000001

	Using device 0: Generic RTL2832U OEM

	Kernel driver is active, or device is claimed by second instance of librtlsdr.
	In the first case, please either detach or blacklist the kernel module
	(dvb_usb_rtl28xxu), or enable automatic detaching at compile time.

	usb_claim_interface error -6
	Failed to open rtlsdr device #0.

Beacause of 

	Kernel driver is active, or device is claimed by second instance of librtlsdr.
	In the first case, please either detach or blacklist the kernel module
	(dvb_usb_rtl28xxu), or enable automatic detaching at compile time.

	usb_claim_interface error -6
	Failed to open rtlsdr device #0.

folowing steps have to be performed:
	
	cd /etc/modprobe.d
	sudo nano no-rtl.conf

Add the following lines to the new file:

	blacklist dvb_usb_rtl28xxu
	blacklist rtl2832
	blacklist rtl2830

Again:
	rtl_test -t


	Found 1 device(s):
	  0:  Realtek, RTL2838UHIDIR, SN: 00000001

	Using device 0: Generic RTL2832U OEM
	Found Rafael Micro R820T tuner
	Supported gain values (29): 0.0 0.9 1.4 2.7 3.7 7.7 8.7 12.5 14.4 15.7 16.6 19.7 20.7 22.9 25.4 28.0 29.7 32.8 33.8 36.4 37.2 38.6 40.2 42.1 43.4 43.9 44.5 48.0 49.6 
	Sampling at 2048000 S/s.
	No E4000 tuner found, aborting.


	git clone git://github.com/MalcolmRobb/dump1090.git
	cd dump1090
	make
	[djt] I got errors at this point where pkg-config could not be found on my minimal Linux.  Installing pkg-config cured this problem, but if you had no errors from the make step you will not need not need to do steps 5 and 6.
	sudo apt-get install pkg-config
	make

Finally:
	./dump1090 --interactive --net

#Internet Radio

[Not tested yet][MataboardMain]

[MataboardMain]: http://www.instructables.com/id/Arduino-Raspberry-Pi-Internet-Radio/?ALLSTEPS "RaspBerryPi based internet radio."

#Home Automation
[BeakPi][BeakPi Home Automation]
[BeakPi Home Automation]: https://github.com/beakable/BeakPi

#Car
[PI CarPC][Raspberry PI CarPC]
[Raspberry PI CarPC]: http://www.engineering-diy.blogspot.ro/2014/04/raspberry-pi-carpc-april-2014-updates.html

#Music

[Pi MusicBox][Raspberry Pi MusicBox]
[Raspberry Pi MusicBox]: http://www.pimusicbox.com/

#3G modem, GPS, and Google maps (both USB)
---
![image](./Res/1009853h.jpg)

See my [About](./Res/1009853h.jpg) page for details. 

See my [About](./Res/LED21x60.docx) page for details.
