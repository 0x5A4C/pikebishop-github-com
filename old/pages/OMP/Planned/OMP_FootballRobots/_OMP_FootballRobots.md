---
layout: page
title: "OMP - Football Robots"
description: ""
---
{% include JB/setup %}

---
##OMP - Football Robots##

Small, remote controlled robots, allowing playing football.   

[Source](http://www.moop.org.uk/index.php/2015/03/15/foobot) of idea

[Harware strucuture](Res/hwStrucuture.png)

[<img src="Res/hwStrucuture.png" width="" height="100"/>](Res/hwStrucuture.png "Harware Strucuture")

**Main modules**

- **Arduino or Nucleo**
- Manipulator
- NRF24
- PSupply
- Charger
- SmallLiPoBattery
- **Arduino Nano or Nucleo**
- NRF24
- Motors
- HBridge
- PSupplyL
- BigLiPoBattery

Resources

[pikebishop.github.io]: Pike Bishop in web.

---
[<img src="Res/Empty.jpg" width="" height="100"/>](Res/Empty.jpg "Demo")
![image](Res/Empty.jpg)

See my [About](Res/Empty.jpg) page for details. 

<!-- Resources and links -->
[pikebishop.github.io]:http://pikebishop.github.io/
