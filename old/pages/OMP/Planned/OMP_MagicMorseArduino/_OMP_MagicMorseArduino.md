---
layout: page
title: "OMP_MagicMorseArduino"
description: ""
---
{% include JB/setup %}

---
Resources
---

[magic-morse-on-arduino]:http://www.hackster.io/rayburne/magic-morse-on-arduino
[Notes-behind-Magic-Morse]:http://www.picaxeforum.co.uk/entry.php?30-Notes-behind-Magic-Morse
[morse-sticks-key]:http://www.hackster.io/rayburne/morse-sticks-key
