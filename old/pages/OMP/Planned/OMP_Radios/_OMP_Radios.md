---
layout: page
title: "OMP_Radios"
description: ""
---
{% include JB/setup %}

---

###Arduino

##Resources
[an example](http://www.doctormonk.com/2012/03/tea5767-fm-radio-breakout-board-for.html "Title")
[an example](http://www.instructables.com/id/TEA5767-FM-Radio-Breakout-Board-for-Arduino  "Title")

##Elements:

      * Consider using parts of old mp3 players and radios
      * Arduino
         * classic
         * uno
      * Leaf module (optional)
         * mini28777
         * stm32 breakBoard
      * Radio module
         * TEA5767 module
         * module with RDS
      * Display 
         * Nokia 5110 LCD
         * LCD 2x16
         * Shield Arduino 16*2 LCD + Keyboard
         * ...
      * KeyPad
         * keyPad
         * rottary switch (nice!)
         * Shield Arduino 16*2 LCD + Keyboard
      * Amplifier
         * headphone
         * or other
            * TDA7384A - Elektronika dla wszystkich 6 2009, 7 2009
      * PreAmplifier (optional)
         * TDA8425 - Nowy Elektronik 4/5.2010 
         * TDA7317 - Elektronika dla wszystkich 6 2009, 7 2009
      * iR control (optional)

##TEA5767 module

TEA5767 is a radio chip, a lot of mobile phones, MP3, MP4 to the radio functions are implemented in his.

   1. High sensitivity, low noise high frequency amplifier,
   2. Radio Frequency: 87.6MHz ~ 108MHz, (supporting the frequency range of 76MHz ~ 87.5MHz between the campus radio channels),
   3. LC tuned oscillator to lower cost, RF AGC circuit
   4. Built-in FM frequency selection, I2C bus control
   5. Built-in FM stereo demodulator, PLL synthesizer tuner decoder
   6. Two programmable ports, soft mute, SNC (stereo noise reduction)
   7. Adaptive stereo decoding, automatic search function
   8. Wait mode, requires a 7.6MHz crystal
   9. 40-pin LQFP package 

![image](./Res/TEA5767/TEA5767.jpg)

Resources
---
![image](./Res/1009853h.jpg)

See my [About](./Res/1009853h.jpg) page for details. 

See my [About](./Res/LED21x60.docx) page for details.

