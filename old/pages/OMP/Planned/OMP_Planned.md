---
layout: page
title: "OMP_Planned"
description: ""
---
{% include JB/setup %}

---

#OMP_AnalaogLogger
Elektronika Praktyczna 04 2009 - Wielokanałowy rejestrator napięć

#OMP_ArduinoCam
#OMP_AudioSystem
[<img src="Res/Concept_001.png" width="" height="100"/>](Res/Concept_001.png "Concept")

#OMP_BigPowerSupply
#OMP_BigYellowDisp
#OMP_CarCameras
#OMP_Charger
#OMP_DigitalyControlledPowerSup
Resources

Elektronika Praktyczna 06 2009 - str. 60

[Digitally Controlled Dual Tracking Power Supply]

[Digitally Controlled Dual Tracking Power Supply - evernote]

<!-- Resources and links -->
[Digitally Controlled Dual Tracking Power Supply]:http://www.kerrywong.com/2013/11/24/a-digitally-controlled-dual-tracking-power-supply-i/
[Digitally Controlled Dual Tracking Power Supply - evernote]:https://www.evernote.com/shard/s3/sh/7cbfed14-8fde-4c50-b24b-7b19cc848d9f/ee2195f3b4408d6866b918c86dd8db1e

#OMP_DisplaysReview
#Target#
**Review of all displays.**

- Led
- Lcd
- Vfd

#Pictures#
[<img src="OMP_DisplaysReview/Res/17190001.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17190001.JPG "")
[<img src="OMP_DisplaysReview/Res/17190002.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17190002.JPG "")
[<img src="OMP_DisplaysReview/Res/17200003.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17200003.JPG "")
[<img src="OMP_DisplaysReview/Res/17200004.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17200004.JPG "")
[<img src="OMP_DisplaysReview/Res/17200005.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17200005.JPG "")
[<img src="OMP_DisplaysReview/Res/17210006.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17210006.JPG "")
[<img src="OMP_DisplaysReview/Res/17210007.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17210007.JPG "")
[<img src="OMP_DisplaysReview/Res/17220008.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17220008.JPG "")
[<img src="OMP_DisplaysReview/Res/17220009.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17220009.JPG "")
[<img src="OMP_DisplaysReview/Res/17220010.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17220010.JPG "")
[<img src="OMP_DisplaysReview/Res/17230011.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17230011.JPG "")
[<img src="OMP_DisplaysReview/Res/17230012.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17230012.JPG "")
[<img src="OMP_DisplaysReview/Res/17230013.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17230013.JPG "")
[<img src="OMP_DisplaysReview/Res/17230014.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17230014.JPG "")
[<img src="OMP_DisplaysReview/Res/17240015.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17240015.JPG "")
[<img src="OMP_DisplaysReview/Res/17240016.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17240016.JPG "")
[<img src="OMP_DisplaysReview/Res/17240017.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17240017.JPG "")
[<img src="OMP_DisplaysReview/Res/17240018.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17240018.JPG "")
[<img src="OMP_DisplaysReview/Res/17250019.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17250019.JPG "")
[<img src="OMP_DisplaysReview/Res/17250020.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17250020.JPG "")
[<img src="OMP_DisplaysReview/Res/17250021.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17250021.JPG "")
[<img src="OMP_DisplaysReview/Res/17250022.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17250022.JPG "")
[<img src="OMP_DisplaysReview/Res/17260023.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17260023.JPG "")
[<img src="OMP_DisplaysReview/Res/17260024.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17260024.JPG "")
[<img src="OMP_DisplaysReview/Res/17260025.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17260025.JPG "")
[<img src="OMP_DisplaysReview/Res/17260026.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17260026.JPG "")
[<img src="OMP_DisplaysReview/Res/17270027.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17270027.JPG "")
[<img src="OMP_DisplaysReview/Res/17270028.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17270028.JPG "")
[<img src="OMP_DisplaysReview/Res/17270029.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17270029.JPG "")
[<img src="OMP_DisplaysReview/Res/17280030.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17280030.JPG "")
[<img src="OMP_DisplaysReview/Res/17280031.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17280031.JPG "")
[<img src="OMP_DisplaysReview/Res/17290032.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17290032.JPG "")
[<img src="OMP_DisplaysReview/Res/17290033.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17290033.JPG "")
[<img src="OMP_DisplaysReview/Res/17290034.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17290034.JPG "")
[<img src="OMP_DisplaysReview/Res/17300035.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17300035.JPG "")
[<img src="OMP_DisplaysReview/Res/17300036.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17300036.JPG "")
[<img src="OMP_DisplaysReview/Res/17340037.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17340037.JPG "")
[<img src="OMP_DisplaysReview/Res/17340038.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17340038.JPG "")
[<img src="OMP_DisplaysReview/Res/17340039.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17340039.JPG "")
[<img src="OMP_DisplaysReview/Res/17340040.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17340040.JPG "")
[<img src="OMP_DisplaysReview/Res/17340041.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17340041.JPG "")
[<img src="OMP_DisplaysReview/Res/17340042.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17340042.JPG "")
[<img src="OMP_DisplaysReview/Res/17350043.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17350043.JPG "")
[<img src="OMP_DisplaysReview/Res/17350044.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17350044.JPG "")
[<img src="OMP_DisplaysReview/Res/17350045.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17350045.JPG "")
[<img src="OMP_DisplaysReview/Res/17350046.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17350046.JPG "")
[<img src="OMP_DisplaysReview/Res/17350047.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17350047.JPG "")
[<img src="OMP_DisplaysReview/Res/17360048.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17360048.JPG "")
[<img src="OMP_DisplaysReview/Res/17360049.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17360049.JPG "")
[<img src="OMP_DisplaysReview/Res/17360050.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17360050.JPG "")
[<img src="OMP_DisplaysReview/Res/17360051.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17360051.JPG "")
[<img src="OMP_DisplaysReview/Res/17360052.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17360052.JPG "")
[<img src="OMP_DisplaysReview/Res/17370053.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17370053.JPG "")
[<img src="OMP_DisplaysReview/Res/17370054.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17370054.JPG "")
[<img src="OMP_DisplaysReview/Res/17370055.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17370055.JPG "")
[<img src="OMP_DisplaysReview/Res/17370056.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17370056.JPG "")
[<img src="OMP_DisplaysReview/Res/17380057.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17380057.JPG "")
[<img src="OMP_DisplaysReview/Res/17380058.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17380058.JPG "")
[<img src="OMP_DisplaysReview/Res/17380059.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17380059.JPG "")
[<img src="OMP_DisplaysReview/Res/17380060.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17380060.JPG "")
[<img src="OMP_DisplaysReview/Res/17380061.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17380061.JPG "")
[<img src="OMP_DisplaysReview/Res/17380062.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17380062.JPG "")
[<img src="OMP_DisplaysReview/Res/17400063.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17400063.JPG "")
[<img src="OMP_DisplaysReview/Res/17400064.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17400064.JPG "")
[<img src="OMP_DisplaysReview/Res/17400065.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17400065.JPG "")
[<img src="OMP_DisplaysReview/Res/17420066.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17420066.JPG "")
[<img src="OMP_DisplaysReview/Res/17420067.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17420067.JPG "")
[<img src="OMP_DisplaysReview/Res/17420068.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17420068.JPG "")
[<img src="OMP_DisplaysReview/Res/17420069.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17420069.JPG "")
[<img src="OMP_DisplaysReview/Res/17420070.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17420070.JPG "")
[<img src="OMP_DisplaysReview/Res/17430071.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17430071.JPG "")
[<img src="OMP_DisplaysReview/Res/17430072.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17430072.JPG "")
[<img src="OMP_DisplaysReview/Res/17430073.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17430073.JPG "")
[<img src="OMP_DisplaysReview/Res/17440074.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17440074.JPG "")
[<img src="OMP_DisplaysReview/Res/17440075.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17440075.JPG "")
[<img src="OMP_DisplaysReview/Res/17440076.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17440076.JPG "")
[<img src="OMP_DisplaysReview/Res/17440077.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17440077.JPG "")
[<img src="OMP_DisplaysReview/Res/17450078.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17450078.JPG "")
[<img src="OMP_DisplaysReview/Res/17450079.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17450079.JPG "")
[<img src="OMP_DisplaysReview/Res/17450080.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17450080.JPG "")
[<img src="OMP_DisplaysReview/Res/17450081.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17450081.JPG "")
[<img src="OMP_DisplaysReview/Res/17460082.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17460082.JPG "")
[<img src="OMP_DisplaysReview/Res/17460083.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17460083.JPG "")
[<img src="OMP_DisplaysReview/Res/17460084.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17460084.JPG "")
[<img src="OMP_DisplaysReview/Res/17460085.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17460085.JPG "")
[<img src="OMP_DisplaysReview/Res/17460086.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17460086.JPG "")
[<img src="OMP_DisplaysReview/Res/17460087.JPG" width="" height="300"/>](OMP_DisplaysReview/Res/17460087.JPG "")

#OMP_FanController
#OMP_FootballRobots

Small, remote controlled robots, allowing playing football.

[Source](http://www.moop.org.uk/index.php/2015/03/15/foobot) of idea

[Harware strucuture](OMP_DisplaysReview/Res/hwStrucuture.png)

[<img src="OMP_DisplaysReview/Res/hwStrucuture.png" width="" height="100"/>](OMP_DisplaysReview/Res/hwStrucuture.png "Harware Strucuture")

**Main modules**

- **Arduino or Nucleo**
- Manipulator
- NRF24
- PSupply
- Charger
- SmallLiPoBattery
- **Arduino Nano or Nucleo**
- NRF24
- Motors
- HBridge
- PSupplyL
- BigLiPoBattery

#OMP_FriendlyArm

**Must have to check!**
RockBox port to Mini2440 - everything can be found here: [Friendlyarm - RockBox].

<!-- Resources -->
[Friendlyarm - Main Page]:http://www.friendlyarm.net/
[Friendlyarm - http://code.google.com]: http://code.google.com/p/friendlyarm/
[Friendlyarm - http://code.google.com - Andriod]:http://code.google.com/p/friendlyarm/wiki/Android

[Friendlyarm - RockBox]:http://www.rockbox.org/wiki/Mini2440Port
#OMP_FuncGenerators
PE 02-99

#OMP_GeneratorAD5850
#OMP_GPS_GSM_Logger
#OMP_KnobUsb
http://runawaybrainz.blogspot.co.uk/2013/11/adafruit-trinket-compatible-volume-usb.html

#OMP_LargeCurrentsMeter
#OMP_LedClock
#OMP_LedCube
#OMP_LM386Amp
#OMP_Load
#OMP_MagicMorseArduin
[magic-morse-on-arduino]:http://www.hackster.io/rayburne/magic-morse-on-arduino
[Notes-behind-Magic-Morse]:http://www.picaxeforum.co.uk/entry.php?30-Notes-behind-Magic-Morse
[morse-sticks-key]:http://www.hackster.io/rayburne/morse-sticks-key

#OMP_MapActiveWallpaper
#OMP_MiniPow
#OMP_Mp3Player
#OMP_PhotoDuino
#OMP_Plane
#OMP_planned
#OMP_Poduszkowiec
#OMP_PovLine
#OMP_PovRotating
#OMP_PowerSupMultimeter
AVT5333 B MULTIMETR PANELOWY

AVT5383 B MIERNIK TABLICOWY UIPt

AVT5399 B DWUKANAŁOWY MULTIMETR PANELOWY

AVTMOD16 MULTIMETR PANELOWY

http://elfly.pl/  MULTIMETR (wersja z ATMega8 w obudowie DIP28)

http://mirley.firlej.org/miernik_panelowy_zasilacza_symetrycznego

http://www.elektroda.pl/rtvforum/viewtopic.php?p=11179678#11179678

http://mdiy.pl/miniaturowy-miernik-do-zasilacza-na-attiny13
#OMP_PresserSenesor
#OMP_PythonSimpleGame
1. Implement simple Pong game
    1. get sources from [Trevor Appleton - Writing Pong using Python and Pygame] and run it,
	2. start with PyGame,
	3. have fun
2. Implement simple Tank Battle game
	1. prepare OO project structure
	2. prepare version for 2 players playing on one computer
	3. have fun
3. Optionally enhance simple Tank Battle game
	1. playing across network
	2. add map/radar
	3. ability to change weapons
	4. add support for cheap sony ps manipulators (usb version is easier)
	3. ...
	4. have fun

Resources

[Trevor Appleton - Writing Pong using Python and Pygame] - some tutorial showing implementation of Pong game.


<!-- Resources and links -->
[Trevor Appleton - Writing Pong using Python and Pygame]: http://trevorappleton.blogspot.com/2014/04/writing-pong-using-python-and-pygame.html
[Presentation about game phisics with openGL and Python (demos avaliable)]: https://www.youtube.com/watch?v=G0jWMoOEFxc
[Presentation about game phisics with openGL and Python - same as above, but pure slides not movie]: http://www.slideshare.net/DanielPope2/programming-physics-games-with-python-and-opengl


#OMP_PythonUIOscilloscope
#OMP_Radios
##Resources
[example](http://www.doctormonk.com/2012/03/tea5767-fm-radio-breakout-board-for.html "Title")

[example](http://www.instructables.com/id/TEA5767-FM-Radio-Breakout-Board-for-Arduino  "Title")

##Elements:

      * Consider using parts of old mp3 players and radios
      * Arduino
         * classic
         * uno
      * Leaf module (optional)
         * mini28777
         * stm32 breakBoard
      * Radio module
         * TEA5767 module
         * module with RDS
      * Display
         * Nokia 5110 LCD
         * LCD 2x16
         * Shield Arduino 16*2 LCD + Keyboard
         * ...
      * KeyPad
         * keyPad
         * rottary switch (nice!)
         * Shield Arduino 16*2 LCD + Keyboard
      * Amplifier
         * headphone
         * or other
            * TDA7384A - Elektronika dla wszystkich 6 2009, 7 2009
      * PreAmplifier (optional)
         * TDA8425 - Nowy Elektronik 4/5.2010
         * TDA7317 - Elektronika dla wszystkich 6 2009, 7 2009
      * iR control (optional)

##TEA5767 module

TEA5767 is a radio chip, a lot of mobile phones, MP3, MP4 to the radio functions are implemented in his.

   1. High sensitivity, low noise high frequency amplifier,
   2. Radio Frequency: 87.6MHz ~ 108MHz, (supporting the frequency range of 76MHz ~ 87.5MHz between the campus radio channels),
   3. LC tuned oscillator to lower cost, RF AGC circuit
   4. Built-in FM frequency selection, I2C bus control
   5. Built-in FM stereo demodulator, PLL synthesizer tuner decoder
   6. Two programmable ports, soft mute, SNC (stereo noise reduction)
   7. Adaptive stereo decoding, automatic search function
   8. Wait mode, requires a 7.6MHz crystal
   9. 40-pin LQFP package

![image](OMP_Radios/OMP_DisplaysReview/Res/TEA5767/TEA5767.jpg)

#OMP_RemoteFlowMeter
##Resources##

http://arduining.com/2012/07/17/arduino-hall-effect-sensor-gaussmeter/

http://garagelab.com/profiles/blogs/tutorial-how-to-use-the-hall-effect-sensor-with-arduino?xg_source=activity

http://bildr.org/2011/04/various-hall-effect-sensors/
#OMP_Sdr
#OMP_SDRTuner
#OMP_SelfBalancingRobot
#OMP_ServoTester
#OMP_SolarCharger
#OMP_StepUpConv
#OMP_STM32Oscilloscope
PE 02-99 - Wzmacniacz do oscyloskopu.

#OMP_SuperCap
3000F 2,7 V BCAP3000P

http://www.elektroda.pl/rtvforum/topic2510321.html

#OMP_uCMultimeter
## Intro ##
Some kind of multimeter based on:
- Arduino
- AD converter from 8051 module
- some I/U converter
- UI :
  - rotary impulsators
  - LCD display
    - graphic
    - text  
Features:
 - 4 U input channels
 - 1 I input channel
 - RLC input channel (optional)
 - beeper input channel (optional)

## AD converter  - AD7714AR-5##
<!-- ![AD7714AR-5](OMP_uCMultimeterOMP_DisplaysReview/Res/pic/IMG_2561.JPG) -->

[Documentation for AD7714](OMP_uCMultimeter/OMP_DisplaysReview/Res/pdfAD7714.pdf)

#OMP_UsbCharger
#OMP_VFD8Diggit
#OMP_WalkRobot

---
![image](./OMP_DisplaysReview/Res/1009853h.jpg)

See my [About](./OMP_DisplaysReview/Res/1009853h.jpg) page for details.

See my [About](./OMP_DisplaysReview/Res/LED21x60.docx) page for details.
