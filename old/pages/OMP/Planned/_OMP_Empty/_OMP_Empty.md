---
layout: page
title: "OMP - Empty"
description: ""
---
{% include JB/setup %}

---
Resources

[pikebishop.github.io]: Pike Bishop in web.

---
[<img src="Res/Empty.jpg" width="" height="100"/>](Res/Empty.jpg "Demo")
![image](Res/Empty.jpg)
See my [About](Res/Empty.jpg) page for details. 

<!-- Resources and links -->
[pikebishop.github.io]:http://pikebishop.github.io/
