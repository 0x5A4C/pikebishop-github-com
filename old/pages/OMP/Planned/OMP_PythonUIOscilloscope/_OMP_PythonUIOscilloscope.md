---
layout: page
title: "OMP_PythonUIOscilloscope"
description: ""
---
{% include JB/setup %}

---
# Intro #
Python application implementing UI client for Dallas8051 oscilloscope
# Review #
# Realization #
## Reverse engineering of transmission protocol ##
## Implementation ##

Resources

[pikebishop.github.io]: Pike Bishop in web.

---
![image](Res/w124_bc.gif)
See my [About](Res/w124_bc.gif) page for details. 

<!-- Resources and links -->
[pikebishop.github.io]:http://pikebishop.github.io/
