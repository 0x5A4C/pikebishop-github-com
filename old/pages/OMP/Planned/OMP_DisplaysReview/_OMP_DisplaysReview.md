---
layout: page
title: "OMP - Displays Review"
description: ""
---
{% include JB/setup %}

---
#Target#
**Review of all displays.**

- Led
- Lcd
- Vfd

#Pictures#
[<img src="Res/17190001.JPG" width="" height="300"/>](Res/17190001.JPG "")
[<img src="Res/17190002.JPG" width="" height="300"/>](Res/17190002.JPG "")
[<img src="Res/17200003.JPG" width="" height="300"/>](Res/17200003.JPG "")
[<img src="Res/17200004.JPG" width="" height="300"/>](Res/17200004.JPG "")
[<img src="Res/17200005.JPG" width="" height="300"/>](Res/17200005.JPG "")
[<img src="Res/17210006.JPG" width="" height="300"/>](Res/17210006.JPG "")
[<img src="Res/17210007.JPG" width="" height="300"/>](Res/17210007.JPG "")
[<img src="Res/17220008.JPG" width="" height="300"/>](Res/17220008.JPG "")
[<img src="Res/17220009.JPG" width="" height="300"/>](Res/17220009.JPG "")
[<img src="Res/17220010.JPG" width="" height="300"/>](Res/17220010.JPG "")
[<img src="Res/17230011.JPG" width="" height="300"/>](Res/17230011.JPG "")
[<img src="Res/17230012.JPG" width="" height="300"/>](Res/17230012.JPG "")
[<img src="Res/17230013.JPG" width="" height="300"/>](Res/17230013.JPG "")
[<img src="Res/17230014.JPG" width="" height="300"/>](Res/17230014.JPG "")
[<img src="Res/17240015.JPG" width="" height="300"/>](Res/17240015.JPG "")
[<img src="Res/17240016.JPG" width="" height="300"/>](Res/17240016.JPG "")
[<img src="Res/17240017.JPG" width="" height="300"/>](Res/17240017.JPG "")
[<img src="Res/17240018.JPG" width="" height="300"/>](Res/17240018.JPG "")
[<img src="Res/17250019.JPG" width="" height="300"/>](Res/17250019.JPG "")
[<img src="Res/17250020.JPG" width="" height="300"/>](Res/17250020.JPG "")
[<img src="Res/17250021.JPG" width="" height="300"/>](Res/17250021.JPG "")
[<img src="Res/17250022.JPG" width="" height="300"/>](Res/17250022.JPG "")
[<img src="Res/17260023.JPG" width="" height="300"/>](Res/17260023.JPG "")
[<img src="Res/17260024.JPG" width="" height="300"/>](Res/17260024.JPG "")
[<img src="Res/17260025.JPG" width="" height="300"/>](Res/17260025.JPG "")
[<img src="Res/17260026.JPG" width="" height="300"/>](Res/17260026.JPG "")
[<img src="Res/17270027.JPG" width="" height="300"/>](Res/17270027.JPG "")
[<img src="Res/17270028.JPG" width="" height="300"/>](Res/17270028.JPG "")
[<img src="Res/17270029.JPG" width="" height="300"/>](Res/17270029.JPG "")
[<img src="Res/17280030.JPG" width="" height="300"/>](Res/17280030.JPG "")
[<img src="Res/17280031.JPG" width="" height="300"/>](Res/17280031.JPG "")
[<img src="Res/17290032.JPG" width="" height="300"/>](Res/17290032.JPG "")
[<img src="Res/17290033.JPG" width="" height="300"/>](Res/17290033.JPG "")
[<img src="Res/17290034.JPG" width="" height="300"/>](Res/17290034.JPG "")
[<img src="Res/17300035.JPG" width="" height="300"/>](Res/17300035.JPG "")
[<img src="Res/17300036.JPG" width="" height="300"/>](Res/17300036.JPG "")
[<img src="Res/17340037.JPG" width="" height="300"/>](Res/17340037.JPG "")
[<img src="Res/17340038.JPG" width="" height="300"/>](Res/17340038.JPG "")
[<img src="Res/17340039.JPG" width="" height="300"/>](Res/17340039.JPG "")
[<img src="Res/17340040.JPG" width="" height="300"/>](Res/17340040.JPG "")
[<img src="Res/17340041.JPG" width="" height="300"/>](Res/17340041.JPG "")
[<img src="Res/17340042.JPG" width="" height="300"/>](Res/17340042.JPG "")
[<img src="Res/17350043.JPG" width="" height="300"/>](Res/17350043.JPG "")
[<img src="Res/17350044.JPG" width="" height="300"/>](Res/17350044.JPG "")
[<img src="Res/17350045.JPG" width="" height="300"/>](Res/17350045.JPG "")
[<img src="Res/17350046.JPG" width="" height="300"/>](Res/17350046.JPG "")
[<img src="Res/17350047.JPG" width="" height="300"/>](Res/17350047.JPG "")
[<img src="Res/17360048.JPG" width="" height="300"/>](Res/17360048.JPG "")
[<img src="Res/17360049.JPG" width="" height="300"/>](Res/17360049.JPG "")
[<img src="Res/17360050.JPG" width="" height="300"/>](Res/17360050.JPG "")
[<img src="Res/17360051.JPG" width="" height="300"/>](Res/17360051.JPG "")
[<img src="Res/17360052.JPG" width="" height="300"/>](Res/17360052.JPG "")
[<img src="Res/17370053.JPG" width="" height="300"/>](Res/17370053.JPG "")
[<img src="Res/17370054.JPG" width="" height="300"/>](Res/17370054.JPG "")
[<img src="Res/17370055.JPG" width="" height="300"/>](Res/17370055.JPG "")
[<img src="Res/17370056.JPG" width="" height="300"/>](Res/17370056.JPG "")
[<img src="Res/17380057.JPG" width="" height="300"/>](Res/17380057.JPG "")
[<img src="Res/17380058.JPG" width="" height="300"/>](Res/17380058.JPG "")
[<img src="Res/17380059.JPG" width="" height="300"/>](Res/17380059.JPG "")
[<img src="Res/17380060.JPG" width="" height="300"/>](Res/17380060.JPG "")
[<img src="Res/17380061.JPG" width="" height="300"/>](Res/17380061.JPG "")
[<img src="Res/17380062.JPG" width="" height="300"/>](Res/17380062.JPG "")
[<img src="Res/17400063.JPG" width="" height="300"/>](Res/17400063.JPG "")
[<img src="Res/17400064.JPG" width="" height="300"/>](Res/17400064.JPG "")
[<img src="Res/17400065.JPG" width="" height="300"/>](Res/17400065.JPG "")
[<img src="Res/17420066.JPG" width="" height="300"/>](Res/17420066.JPG "")
[<img src="Res/17420067.JPG" width="" height="300"/>](Res/17420067.JPG "")
[<img src="Res/17420068.JPG" width="" height="300"/>](Res/17420068.JPG "")
[<img src="Res/17420069.JPG" width="" height="300"/>](Res/17420069.JPG "")
[<img src="Res/17420070.JPG" width="" height="300"/>](Res/17420070.JPG "")
[<img src="Res/17430071.JPG" width="" height="300"/>](Res/17430071.JPG "")
[<img src="Res/17430072.JPG" width="" height="300"/>](Res/17430072.JPG "")
[<img src="Res/17430073.JPG" width="" height="300"/>](Res/17430073.JPG "")
[<img src="Res/17440074.JPG" width="" height="300"/>](Res/17440074.JPG "")
[<img src="Res/17440075.JPG" width="" height="300"/>](Res/17440075.JPG "")
[<img src="Res/17440076.JPG" width="" height="300"/>](Res/17440076.JPG "")
[<img src="Res/17440077.JPG" width="" height="300"/>](Res/17440077.JPG "")
[<img src="Res/17450078.JPG" width="" height="300"/>](Res/17450078.JPG "")
[<img src="Res/17450079.JPG" width="" height="300"/>](Res/17450079.JPG "")
[<img src="Res/17450080.JPG" width="" height="300"/>](Res/17450080.JPG "")
[<img src="Res/17450081.JPG" width="" height="300"/>](Res/17450081.JPG "")
[<img src="Res/17460082.JPG" width="" height="300"/>](Res/17460082.JPG "")
[<img src="Res/17460083.JPG" width="" height="300"/>](Res/17460083.JPG "")
[<img src="Res/17460084.JPG" width="" height="300"/>](Res/17460084.JPG "")
[<img src="Res/17460085.JPG" width="" height="300"/>](Res/17460085.JPG "")
[<img src="Res/17460086.JPG" width="" height="300"/>](Res/17460086.JPG "")
[<img src="Res/17460087.JPG" width="" height="300"/>](Res/17460087.JPG "")


# Resources #

[pikebishop.github.io]: Pike Bishop in web.

---
![image](Res/Empty.jpg)
See my [About](Res/Empty.jpg) page for details. 

<!-- Resources and links -->
[pikebishop.github.io]:http://pikebishop.github.io/
