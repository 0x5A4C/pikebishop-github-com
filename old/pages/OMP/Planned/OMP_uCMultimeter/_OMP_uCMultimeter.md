---
layout: page
title: "OMP_uCMultimeter"
description: ""
---
{% include JB/setup %}

---
Resources

[pikebishop.github.io]: Pike Bishop in web.


# Intro #
Some kind of multimeter based on:
- Arduino
- AD converter from 8051 module
- some I/U converter
- UI :
  - rotary impulsators 
  - LCD display
    - graphic 
    - text  
Features:
 - 4 U input channels
 - 1 I input channel
 - RLC input channel (optional) 
 - beeper input channel (optional) 
# AD converter  - AD7714AR-5#
![AD7714AR-5](Res/pic/IMG_2561.JPG)

[Documentation for AD7714](Res/pdfAD7714.pdf)

---
![image](Res/w124_bc.gif)
See my [About](Res/w124_bc.gif) page for details. 

<!-- Resources and links -->
[pikebishop.github.io]:http://pikebishop.github.io/
