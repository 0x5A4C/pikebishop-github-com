---
layout: page
title: "OMP - SimplePythonGame"
description: ""
---
{% include JB/setup %}

---

1. Implement simple Pong game
    1. get sources from [Trevor Appleton - Writing Pong using Python and Pygame] and run it, 
	2. start with PyGame,
	3. have fun
2. Implement simple Tank Battle game
	1. prepare OO project structure
	2. prepare version for 2 players playing on one computer
	3. have fun
3. Optionally enhance simple Tank Battle game
	1. playing across network
	2. add map/radar
	3. ability to change weapons
	4. add support for cheap sony ps manipulators (usb version is easier)
	3. ...
	4. have fun

Resources

[Trevor Appleton - Writing Pong using Python and Pygame] - some tutorial showing implementation of Pong game.



[pikebishop at github.io]: Pike Bishop in web.

---
![image](Res/Empty.jpg)

See my [About](Res/Empty.jpg) page for details. 

<!-- Resources and links -->
[Trevor Appleton - Writing Pong using Python and Pygame]: http://trevorappleton.blogspot.com/2014/04/writing-pong-using-python-and-pygame.html
[Presentation about game phisics with openGL and Python (demos avaliable)]: https://www.youtube.com/watch?v=G0jWMoOEFxc
[Presentation about game phisics with openGL and Python - same as above, but pure slides not movie]: http://www.slideshare.net/DanielPope2/programming-physics-games-with-python-and-opengl
[pikebishop at github.io]:http://pikebishop.github.io/
