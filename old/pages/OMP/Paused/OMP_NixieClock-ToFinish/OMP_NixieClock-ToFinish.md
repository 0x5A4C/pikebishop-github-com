---
layout: page
title: "OMP_NixieClock-ToFinish"
description: ""
---
{% include JB/setup %}

---

- Complete documentation.
- Configure Arduino framework to compile code for ATMEGA16 as target and USBAsp as programmer.
- Build new software.
- Fix problem with incorrect RTC frequency.
- Add ability to set time with IRED sensor and pilot or extra buttons, or capa buttons or whatever... (documentation).
- Add ability to receive DCF signal from central clock server or locally directly from receiver.
- Find power supply.
- Build Housing cuprum pipes, plexi or both.
 
Resources
---
![image](./Res/1009853h.jpg)

See my [About](./Res/1009853h.jpg) page for details. 

See my [About](./Res/LED21x60.docx) page for details.